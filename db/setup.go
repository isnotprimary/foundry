package db

import (
	"context"
	"embed"
	"errors"
	"fmt"
	"testing"

	"github.com/circleci/ex/testing/dbfixture"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/pgx"
	_ "github.com/golang-migrate/migrate/v4/source/file" // Needed to read migrations from an embed.fs
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/jmoiron/sqlx"

	"gitlab.com/isnotprimary/foundry/testing/testhelper"
)

//go:embed migrations
var migrations embed.FS

func SetupURL(url string) (*sqlx.DB, error) {
	db, err := sqlx.Connect("pgx", url)
	if err != nil {
		return nil, err
	}
	driver, err := pgx.WithInstance(db.DB, &pgx.Config{})
	if err != nil {
		return nil, err
	}
	fs, err := iofs.New(migrations, "migrations")
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithInstance("pgx", fs, "foundry", driver)
	if err != nil {
		return nil, err
	}

	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return nil, err
	}

	return db, nil
}
func Setup(user, password, host, name string) (*sqlx.DB, error) {
	url := fmt.Sprintf("postgres://%s:%s@%s/%s", user, password, host, name)
	return SetupURL(url)
}

//go:embed schema/schema.sql
var schema string

func SetupFixture(ctx context.Context, t testing.TB) (db *dbfixture.Fixture) {
	t.Helper()
	host := "localhost:5432"
	if testhelper.IsCI() {
		host = "postgres:5432"
	}
	return dbfixture.SetupDB(ctx, t, schema, dbfixture.Connection{
		Host:     host,
		User:     "foundry",
		Password: "insecureBox",
	})
}
