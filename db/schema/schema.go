package main

import (
	"log" //nolint: depguard // only used as a standalone executable to update schema

	"gitlab.com/isnotprimary/foundry/db"

	_ "github.com/golang-migrate/migrate/v4/database/pgx"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {
	_, err := db.Setup("foundry", "insecureBox", "localhost:5432", "foundry")
	if err != nil {
		log.Fatal(err)
	}
}
