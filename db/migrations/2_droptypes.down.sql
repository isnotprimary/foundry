CREATE TABLE types
(
    type_id         integer PRIMARY KEY,
    capacity        float,
    description     text,
    group_id        integer,
    icon_id         integer,
    market_group_id integer,
    mass            float,
    name            varchar(255),
    packaged_volume float,
    portion_size    integer,
    published       boolean,
    volume          float
);