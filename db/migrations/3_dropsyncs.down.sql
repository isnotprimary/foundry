CREATE TABLE sde_syncs
(
    id        varchar(80) PRIMARY KEY,
    must_sync boolean default true
);