package db

import (
	"context"
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
)

// Querier interfaces the *sqlx.DB and *sqlx.Tx queries to allow db functions to support
// transactions or direct queries
type Querier interface {
	SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	NamedExecContext(ctx context.Context, query string, arg interface{}) (sql.Result, error)
	GetContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error
	NamedQuery(query string, arg interface{}) (*sqlx.Rows, error)
	ExecContext(ctx context.Context, query string, args ...any) (sql.Result, error)
}

// CommitOrAbort will rollback the *sqlx.Tx if the error is not nil, otherwise it will complete
// It logs errors to the *zerolog.Event under txErr. It is intended for use as a defer directly
// after beginning a transaction
//
//	event := log.Info().Str("update", "assets")
//	start := time.Now()
//	defer logging.End(event, &start, &err)
//
//	tx, err := s.db.BeginTxx(ctx, nil)
//	if err != nil {
//	   return err
//	}
//	defer db.CommitOrAbort(tx, event, &err)
func CommitOrAbort(tx *sqlx.Tx, event *zerolog.Event, err *error) {
	var txErr error
	defer func() {
		if txErr != nil {
			event.Str("txErr", txErr.Error())
		}
	}()

	if *err != nil {
		if errors.Is(*err, sql.ErrTxDone) || errors.Is(*err, sql.ErrConnDone) {
			return
		}
		txErr = tx.Rollback()
		return
	}
	txErr = tx.Commit()
}
