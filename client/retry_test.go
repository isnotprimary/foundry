package client

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
	"time"

	"github.com/circleci/ex/testing/testcontext"
	"github.com/gin-gonic/gin"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestRetryRoundTrip_RoundTrip(t *testing.T) {
	tests := []struct {
		name      string
		apiReturn []result
		wantRes   int
		wantErr   bool
	}{
		{
			name: "first result ok",
			apiReturn: []result{
				{
					code: 200,
				},
			},
			wantRes: 200,
			wantErr: false,
		},
		{
			name: "first result 404",
			apiReturn: []result{
				{
					code: 404,
				},
			},
			wantRes: 404,
			wantErr: false,
		},
		{
			name: "first result 500 second ok",
			apiReturn: []result{
				{
					code: 500,
				},
				{
					code: 200,
				},
			},
			wantRes: 200,
			wantErr: false,
		},
		{
			name: "timeout during first",
			apiReturn: []result{
				{
					delay: 200 * time.Millisecond,
					code:  400,
				},
				{
					code: 200,
				},
			},
			wantRes: 200,
			wantErr: false,
		},
		{
			name: "context timeout",
			apiReturn: []result{
				{
					delay: 200 * time.Millisecond,
				},
				{
					delay: 200 * time.Millisecond,
				},
				{
					code: 200, // shouldn't be reached
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			r := gin.Default()
			api := fakeAPI{
				results: tt.apiReturn,
			}
			r.GET("/", api.handler)

			srv := httptest.NewServer(r)
			t.Cleanup(srv.Close)

			cli := http.Client{
				Timeout: 100 * time.Millisecond,
			}
			ctx, cancel := context.WithTimeout(testcontext.Background(), 300*time.Millisecond)
			t.Cleanup(cancel)

			_, gotRes, err := Retry(ctx, func() ([]int32, *http.Response, error) {
				res, err := cli.Get(srv.URL)
				return nil, res, err
			})
			if tt.wantErr {
				fmt.Println(gotRes)
				assert.Check(t, err != nil)
			} else {
				assert.NilError(t, err)
				assert.Check(t, cmp.Equal(tt.wantRes, gotRes.StatusCode))
				data, err := io.ReadAll(gotRes.Body)
				assert.NilError(t, err)
				assert.Check(t, cmp.Equal("data", string(data)))
			}
		})
	}
}

type fakeAPI struct {
	mu       sync.Mutex
	results  []result
	reqCount int
}

type result struct {
	code  int
	delay time.Duration
}

func (m *fakeAPI) handler(c *gin.Context) {
	m.mu.Lock()
	defer m.mu.Unlock()

	if m.reqCount >= len(m.results) {
		_ = c.Error(fmt.Errorf("too many requests made"))
		return
	}

	res := m.results[m.reqCount]
	m.reqCount++
	if res.delay > 0 {
		time.Sleep(res.delay)
	}
	if c.Request.Context().Err() != nil {
		return
	}
	c.String(res.code, "data")
}
