package client

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/poll"
)

func TestErrorLimitRoundTrip_RoundTrip(t *testing.T) {
	//TODO these tests are currently order dependent
	lh := &limitHandler{}
	fakeESIServer := httptest.NewServer(lh)
	t.Cleanup(func() {
		fakeESIServer.Close()
	})

	lh.set(100, 100, 200)

	hc := http.Client{
		Timeout:   10 * time.Second,
		Transport: &ErrorLimitRoundTrip{Proxied: http.DefaultTransport},
	}

	t.Run("healthy requests", func(t *testing.T) {
		_, err := hc.Get(fakeESIServer.URL)
		assert.NilError(t, err)

		_, err = hc.Get(fakeESIServer.URL)
		assert.NilError(t, err)
	})

	t.Run("no triggers from cached", func(t *testing.T) {
		lh.setCached(true)
		lh.set(1, 10, 200)
		_, err := hc.Get(fakeESIServer.URL)
		assert.NilError(t, err)

		lh.set(1, 10, 200)
		_, err = hc.Get(fakeESIServer.URL)
		assert.NilError(t, err)
		lh.setCached(false)
	})

	t.Run("low remaining request recovers", func(t *testing.T) {
		lh.set(1, 3, 200)
		_, err := hc.Get(fakeESIServer.URL)
		assert.NilError(t, err)

		_, err = hc.Get(fakeESIServer.URL)
		assert.ErrorIs(t, err, ErrRateLimit)

		until := time.Now().Add(2 * time.Second).Add(999 * time.Millisecond)
		poll.WaitOn(t, func(_ poll.LogT) poll.Result {
			_, err = hc.Get(fakeESIServer.URL)
			if time.Now().Before(until) && err == nil {
				return poll.Error(fmt.Errorf("circuit opened to early"))
			}
			if err != nil {
				return poll.Continue("Circuit not yet reset")
			}
			return poll.Success()
		}, poll.WithTimeout(4*time.Second))
	})

	t.Run("recovers after multiple errors", func(t *testing.T) {
		lh.set(1, 3, 200)

		until := time.Now().Add(2 * time.Second).Add(999 * time.Millisecond)
		for i := 0; i < 100; i++ {
			_, _ = hc.Get(fakeESIServer.URL)
		}

		poll.WaitOn(t, func(_ poll.LogT) poll.Result {
			_, err := hc.Get(fakeESIServer.URL)
			if time.Now().Before(until) && err == nil {
				return poll.Error(fmt.Errorf("circuit opened to early"))
			}
			if err != nil {
				return poll.Continue("Circuit not yet reset")
			}
			return poll.Success()
		}, poll.WithTimeout(4*time.Second))
	})

	t.Run("triggers on 420", func(t *testing.T) {
		lh.set(1, 10, 420)
		_, err := hc.Get(fakeESIServer.URL)
		assert.ErrorIs(t, err, ErrRateLimit)

		// Should still be broken, and therefore not get to a successful request
		lh.set(1, 10, 200)
		_, err = hc.Get(fakeESIServer.URL)
		assert.ErrorIs(t, err, ErrRateLimit)
	})
}

type limitHandler struct {
	remaining int
	reset     int
	status    int
	cached    bool
}

func (lh *limitHandler) set(remaining, reset, status int) {
	lh.remaining = remaining
	lh.reset = reset
	lh.status = status
}
func (lh *limitHandler) setCached(cached bool) {
	lh.cached = cached
}

func (lh *limitHandler) ServeHTTP(rw http.ResponseWriter, _ *http.Request) {
	rw.Header().Set("X-ESI-Error-Limit-Remain", strconv.Itoa(lh.remaining))
	rw.Header().Set("X-ESI-Error-Limit-Reset", strconv.Itoa(lh.reset))
	if lh.cached {
		rw.Header().Set("X-From-Cache", "1")
	}
	rw.WriteHeader(lh.status)
}
