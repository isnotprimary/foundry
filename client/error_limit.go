package client

import (
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
)

var ErrRateLimit = fmt.Errorf("esi rate limit exceeded")

type ErrorLimitRoundTrip struct {
	Proxied http.RoundTripper

	lock     sync.RWMutex
	exceeded bool
	timer    *time.Timer
}

func (el *ErrorLimitRoundTrip) RoundTrip(req *http.Request) (res *http.Response, err error) {
	event := log.Trace()
	defer event.Msg("esi client call")

	res, err = el.call(req)
	if err != nil {
		event.Err(err)
		return res, err
	}

	cached := res.Header.Get("X-From-Cache") == "1"
	event.Bool("cached", cached)

	if cached { // Don't re-trigger error limits from a cached value
		return res, nil
	}

	remain, reset, err := getErrorLimitValues(res)
	if err != nil {
		log.Warn().Err(err).Msg(`invalid "X-ESI-Error-Limit-Remain"" header received`)
		return res, err
	}

	event.Int("X-ESI-Error-Limit-Remain", remain)
	event.Dur("X-ESI-Error-Limit-Reset", reset)

	// Tries to start breaking the circuit just before we run out of limit
	if remain <= 20 || res.StatusCode == 420 {
		el.lock.Lock()
		defer el.lock.Unlock()

		// already broken circuit, but this req raced another
		if el.timer != nil && res.StatusCode == 420 {
			return nil, ErrRateLimit
		}

		if el.timer != nil {
			// Already broken and timer already set, all done
			return res, nil
		}

		el.timer = time.NewTimer(reset)
		el.exceeded = true
		go el.awaitExpiry()

		return res, nil
	}

	return res, err
}

func (el *ErrorLimitRoundTrip) awaitExpiry() {
	<-el.timer.C

	el.lock.Lock()
	defer el.lock.Unlock()
	el.timer = nil
	el.exceeded = false
}

func getErrorLimitValues(res *http.Response) (int, time.Duration, error) {
	if res == nil {
		return 0, 0, fmt.Errorf("no response available during error limit tracking")
	}
	remainStr := res.Header.Get("X-ESI-Error-Limit-Remain")
	resetStr := res.Header.Get("X-ESI-Error-Limit-Reset")

	if remainStr == "" || resetStr == "" {
		return 0, 0, fmt.Errorf("no error limit headers")
	}

	remain, err := strconv.Atoi(remainStr)
	if err != nil {
		return 0, 0, fmt.Errorf(`invalid "X-ESI-Error-Limit-Remain" header received`)
	}
	reset, err := strconv.Atoi(resetStr)
	if err != nil {
		return 0, 0, fmt.Errorf(`invalid "X-ESI-Error-Limit-Reset" header received`)
	}

	return remain, time.Duration(reset) * time.Second, nil
}

func (el *ErrorLimitRoundTrip) call(req *http.Request) (*http.Response, error) {
	el.lock.RLock()
	defer el.lock.RUnlock()

	if el.exceeded {
		return nil, ErrRateLimit
	}

	return el.Proxied.RoundTrip(req)
}
