package client

import (
	"fmt"
	"math"
	"net/http"
	"strconv"

	"golang.org/x/net/context"
)

type Paginator[F any] struct {
	do func(int32) ([]F, *http.Response, error)

	maxPage     int32
	currentPage int32
}

func NewPaginator[F any](do func(int32) ([]F, *http.Response, error)) Paginator[F] {
	return Paginator[F]{
		do:      do,
		maxPage: 1, // pages are 1 index, not known before first call
	}
}

func (p *Paginator[F]) Next() bool {
	return p.currentPage < p.maxPage
}

func (p *Paginator[F]) Page(ctx context.Context) ([]F, error) {
	p.currentPage++ // Pages are 1 index, so incr first

	f, resp, err := Retry(ctx, func() ([]F, *http.Response, error) {
		return p.do(p.currentPage)
	})
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting calling ESI: %s", resp.Status)
	}

	if p.currentPage == 1 {
		pagesStr := resp.Header.Get("X-Pages")
		if pagesStr != "" {
			maxPage, err := strconv.ParseInt(pagesStr, 10, 64)
			if maxPage > math.MaxInt32 {
				maxPage = math.MaxInt32
			}
			//nolint:gosec // range already capped
			p.maxPage = int32(maxPage)
			if err != nil {
				return nil, fmt.Errorf("unexpected header from esi: %v", err)
			}
		}
	}
	return f, nil
}
