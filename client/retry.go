package client

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/circleci/ex/o11y"
	"github.com/rs/zerolog"

	"gitlab.com/isnotprimary/foundry/logging"
)

func Retry[T any](ctx context.Context, do func() ([]T, *http.Response, error)) (t []T, res *http.Response, err error) {
	attempts := 0
	attempt := func() error {
		attempts++
		t, res, err = do()
		if err != nil {
			return err
		}
		if res.StatusCode >= 400 && res.StatusCode < 500 {
			return backoff.Permanent(nil)
		} else if res.StatusCode >= 500 {
			return fmt.Errorf("status code %d", res.StatusCode)
		}

		return nil
	}

	bo := backoff.NewExponentialBackOff()
	bo.InitialInterval = time.Millisecond * 50
	bo.MaxElapsedTime = 2 * time.Minute
	err = backoff.Retry(attempt, backoff.WithContext(bo, ctx))
	o11y.Log(ctx, "retry-calls",
		o11y.Pair{
			Key:   "attempts",
			Value: attempts,
		},
		o11y.Pair{
			Key:   logging.LogLevelKey,
			Value: zerolog.TraceLevel,
		},
	)
	return t, res, err
}
