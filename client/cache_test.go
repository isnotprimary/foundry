package client

import (
	"context"
	"testing"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/db"
)

func Test_update(t *testing.T) {
	ctx := context.Background()
	dbFix := db.SetupFixture(ctx, t)

	cache := NewCache(dbFix.DB)

	cache.Set("/path", []byte("first"))
	cache.Set("/path/to/another/11", []byte("{second\n@#~!\""))
	res, ok := cache.Get("/path")
	assert.Assert(t, ok)
	assert.Check(t, cmp.DeepEqual(res, []byte("first")))

	cache.Delete("/path")
	_, ok = cache.Get("/path")
	assert.Assert(t, !ok)

	res, ok = cache.Get("/path/to/another/11")
	assert.Assert(t, ok)
	assert.Check(t, cmp.DeepEqual(res, []byte("{second\n@#~!\"")))
}
