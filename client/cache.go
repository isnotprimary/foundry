package client

import (
	"github.com/jackc/pgtype"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
)

type PostgresCache struct {
	db *sqlx.DB
}

func NewCache(db *sqlx.DB) *PostgresCache {
	return &PostgresCache{
		db: db,
	}
}

func (pc *PostgresCache) Get(key string) ([]byte, bool) {
	var b [][]byte
	err := pc.db.Select(&b, sqlSelectReq, key)
	if err != nil {
		log.Error().Err(err).Msg("postgresCache.Get err")
		return nil, false
	}
	if len(b) == 0 {
		return nil, false
	}
	return b[0], true
}

type keyRes struct {
	ID   string       `db:"id"`
	Resp pgtype.Bytea `db:"resp"`
}

func (pc *PostgresCache) Set(key string, rBytes []byte) {
	b := keyRes{
		ID: key,
		Resp: pgtype.Bytea{
			Bytes:  rBytes,
			Status: pgtype.Present,
		},
	}
	_, err := pc.db.NamedExec(sqlInsertReq, b)
	if err != nil {
		log.Error().Err(err).Msg("postgresCache.Set err")
	}
}

func (pc *PostgresCache) Delete(key string) {
	_, err := pc.db.Exec(sqlDeleteReq, key)
	if err != nil {
		log.Error().Err(err).Msg("postgresCache.Delete err")
	}
}

// language=PostgreSQL
const sqlSelectReq = `
SELECT resp FROM cached_esi
WHERE id = $1;
`

// language=PostgreSQL
const sqlInsertReq = `
INSERT INTO cached_esi (id, resp)
VALUES (:id, :resp)
ON CONFLICT (id) DO UPDATE
SET resp=EXCLUDED.resp;
`

// language=PostgreSQL
const sqlDeleteReq = `
DELETE FROM cached_esi
WHERE id = $1;
`
