package client

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/circleci/ex/o11y"
)

type MetricsRoundTrip struct {
	proxied http.RoundTripper
	mp      o11y.MetricsProvider
}

func NewMetricClient(p http.RoundTripper, mp o11y.MetricsProvider) *MetricsRoundTrip {
	return &MetricsRoundTrip{
		proxied: p,
		mp:      mp,
	}
}

func (el *MetricsRoundTrip) RoundTrip(req *http.Request) (res *http.Response, err error) {
	start := time.Now()
	_ = el.mp.Count("http_request", 1, []string{"http_path:" + req.URL.Path, "http_host:" + req.Host}, 1)

	res, err = el.proxied.RoundTrip(req)
	tags := getTags(req, res)
	if err != nil {
		_ = el.mp.Count("http_err", 1, tags, 1)
	}

	_ = el.mp.TimeInMilliseconds("http_client", float64(time.Since(start).Milliseconds()), tags, 1)

	remain, _, tagErr := getErrorLimitValues(res)
	if tagErr != nil {
		return res, err
	}
	_ = el.mp.Gauge("esi_error_budget", float64(remain), nil, 1)

	return res, err
}

func getTags(req *http.Request, res *http.Response) []string {
	var tags []string

	path := req.URL.Path
	parts := strings.Split(path, "/")
	for i, part := range parts {
		_, err := strconv.Atoi(part)
		if err == nil {
			parts[i] = "{id}"
		}
	}

	tags = append(tags,
		tag("http_path", strings.Join(parts, "/")),
		tag("http_method", req.Method),
		tag("http_host", req.URL.Hostname()),
	)
	if res != nil {
		code := fmt.Sprintf("%dxx", res.StatusCode/100)
		tags = append(tags, tag("http_status", code))
		cached := res.Header.Get("X-From-Cache") == "1"
		tags = append(tags, tag("http_cached", fmt.Sprintf("%t", cached)))
	} else {
		tags = append(tags, tag("http_status", "0"))
		tags = append(tags, tag("http_cached", "false"))
	}

	return tags
}

func tag(k, v string) string {
	return fmt.Sprintf("%s:%s", k, v)
}
