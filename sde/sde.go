package sde

import (
	"context"
	"os"

	"github.com/circleci/ex/o11y"
	"gopkg.in/yaml.v3"
)

func LoadSDE[T any](ctx context.Context, name, file string, out *T) (err error) {
	//nolint:ineffassign,staticcheck // Assign the context so it can't accidentally be missed if func extended
	ctx, span := o11y.StartSpan(ctx, "load-"+name)
	defer o11y.End(span, &err)

	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	return yaml.NewDecoder(f).Decode(out)
}
