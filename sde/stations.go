package sde

import (
	"context"
	"path/filepath"
)

type Station struct {
	ConstellationID int32     `yaml:"constellationID" json:"constellation_id"`
	OperationID     int32     `yaml:"operationID" json:"-"`
	Operation       Operation `yaml:"-" json:"-"`
	RegionID        int32     `yaml:"regionID" json:"region_id"`
	SystemID        int32     `yaml:"solarSystemID" json:"system_id"`
	ID              int32     `yaml:"stationID" json:"id"`
	Name            string    `yaml:"stationName" json:"name"`
}

type Operation struct {
	Services []int32 `yaml:"services"`
}

func LoadStations(ctx context.Context, sdeDir string) (sMap map[int32]Station, err error) {
	file := filepath.Join(sdeDir, "bsd", "staStations.yaml")
	stations := make([]Station, 4000)
	err = LoadSDE(ctx, "stations", file, &stations)
	if err != nil {
		return nil, err
	}

	ops := make(map[int32]Operation)
	file = filepath.Join(sdeDir, "fsd", "stationOperations.yaml")
	err = LoadSDE(ctx, "operations", file, &ops)
	if err != nil {
		return nil, err
	}

	sMap = make(map[int32]Station)
	for _, s := range stations {
		s := s
		s.Operation = ops[s.OperationID]
		sMap[s.ID] = s
	}

	return sMap, nil
}
