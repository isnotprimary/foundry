package sde

import (
	"context"
	"path/filepath"
)

type Skill struct {
	Level  int32 `yaml:"level"`
	TypeID int32 `yaml:"typeID"`
}

type Material struct {
	Quantity int32 `yaml:"quantity"`
	TypeID   int32 `yaml:"typeID"`
}

type Manufacturing struct {
	Skills    []Skill    `yaml:"skills"`
	Materials []Material `yaml:"materials"`
	Products  []Material `yaml:"products"`
	Time      int32      `yaml:"time"`
}

type Activities struct {
	Manufacturing Manufacturing `yaml:"manufacturing"`
}

type Blueprint struct {
	MetaGroupID        int32      `yaml:"-"`
	TypeID             int32      `yaml:"blueprintTypeID"`
	MaxProductionLimit int32      `yaml:"maxProductionLimit"`
	Activities         Activities `yaml:"activities"`
}

type Type struct {
	MetaGroupID int32 `yaml:"metaGroupID"`
}

func LoadBlueprints(ctx context.Context, sdeDir string) (bp map[int32]Blueprint, err error) {
	return loadBlueprints(ctx, sdeDir)
}

func loadBlueprints(ctx context.Context, sdeDir string) (bp map[int32]Blueprint, err error) {
	file := filepath.Join(sdeDir, "fsd", "blueprints.yaml")
	blueprints := make(map[int32]Blueprint, 4000)
	err = LoadSDE(ctx, "blueprints", file, &blueprints)
	if err != nil {
		return nil, err
	}

	tfile := filepath.Join(sdeDir, "fsd", "types.yaml")
	types := make(map[int32]Type)
	err = LoadSDE(ctx, "typeIDs", tfile, &types)
	if err != nil {
		return nil, err
	}

	for i, bp := range blueprints {
		if len(bp.Activities.Manufacturing.Products) > 0 {
			product := bp.Activities.Manufacturing.Products[0]
			t := types[product.TypeID]
			bp.MetaGroupID = t.MetaGroupID
			blueprints[i] = bp
		}
	}

	return blueprints, nil
}
