package sde

import (
	"testing"

	"github.com/circleci/ex/testing/testcontext"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestLoadStations_real(t *testing.T) {
	ctx := testcontext.Background()
	stations, err := LoadStations(ctx, "../data/sde")
	assert.Assert(t, cmp.Nil(err))
	assert.Check(t, cmp.Len(stations, 5154))
}
