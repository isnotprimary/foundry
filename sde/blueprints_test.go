package sde

import (
	"testing"

	"github.com/circleci/ex/testing/testcontext"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
	"gotest.tools/v3/fs"
)

func TestLoadAllBlueprints(t *testing.T) {
	ctx := testcontext.Background()
	gotDBTypes, err := LoadBlueprints(ctx, "../data/sde")
	assert.Assert(t, cmp.Nil(err))
	assert.Check(t, len(gotDBTypes) > 4713, "Unlikely low number of blueprints")
}

func TestLoadBlueprints(t *testing.T) {
	tests := []struct {
		name        string
		file        fs.PathOp
		expectedBPs map[int32]Blueprint
	}{
		{
			name: "single full blueprint",
			file: fs.WithDir("fsd",
				fs.WithFile("blueprints.yaml", singleBPFile),
				fs.WithFile("types.yaml", metagroups),
			),
			expectedBPs: singleBP,
		},
		{
			name: "2 blueprints",
			file: fs.WithDir("fsd",
				fs.WithFile("blueprints.yaml", twoBPFile),
				fs.WithFile("types.yaml", metagroups),
			),
			expectedBPs: twoBPs,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := testcontext.Background()
			sdeDir := fs.NewDir(t, "archived-dir", tt.file)

			gotBp, err := LoadBlueprints(ctx, sdeDir.Path())
			assert.NilError(t, err)
			assert.Check(t, cmp.DeepEqual(tt.expectedBPs, gotBp))
		})
	}
}

var metagroups = `
582:
  metaGroupID: 1
165:
  metaGroupID: 2
`

var singleBPFile = `
683:
    activities:
        copying:
            time: 4800
        invention:
            materials:
            -   quantity: 2
                typeID: 20416
            -   quantity: 2
                typeID: 25887
            products:
            -   probability: 0.3
                quantity: 1
                typeID: 39581
            skills:
            -   level: 1
                typeID: 11442
            -   level: 1
                typeID: 11454
            -   level: 1
                typeID: 21790
            time: 63900
        manufacturing:
            materials:
            -   quantity: 24000
                typeID: 34
            -   quantity: 4500
                typeID: 35
            -   quantity: 1875
                typeID: 36
            -   quantity: 375
                typeID: 37
            products:
            -   quantity: 1
                typeID: 582
            skills:
            -   level: 1
                typeID: 3380
            time: 6000
        research_material:
            time: 2100
        research_time:
            time: 2100
    blueprintTypeID: 683
    maxProductionLimit: 30
`

var BP683 = Blueprint{
	Activities: Activities{
		Manufacturing: Manufacturing{
			Skills: []Skill{
				{
					Level:  1,
					TypeID: 3380,
				},
			},
			Materials: []Material{
				{
					Quantity: 24000,
					TypeID:   34,
				},
				{
					Quantity: 4500,
					TypeID:   35,
				},
				{
					Quantity: 1875,
					TypeID:   36,
				},
				{
					Quantity: 375,
					TypeID:   37,
				},
			},
			Products: []Material{
				{
					Quantity: 1,
					TypeID:   582,
				},
			},
			Time: 6000,
		},
	},
	TypeID:             683,
	MaxProductionLimit: 30,
	MetaGroupID:        1,
}
var singleBP = map[int32]Blueprint{
	683: BP683,
}

var twoBPFile = singleBPFile + `
681:
    activities:
        copying:
            time: 480
        manufacturing:
            materials:
            -   quantity: 86
                typeID: 38
            products:
            -   quantity: 1
                typeID: 165
            time: 600
        research_material:
            time: 210
        research_time:
            time: 210
    blueprintTypeID: 681
    maxProductionLimit: 300
`
var BP681 = Blueprint{
	Activities: Activities{
		Manufacturing: Manufacturing{
			Materials: []Material{
				{
					Quantity: 86,
					TypeID:   38,
				},
			},
			Products: []Material{
				{
					Quantity: 1,
					TypeID:   165,
				},
			},
			Time: 600,
		},
	},
	TypeID:             681,
	MaxProductionLimit: 300,
	MetaGroupID:        2,
}

var twoBPs = map[int32]Blueprint{
	683: BP683,
	681: BP681,
}
