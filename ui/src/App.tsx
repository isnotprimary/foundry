import {useCallback, useEffect, useState} from 'react'
import './App.css'
import TitleBar from './TitleBar'
import api, {Portraits} from './api';
import Blueprints from './Blueprints';
import Footer from "./Footer.tsx";

function App() {
  const [portraits, setPortraits] = useState<Portraits>();
  const [loggedIn, setLoggedIn] = useState<boolean>(false);

  const getPortrait = useCallback(() => {
    api<Portraits>("/api/portrait")
    .then(response => {
      setPortraits(response)
      setLoggedIn(true)
    })
    .catch(err => console.log(err))
  }, [])

  useEffect(() => {
    getPortrait()
  }, [getPortrait]);


  return (
      <>
        <div className="align-content-center w-full m-auto">
          <TitleBar portraits={portraits}/>
          <Blueprints isLoggedIn={loggedIn}/>
          <Footer/>
        </div>
      </>
  )
}

export default App
