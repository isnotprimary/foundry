import {describe, expect, it} from 'vitest'
import Blueprints from "./Blueprints";
import {act, render, screen, userEvent, within} from './test/test-render'

describe('Blueprints renders', () => {

  it('when not logged in', () => {
    const {container} = render(
        <Blueprints isLoggedIn={false}/>
    )

    expect(container).toMatchSnapshot()
  })
  it('when logged in', async () => {
    const container = render(
        <Blueprints isLoggedIn={true}/>
    )
    const tab = await screen.findByText("Region")
    await act(() => userEvent.click(tab))
    
    const regionDiv = await screen.findByTestId("region-combo-loading-false")
    const button = await within(regionDiv).findAllByRole("button")
    await act(() => userEvent.click(button[0]))
    await new Promise(r => setTimeout(r, 1000)); // wait for animation
    await screen.findByText("2 options available.")
    expect(container).toMatchSnapshot()
  })
})