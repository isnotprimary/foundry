import {describe, expect, it} from 'vitest'
import InfoCard from './InfoCard'
import {render} from './test/test-render.tsx'

describe('InfoCard renders', () => {
  it('when isLoggedIn is true', () => {
    const component = render(
        <InfoCard isLoggedIn={true}/>
    )
    expect(component).toMatchSnapshot()
  })
  it('when isLoggedIn is false', () => {
    const component = render(
        <InfoCard isLoggedIn={false}/>
    )
    expect(component).toMatchSnapshot()
  })
})