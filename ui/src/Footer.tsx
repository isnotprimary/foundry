import {Divider, Image, Link} from "@heroui/react";
import gitlab from "/square-gitlab.svg"

function Footer() {
  return (
      <>
        <Divider className="my-4"/>
        <div
            className="flex flex-col relative overflow-hidden height-auto m-auto">
          <span className="d-block max-w-[1024px] align-content-center m-auto">
            <div>MIT License - Copyright © 2023 Dominic Lavery</div>
          </span>
          <span className="d-block max-w-[1024px] align-content-center m-auto mt-2">
            <Link href="https://gitlab.com/isnotprimary/foundry" isExternal target="_self">
              <Image
                  width={36}
                  alt="Gitlab Icon"
                  src={gitlab}
              />
            </Link>
          </span>
        </div>
      </>
  )
}

export default Footer