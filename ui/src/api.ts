function api<T>(url: string): Promise<T> {
  return fetch(url)
  .then(response => {
    if (!response.ok) {
      throw new Error(response.statusText)
    }
    return response.json() as Promise<T>
  })
}

export async function postBuildQueue(region: number|undefined, market: number|undefined, factory: number, req: BuildQueueItem[]): Promise<BuildQueueResult> {
  let params
  if (region) {
    params = {
      region: String(region),
      factory: String(factory),
    }
  } else {
    params = {
      market: String(market),
      factory: String(factory),
    }
  }
  return fetch("/api/buildqueue?" + new URLSearchParams(params).toString(), {
    method: "POST",
    body: JSON.stringify({blueprints: req})
  })
  .then(response => {
    if (!response.ok) {
      throw new Error(response.statusText)
    }
    return response.json() as Promise<BuildQueueResult>
  })
}

export interface BuildQueueResult {
  materials: MaterialCost[]
  blueprints: Blueprint[]
}

export interface BuildQueueItem {
  item_id: number,
  runs: number
}

export interface Portraits {
  px64x64: string,
  px128x128: string,
  px256x256: string,
  px512x512: string
}

export interface Region {
  name: string,
  id: number,
}

export interface Blueprint {
  name: string,
  typeId: number,
  activities: Activities,
  max_production_limit: number

  item_id: number,
  location_flag: string,
  location_id: number,
  material_efficiency: number,
  quantity: number,
  runs: number,
  time_efficiency: number
  cost: CostList
}

export interface CostList {
  runs: number
  total: number
  profit: number
  profit_perc: number
  sales_tax: number
  cost_per_unit: number
  sales_volume_profit: number
  possible: boolean
}

export interface Activities {
  skills: Skill[],
  materials: Material[],
  products: Material[],
  time: number
}

export interface Skill {
  level: number,
  typeID: number
}

export interface Material {
  quantity: number,
  typeID: number
}

export interface MaterialCost {
  quantity: number,
  typeID: number,
  total: number,
  name: string,
  enough: boolean
}

export interface Station {
  id: number,
  name: string
}

export default api