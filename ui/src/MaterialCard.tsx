import {Card, CardBody} from "@heroui/react"
import {MaterialCost} from "./api"


interface Props {
  material: MaterialCost
}

export default function MaterialCard(props: Props) {
  let background = ""
  if (!props.material.enough) {
    background = " bg-warning"
  }
  return (
      <Card className={"min-h-[6em] max-w-[1024px] m-auto mt-1"+background}>
        <CardBody>
          <span>{props.material.name}</span>
          <span>Needed: {props.material.quantity.toLocaleString('en')}</span>
          <span>Cost: {props.material.total.toLocaleString('en')} ISK</span>
        </CardBody>
      </Card>
  )
}