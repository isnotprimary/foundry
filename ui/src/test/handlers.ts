import {http, HttpResponse} from 'msw'

export const handlers = [
    http.get(
        '/api/blueprints',
        ({request}) => {
            const url = new URL(request.url)

            const rID = url.searchParams.get("region")
            const fID = url.searchParams.get("factory")

            if (rID === "10000002" && fID === "60002959") {
                return HttpResponse.json([{
                    "name": "Large Targeting Systems Stabilizer I Blueprint",
                    "item_id": 100000060,
                    "cost": {
                        "runs": 40,
                        "profit": 201355546.23,
                        "profit_perc": 219.43,
                        "cost_per_unit": 22.44,
                        "sales_volume_profit": 23.45,
                    }
                }])
            }
        },
    ),
    http.get(
        '/api/allblueprints',
        ({request}) => {
            const url = new URL(request.url)

            const rID = url.searchParams.get("region")
            const fID = url.searchParams.get("factory")

            if (rID === "10000002" && fID === "60002959") {
                return HttpResponse.json([{
                    "name": "Large Targeting Systems Stabilizer I Blueprint",
                    "item_id": 100000060,
                    "cost": {
                        "runs": 40,
                        "profit": 201355546.23,
                        "profit_perc": 219.43,
                        "cost_per_unit": 22.44,
                        "sales_volume_profit": 23.45,
                    }
                }])
            }
        },
    ),
    http.get(
        '/api/factories',
        () => {
            return HttpResponse.json([
                {
                    "name": "Jita 1",
                    "id": 60002959,
                },
                {
                    "name": "Jita 2",
                    "id": 60002960,
                }
            ])
        },
    ),
    http.get(
        '/api/regions',
        () => {
            return HttpResponse.json([
                {
                    "name": "Region 1",
                    "id": 60002959,
                },
                {
                    "name": "Region 2",
                    "id": 60002960,
                }
            ])
        },
    )
]