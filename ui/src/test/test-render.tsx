import {ReactElement} from 'react'
/* eslint-disable */
import {render, RenderOptions} from '@testing-library/react'
import {HeroUIProvider} from "@heroui/react"

const AllTheProviders = ({children}: { children: React.ReactNode }) => {
  return (
      <HeroUIProvider>
        {children}
      </HeroUIProvider>
  )
}

const customRender = (
    ui: ReactElement,
    options?: Omit<RenderOptions, 'wrapper'>,
) => render(ui, {wrapper: AllTheProviders, ...options})

export * from '@testing-library/react'
export {default as userEvent} from '@testing-library/user-event'
export {customRender as render}