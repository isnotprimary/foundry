import {
    Autocomplete,
    AutocompleteItem,
    Button,
    Card,
    CardBody,
    Tab,
    Tabs,
    useDisclosure
} from "@heroui/react";
import {BuildQueueItem, BuildQueueResult, postBuildQueue, Region, Station} from "./api";
import {useAsyncList} from "@react-stately/data";
import {useState} from "react";
import BlueprintsTable from "./BlueprintsTable.tsx";
import InfoCard from "./InfoCard.tsx";
import BuildQueue from "./BuildQueue.tsx";
import RegionPicker from "./RegionPicker.tsx";
import TradeHubPicker from "./TradeHubPicker.tsx";

interface Props {
    isLoggedIn: boolean
}

interface SearchOpts {
    regionID: number | undefined,
    marketID: number | undefined,
    tech: string,
    factoryID: number,
    all: boolean
}

export default function Blueprints(props: Props) {
    const [region, setRegion] = useState<number | undefined>();
    const [market, setMarket] = useState<number | undefined>();
    const [factory, setFactory] = useState<number | undefined>();
    const [allBlueprints, setAllBlueprints] = useState<boolean>(false);
    const [tech, setTech] = useState<string>("ALL");
    const [search, setSearch] = useState<SearchOpts>();
    const [selectedKeys, setSelectedKeys] = useState<Set<number>>(new Set([]));
    const {isOpen, onOpen, onOpenChange} = useDisclosure();
    const [loading, setLoading] = useState(false);
    const [queue, setQueue] = useState<BuildQueueResult>();
    const [runs, setRuns] = useState(new Map<number, number>());
    const [bqError, setBqError] = useState<boolean>(false)

    const runSearch = function () {
        setSearch({
            regionID: region,
            factoryID: factory!,
            marketID: market,
            tech: tech,
            all: allBlueprints,
        })
    }

    const factories = useAsyncList<Region>({
        async load({signal}) {
            const res = await fetch("/api/factories", {signal})
            const json = await res.json() as Station[];
            //todo errors
            return {
                items: json,
            };
        },
    });

    const fetchBuildQueue = function (rID: number|undefined, mID: number|undefined, fID: number) {
        setLoading(true)

        const req = new Array<BuildQueueItem>()
        for (const key of selectedKeys) {
            let reqRun = runs.get(Number(key))
            if (typeof reqRun === "undefined") {
                reqRun = 0 // 0 is defaulted in the backend
            }
            req.push({item_id: Number(key), runs: reqRun})
        }

        setBqError(false)

        //todo better error handling
        postBuildQueue(rID, mID, fID, req)
            .then(bqr => {
                if (bqr) {
                    setQueue(bqr)
                }
            })
            .catch(r => {
                setBqError(true)
                console.log(r)
            })
            .finally(() => setLoading(false))
    }

    const openBuildQueue = function () {
        if ((typeof region === "undefined" && typeof market === "undefined") ||
            typeof factory === "undefined") {
            console.log("region or factory noy set for build queue. This shouldn't happen and is a bug")
            return
        }

        fetchBuildQueue(region, market, factory)
        onOpen()
    }

    return (
        <>
            <Card className="d-block max-w-[1024px] align-content-center m-auto mt-2">
                <CardBody className="grid grid-cols-1 sm:grid-cols-4 gap-4 items-end justify-center">
                    <Tabs aria-label="hub or region" placement="top">
                        <Tab key="hub" title="Trade Hub" className="pb-0">
                            <TradeHubPicker isLoggedIn={props.isLoggedIn} setMarket={e => {
                                setMarket(e)
                                setRegion(undefined)
                            }}/>
                        </Tab>
                        <Tab key="region" title="Region" className="pb-0">
                            <RegionPicker isLoggedIn={props.isLoggedIn} setRegion={e => {
                                setRegion(e)
                                setMarket(undefined)
                            }}/>
                        </Tab>
                    </Tabs>
                    <div data-testid={`factory-combo-loading-${factories.isLoading}`}>
                        <Tabs aria-label="blueprints considered" placement="top" onSelectionChange={s=> {
                            setAllBlueprints(s === "all")
                            // If searching all blueprints, we disallow selecting all tech levels
                            if (s === "all" && tech === "ALL") {
                                setTech("TECH1")
                            }
                        }}>
                            <Tab key="owned" title="Owned" className="pb-0"/>
                            <Tab key="all" title="All" className="pb-0"/>
                        </Tabs>
                        <Autocomplete
                            isDisabled={!props.isLoggedIn}
                            className="max-w-xs m-auto pt-3"
                            label="Select a factory station"
                            placeholder="Type to search..."
                            variant="bordered"
                            isLoading={factories.isLoading}
                            defaultItems={factories.items}
                            allowsCustomValue={false}
                            listboxProps={{hideEmptyContent: true}}
                            popoverProps={{className: "dark text-foreground"}}
                            onSelectionChange={e => setFactory(e as number)}
                        >
                            {(item) => (
                                <AutocompleteItem key={item.id}>
                                    {item.name}
                                </AutocompleteItem>
                            )}
                        </Autocomplete>
                    </div>
                    <div>
                        <Autocomplete
                            isDisabled={!props.isLoggedIn}
                            className="max-w-xs m-auto"
                            label="Tech filter"
                            variant="bordered"
                            defaultSelectedKey="ALL"
                            allowsCustomValue={false}
                            listboxProps={{hideEmptyContent: true}}
                            popoverProps={{className: "dark text-foreground"}}
                            onSelectionChange={e => setTech(e as string)}
                            selectedKey={tech}
                        >
                            {/*When using the all blueprints endpoint a meta group must be select for performance*/}
                            { allBlueprints ? <></> : <AutocompleteItem key="ALL">All</AutocompleteItem> }
                            <AutocompleteItem key="TECH1">Tech 1</AutocompleteItem>
                            <AutocompleteItem key="TECH2">Tech 2</AutocompleteItem>
                            <AutocompleteItem key="ADV">Advanced</AutocompleteItem>
                            <AutocompleteItem key="STRUCT">Structure</AutocompleteItem>
                        </Autocomplete>
                    </div>
                    <div className="flex gap-4 items-center">
                        <Button className="m-auto"
                                isDisabled={!props.isLoggedIn || !factory || (!region && !market)}
                                color="primary"
                                variant="flat"
                                onPress={runSearch}>
                            Search
                        </Button>
                        <Button className="m-auto"
                                isDisabled={!(selectedKeys.size > 0) || search?.all}
                                color="primary"
                                variant="flat"
                                onPress={openBuildQueue}>
                            Build queue
                        </Button>
                    </div>
                </CardBody>
            </Card>
            {search ?
                <BlueprintsTable key={`${search.regionID}-${search.marketID}-${search.factoryID}-${search.tech}-${search.all}`}
                                 factory={search?.factoryID}
                                 region={search?.regionID}
                                 market={search?.marketID}
                                 tech={search?.tech}
                                 all={search.all}
                                 selectedKeys={selectedKeys}
                                 setSelectedKeys={setSelectedKeys}/> :
                <InfoCard isLoggedIn={props.isLoggedIn}/>
            }
            {factory &&
                <BuildQueue queue={queue}
                            onOpenChange={onOpenChange}
                            isOpen={isOpen}
                            setRunsMap={setRuns}
                            runsMap={runs}
                            loading={loading}
                            update={() => {
                                fetchBuildQueue(region, market, factory)
                            }}
                            error={bqError}/>
            }
        </>
    )
}