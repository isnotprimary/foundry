import {useAsyncList} from "@react-stately/data";
import {Blueprint} from "./api.ts";
import {
  Pagination,
  Spinner,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  Selection
} from "@heroui/react";
import {get} from "lodash"
import React from "react";

interface Props {
  factory: number
  region: number|undefined
  market: number|undefined
  all: boolean
  tech: string
  selectedKeys: Set<number>
  setSelectedKeys: (keys: Set<number>) => void
}

export default function BlueprintsTable(props: Props) {
  const blueprints = useAsyncList<Blueprint>({
    async load({signal}) {
      let params
      if (props.region) {
        params = {
          region: String(props.region),
          factory: String(props.factory),
          meta_group_id: "",
        }
      } else {
        params = {
          market: String(props.market),
          factory: String(props.factory),
          meta_group_id: "",
        }
      }
      params.meta_group_id = props.tech
      const path = props.all ? "/api/allblueprints?" : "/api/blueprints?"
      const res = await fetch(path + new URLSearchParams(params).toString(), {signal})
      const json = await res.json() as Blueprint[];
      //todo errors:
      return {
        items: json,
      };
    },
    initialSortDescriptor: {
      column: "cost.sales_volume_profit",
      direction: "descending"
    },
    sort({items, sortDescriptor}) {
      return {
        items: items.sort((a, b) => {
          let cmp = 0
          switch (sortDescriptor.column) {
            case "cost.profit":
              cmp = a.cost.profit < b.cost.profit ? -1 : 1
              break
            case "cost.profit_perc":
              cmp = a.cost.profit_perc < b.cost.profit_perc ? -1 : 1
              break
            case "cost.sales_volume_profit":
              cmp = a.cost.sales_volume_profit < b.cost.sales_volume_profit ? -1 : 1
              break
            case "cost.cost_per_unit":
              cmp = a.cost.cost_per_unit < b.cost.cost_per_unit ? -1 : 1
              break
          }
          
          if (sortDescriptor.direction === "descending") {
            cmp *= -1;
          }

          return cmp;
        }),
      };
    },
  });

  const [page, setPage] = React.useState(1);
  const rowsPerPage = 20;

  const pages = Math.ceil(blueprints.items.length / rowsPerPage);

  const items = React.useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;

    return blueprints.items.slice(start, end);
  }, [page, blueprints]);


  let keys : Selection
  if (props.selectedKeys.size === blueprints.items.length ) {
    keys = "all"
  } else {
    // The keys seem to need to be string, the api wants them in ints, so we deal with
    // the conversion here
    const set = new Set<string>
    props.selectedKeys.forEach(k => set.add(k.toString()))
    keys = set
  }

  const onSelectionChange = function (keys: Selection) {
    const set = new Set<number>
    if (keys === "all") {
      blueprints.items.forEach(bp => {
        set.add(bp.item_id)
      })
    } else {
      // The keys seem to need to be string, the api wants them in ints, so we deal with
      // the conversion here
      keys.forEach(k => set.add(Number(k)))
    }
    props.setSelectedKeys(set)
  }

  return (
      <Table isHeaderSticky
             hideHeader={blueprints.isLoading}
             selectionMode={props.all ? "none": "multiple"}
             selectedKeys={keys}
             onSelectionChange={onSelectionChange}
             sortDescriptor={blueprints.sortDescriptor}
             onSortChange={desc => {blueprints.sort(desc)}}
             isStriped={props.all}
             className="mt-4 lg:px-4"
             classNames={{
               table: "min-h-[56px]",
             }}
             aria-label="results"
             bottomContent={
                 !blueprints.isLoading && items.length > 0 &&
                 <div className="flex w-full justify-center">
                   <Pagination
                       isCompact
                       showControls
                       showShadow
                       color="primary"
                       page={page}
                       total={pages}
                       onChange={(page) => setPage(page)}
                   />
                 </div>
             }>
        <TableHeader>
          <TableColumn key="name">Name</TableColumn>
          <TableColumn key="cost.runs">Runs</TableColumn>
          <TableColumn key="material_efficiency">ME</TableColumn>
          <TableColumn key="time_efficiency">TE</TableColumn>
          <TableColumn allowsSorting key="cost.profit_perc">Profit%</TableColumn>
          <TableColumn allowsSorting key="cost.profit">Profit</TableColumn>
          <TableColumn allowsSorting key="cost.sales_volume_profit">Daily Sales Profit</TableColumn>
          <TableColumn allowsSorting key="cost.cost_per_unit">Unit Cost</TableColumn>
          {!props.all ? <TableColumn key="station_name">Location</TableColumn>: <></>}
        </TableHeader>
        <TableBody
            isLoading={blueprints.isLoading}
            items={items}
            loadingContent={<Spinner/>}
            emptyContent={!blueprints.isLoading && "No viable blueprints to display."}
            data-testid={"table-body"}
        >
          {(item) => (
              <TableRow data-testid={"table-row"} key={props.all? item.name : item.item_id}>
                {(columnKey) =>
                    columnKey === "cost.profit" || columnKey === "cost.cost_per_unit" ||
                    columnKey === "cost.sales_volume_profit" ?
                        <TableCell>{Number(get(item, columnKey)).toLocaleString('en') + " ISK"}</TableCell> :
                        <TableCell>{get(item, columnKey)}</TableCell>
                }
              </TableRow>
          )}
        </TableBody>
      </Table>
  )
}