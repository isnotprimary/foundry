import {describe, expect, it} from 'vitest'
import TitleBar from "./TitleBar";
import {render} from './test/test-render.tsx'

describe('Titlebar renders', () => {
  it('without portrait', () => {
    const {container} = render(
        <TitleBar/>
    )
    expect(container).toMatchSnapshot()
  })
})