import {useAsyncList} from "@react-stately/data";
import {Region} from "./api.ts";
import {Autocomplete, AutocompleteItem} from "@heroui/react";

interface Props {
    isLoggedIn: boolean
    setRegion: (region: number) => void
}

export default function RegionPicker(props: Props) {
    const regions = useAsyncList<Region>({
        async load({signal}) {
            const res = await fetch("/api/regions", {signal})
            const json = await res.json() as Region[];
            //todo errors
            return {
                items: json,
            };
        },
    });

    return (
        <div data-testid={`region-combo-loading-${regions.isLoading}`}>
            <Autocomplete
                isDisabled={!props.isLoggedIn}
                className="max-w-xs m-auto"
                label="Select a trade region"
                placeholder="Type to search..."
                variant="bordered"
                isLoading={regions.isLoading}
                defaultItems={regions.items}
                listboxProps={{hideEmptyContent: true}}
                popoverProps={{className: "dark text-foreground"}}
                allowsCustomValue={false}
                menuTrigger="input"
                onSelectionChange={e => props.setRegion(e as number)}
            >
                {(item) => (
                    <AutocompleteItem key={item.id}>
                        {item.name}
                    </AutocompleteItem>
                )}
            </Autocomplete>
        </div>
    )
}