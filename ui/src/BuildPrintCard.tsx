import {Card, CardBody, Slider} from "@heroui/react"
import {Blueprint} from "./api"


interface Props {
  blueprint: Blueprint
  runs: number
  setRuns: (runs: number) => void
}

export default function BuildPrintCard(props: Props) {
  let maxRuns = props.blueprint.runs
  if (maxRuns <= 0 || props.blueprint.max_production_limit < maxRuns) {
    maxRuns = props.blueprint.max_production_limit
  }
  let background = ""
  if (!props.blueprint.cost.possible) {
    background = " bg-warning"
  }
  return (
      <Card className={"lg:min-w-max min-h-[6em] max-w-[1024px] m-auto mt-1"+ background}>
        <CardBody>
          <span>{props.blueprint.name}</span>
          <span>Runs: {props.runs}</span>
          <Slider
              step={1}
              maxValue={maxRuns}
              minValue={1}
              onChange={(runs) => {
                if (typeof runs === "number") {
                  props.setRuns(runs)
                }
              }}
              value={props.runs}
              className="max-w-md"
          />
          <span>Cost: {props.blueprint.cost.total.toLocaleString("en")} ISK</span>
          <span>Profit: {props.blueprint.cost.profit.toLocaleString("en")} ISK</span>
        </CardBody>
      </Card>
  )
}