import {Autocomplete, AutocompleteItem} from "@heroui/react";

interface Props {
    isLoggedIn: boolean
    setMarket: (market: number) => void
}

export default function TradeHubPicker(props: Props) {
    const hubs = [
        {
            id: 60003760,
            name: "Jita IV - Moon 4 - Caldari Navy Assembly Plant",
        },
        {
            id: 60008494,
            name: "Amarr VIII (Oris) - Emperor Family Academy",
        },
        {
            id: 60004588,
            name: "Rens VI - Moon 8 - Brutor Tribe Treasury",
        },
        {
            id: 60011866,
            name: "Dodixie IX - Moon 20 - Federation Navy Assembly Plant",
        },
        {
            id: 60005686,
            name: "Hek VIII - Moon 12 - Boundless Creation Factory",
        }
    ]

    return (
            <Autocomplete
                isDisabled={!props.isLoggedIn}
                className="max-w-xs m-auto"
                label="Select a trade hub"
                placeholder="Type to search..."
                variant="bordered"
                defaultItems={hubs}
                listboxProps={{hideEmptyContent: true}}
                popoverProps={{className: "dark text-foreground"}}
                allowsCustomValue={false}
                // menuTrigger="input"
                onSelectionChange={e => props.setMarket(e as number)}
            >
                {(item) => (
                    <AutocompleteItem key={item.id}>
                        {item.name}
                    </AutocompleteItem>
                )}
            </Autocomplete>
    )
}