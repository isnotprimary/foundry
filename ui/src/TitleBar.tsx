import {
  Avatar,
  Image,
  Link,
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem
} from "@heroui/react";
import eveLogin from './assets/evesso.png'
import {Portraits} from "./api";
import factory from "/factory.svg"

interface Props {
  portraits?: Portraits
}

export default function TitleBar(props: Props) {
  return (
      <Navbar className="p-2 pt-4">
        <NavbarBrand className="gap-2">
          <Image
              width={36}
              src={factory}
          />
          <p className="font-bold text-inherit">Eve Foundry</p>
        </NavbarBrand>
        <NavbarContent justify="end">
          <NavbarItem>
            {props.portraits
                ?
                <Link href="/api/login" isExternal target="_self">
                  <Avatar isBordered radius="sm" size="lg" src={props.portraits.px64x64}
                          alt="user avatar"/>
                </Link>
                :
                <Link href="/api/login" isExternal target="_self">
                  <Image src={eveLogin} alt="login with eve"/>
                </Link>
            }
          </NavbarItem>
        </NavbarContent>
      </Navbar>
  );
}