import {Card, CardBody, CardFooter, Divider, Link} from "@heroui/react";

interface Props {
  isLoggedIn: boolean
}

export default function InfoCard(props: Props) {
  return (
      <Card className="d-block max-w-[1024px] m-auto mt-4">
        <CardBody className="items-center justify-center text-center">
          <p>Eve Foundry helps you find your currently most profitable blueprints in Eve
            Online. It checks the sell order price of raw materials, calculates the cost of
            manufacturing and checks if it is profitable to sell an item to buy orders.</p>
        </CardBody>
        {!props.isLoggedIn ?
            <>
              <Divider/>
              <CardFooter>
                <Link
                    isExternal
                    showAnchorIcon
                    href="/api/login"
                    target="_self"
                >
                  Login to get started
                </Link>
              </CardFooter>
            </> :
            <>
              <Divider/>
              <CardFooter className="text-warning">
                <p>Select a trade region, factory station and hit search to find profitable blueprints.
                  Select blueprints to add them to the build queue to get a shopping list of materials.
                </p>
              </CardFooter>
            </>
        }
      </Card>
  )
}