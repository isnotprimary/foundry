import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Spinner
} from "@heroui/react"
import {BuildQueueResult} from "./api"
import BuildQueueBody from "./BuildQueueBody.tsx";

interface Props {
  queue?: BuildQueueResult
  isOpen: boolean
  onOpenChange: (isOpen: boolean) => void
  runsMap: Map<number, number>
  setRunsMap: (runsMap: Map<number, number>) => void
  loading: boolean
  error: boolean
  update: () => void
}

export default function BuildQueue(props: Props) {
  const copyToClipboard = ()=> {
    let text = ""
    props.queue?.materials.forEach(mat => {
      text += `${mat.name}	${mat.quantity}\n`
    })
    navigator.clipboard.writeText(text).catch(e => console.log(e));
  }

  return (
      <Modal isOpen={props.isOpen} onOpenChange={props.onOpenChange} scrollBehavior="inside"
             className="dark text-foreground bg-background max-h-[90vh] w-fit lg:min-w-max text-sm">
        <ModalContent>
          {(onClose) => (
              <>
                <ModalHeader className="flex flex-col gap-1">Build Queue</ModalHeader>
                {props.loading ?
                    <Spinner/> :
                      props.error ?
                          <ModalBody>An error occurred loading the build queue </ModalBody>:
                          <BuildQueueBody queue={props.queue} setRunsMap={props.setRunsMap}
                                          runsMap={props.runsMap}/>
                }
                <ModalFooter>
                  <Button color="primary" variant="light" onPress={copyToClipboard}
                          disabled={props.loading}>
                    Copy for Multibuy
                  </Button>
                  <Button color="primary" variant="light" onPress={props.update}
                          disabled={props.loading}>
                    Update
                  </Button>
                  <Button color="danger" variant="light" onPress={onClose}>
                    Close
                  </Button>
                </ModalFooter>
                </>
          )}
        </ModalContent>
      </Modal>
  )
}