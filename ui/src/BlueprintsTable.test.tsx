import {describe, expect, it} from 'vitest'
import BlueprintsTable from "./BlueprintsTable.tsx";
import {render, screen} from './test/test-render.tsx'

describe('BlueprintsTable renders', () => {

  it('when gets response', async () => {
    render(
        <BlueprintsTable region={10000002}
                         factory={60002959}
                         market={undefined}
                         tech={'ALL'}
                         all={false}
                         selectedKeys={new Set()}
            // eslint-disable-next-line @typescript-eslint/no-empty-function
                         setSelectedKeys={() => {
                         }}/>
    )
    await screen.findByTestId("table-row")
    const tb = await screen.findByTestId("table-body")
    expect(tb).toMatchSnapshot()
  })

  it('when gets response and in all mode', async () => {
    render(
        <BlueprintsTable region={10000002}
                         factory={60002959}
                         market={undefined}
                         tech={'ALL'}
                         all={true}
                         selectedKeys={new Set()}
            // eslint-disable-next-line @typescript-eslint/no-empty-function
                         setSelectedKeys={() => {
                         }}/>
    )
    await screen.findByTestId("table-row")
    const tb = await screen.findByTestId("table-body")
    expect(tb).toMatchSnapshot()
  })
})