import {ModalBody} from "@heroui/react";
import MaterialCard from "./MaterialCard.tsx";
import BuildPrintCard from "./BuildPrintCard.tsx";
import {BuildQueueResult} from "./api.ts";

interface Props {
  queue?: BuildQueueResult
  runsMap: Map<number, number>
  setRunsMap: (runsMap: Map<number, number>) => void
}

export default function BuildQueueBody(props: Props) {
return (
    <>
      <ModalBody className="grid grid-cols-2">
        <div>
          {props.queue?.materials.map(
              (mat) => {
                return <MaterialCard key={mat.typeID} material={mat}/>
              }
          )}
        </div>
        <div>
          {props.queue?.blueprints.map(
              (bp) => {
                let runsVal = props.runsMap.get(bp.item_id)
                if (typeof runsVal === "undefined") {
                  runsVal = bp.cost.runs
                }
                return <BuildPrintCard
                    key={bp.item_id}
                    blueprint={bp}
                    runs={runsVal}
                    setRuns={(runs) => {
                      props.setRunsMap(new Map<number, number>(props.runsMap).set(bp.item_id, runs))
                    }}
                />
              }
          )}
        </div>
      </ModalBody>
    </>
)
}