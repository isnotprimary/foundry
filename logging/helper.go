package logging

import (
	"context"

	"github.com/rs/zerolog"

	"github.com/circleci/ex/o11y"
)

type helpers struct{}

func (n helpers) ExtractPropagation(_ context.Context) o11y.PropagationContext {
	return o11y.PropagationContext{}
}

func (n helpers) InjectPropagation(ctx context.Context,
	ca o11y.PropagationContext, _ ...o11y.SpanOpt) (context.Context, o11y.Span) {
	ctx, span := o11y.StartSpan(ctx, "root")
	for k, v := range ca.Headers {
		span.AddRawField(k, v)
	}
	span.AddRawField(LogLevelKey, zerolog.TraceLevel)
	return ctx, span
}

func (n helpers) TraceIDs(_ context.Context) (traceID, parentID string) {
	return "", ""
}
