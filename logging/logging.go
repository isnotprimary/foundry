package logging

import (
	"context"
	"os"
	"time"

	"github.com/circleci/ex/httpserver"
	"github.com/circleci/ex/o11y"
	"github.com/circleci/ex/system"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const LogLevelKey = "LOG_LEVEL"

func Setup(ctx context.Context, sys *system.System, level zerolog.Level, bind string) (
	context.Context, func(context.Context), error) {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(level)

	reg := prometheus.NewRegistry()
	reg.MustRegister(collectors.NewGoCollector())
	p := ZeroLogProvider{
		metricsProvider: NewPrometheusProvider(reg),
		globalFields:    make(map[string]interface{}),
	}
	ctx = o11y.WithProvider(ctx, &p)

	_, err := httpserver.Load(ctx, httpserver.Config{
		Name:          "metrics",
		Addr:          bind,
		Handler:       promhttp.HandlerFor(reg, promhttp.HandlerOpts{}),
		ShutdownGrace: 5 * time.Second,
	}, sys)

	if err != nil {
		return nil, nil, err
	}

	return ctx, p.Close, nil
}
