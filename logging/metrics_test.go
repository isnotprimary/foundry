package logging

import (
	"net/http/httptest"
	"testing"

	"github.com/circleci/ex/httpclient"
	"github.com/circleci/ex/testing/testcontext"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/client_golang/prometheus/testutil"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
	"gotest.tools/v3/golden"
)

func TestPrometheusProvider_Count(t *testing.T) {
	tests := []struct {
		name     string
		testFunc func(*PrometheusProvider, string, float64, []string) error
		values   [4]float64
		gold     string
	}{
		{
			name: "counts",
			testFunc: func(pp *PrometheusProvider, s string, i float64, strings []string) error {
				return pp.Count(s, int64(i), strings, 1)
			},
			values: [4]float64{1, 1, 1, 1},
			gold:   "counts.txt",
		},
		{
			name: "histograms",
			testFunc: func(pp *PrometheusProvider, s string, i float64, strings []string) error {
				return pp.Histogram(s, i, strings, 1)
			},
			values: [4]float64{1.2, 1.2, 1.2, 4.4},
			gold:   "histograms.txt",
		},
		{
			name: "gauges",
			testFunc: func(pp *PrometheusProvider, s string, i float64, strings []string) error {
				return pp.Gauge(s, i, strings, 1)
			},
			values: [4]float64{1, 1, 1, 1},
			gold:   "gauges.txt",
		},
		{
			name: "summaries",
			testFunc: func(pp *PrometheusProvider, s string, i float64, strings []string) error {
				return pp.TimeInMilliseconds(s, i, strings, 1)
			},
			values: [4]float64{1.2, 1.2, 3.3, 4.5},
			gold:   "summaries.txt",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reg := prometheus.NewRegistry()
			pp := NewPrometheusProvider(reg)

			metricName := "test.metric"
			err := tt.testFunc(pp, metricName, tt.values[0], []string{"tag:1", "test.tag:2"})
			assert.NilError(t, err)

			lint, err := testutil.GatherAndLint(prometheus.DefaultGatherer, metricName)
			assert.NilError(t, err)
			assert.Check(t, cmp.Len(lint, 0))

			err = tt.testFunc(pp, metricName, tt.values[1], []string{"tag2:1", "test.tag:2"})
			assert.Error(t, err, "label name \"tag\" missing in label map")

			err = tt.testFunc(pp, metricName, tt.values[2], []string{"tag:1", "test.tag:2"})
			assert.NilError(t, err)
			err = tt.testFunc(pp, metricName, tt.values[3], []string{"tag:1", "test.tag:2"})
			assert.NilError(t, err)

			c, err := testutil.GatherAndCount(reg)
			assert.NilError(t, err)
			assert.Check(t, cmp.Equal(c, 1))

			server := httptest.NewServer(promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
			client := httpclient.New(httpclient.Config{
				Name:    "metrics",
				BaseURL: server.URL,
			})
			t.Cleanup(func() {
				server.Close()
			})
			var got string
			req := httpclient.NewRequest("GET", "/metrics", httpclient.Decoder(200, httpclient.NewStringDecoder(&got)))

			err = client.Call(testcontext.Background(), req)
			assert.NilError(t, err)
			golden.Assert(t, got, tt.gold)
		})
	}
}
