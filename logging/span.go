package logging

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/circleci/ex/o11y"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type ZeroLogSpan struct {
	name            string
	start           time.Time
	fields          map[string]interface{}
	metricsProvider o11y.MetricsProvider
	metrics         []o11y.Metric
}

func (s *ZeroLogSpan) AddField(key string, val interface{}) {
	s.fields["app."+key] = val
}
func (s *ZeroLogSpan) AddRawField(key string, val interface{}) {
	s.fields[key] = val
}

func (s *ZeroLogSpan) RecordMetric(metric o11y.Metric) {
	s.metrics = append(s.metrics, metric)
}

func (s *ZeroLogSpan) End() {
	s.sendSpan()
	s.sendMetrics()
}

func (s *ZeroLogSpan) Flatten(_ string) {

}

func (s *ZeroLogSpan) sendSpan() {
	var event *zerolog.Event

	rawErr, hasErr := s.fields["error"]
	rawWarn, hasWarn := s.fields["warn"]
	rawLevel, hasLevel := s.fields[LogLevelKey]
	rawResult, hasResult := s.fields["result"]
	if strings.HasPrefix(s.name, "worker loop:") && hasResult && rawResult == "success" { // noisy low value log
		rawLevel = zerolog.TraceLevel
		hasLevel = true
	}

	if hasErr {
		event = log.Err(ensureErr(rawErr))
		delete(s.fields, "error")
	} else if hasWarn {
		event = log.Err(ensureErr(rawWarn))
		delete(s.fields, "warning")
	} else if hasLevel {
		switch l := rawLevel.(type) {
		case zerolog.Level:
			event = log.WithLevel(l)
		case string:
			level, err := zerolog.ParseLevel(l)
			if err != nil {
				event = log.WithLevel(zerolog.NoLevel)
				event.Any("parsing_error", err)
			}
			event = log.WithLevel(level)
		}
		delete(s.fields, LogLevelKey)
	} else {
		event = log.Debug()
	}

	for k, v := range s.fields {
		event.Any(k, v)
	}

	if !s.start.IsZero() {
		duration := time.Since(s.start)
		event.Dur("duration_ms", duration)
	}
	event.Msg(s.name)
}

func (s *ZeroLogSpan) sendMetrics() {
	if s.metricsProvider == nil {
		return
	}
	for _, m := range s.metrics {
		tags := extractTagsFromFields(m.TagFields, s.fields)
		val, ok := getField(m.Field, s.fields)
		if !ok {
			continue
		}
		switch m.Type {
		case o11y.MetricTimer:
			valFloat, ok := toMilliSecond(val)
			if !ok {
				panic(m.Field + " can not be coerced to milliseconds")
			}
			_ = s.metricsProvider.TimeInMilliseconds(m.Name, valFloat, tags, 1)
		case o11y.MetricGauge:
			valFloat, ok := toFloat64(val)
			if !ok {
				panic(m.Field + " can not be coerced to float")
			}
			_ = s.metricsProvider.Gauge(m.Name, valFloat, tags, 1)
		case o11y.MetricCount:
			valInt, ok := toInt64(val)
			if !ok {
				panic(m.Field + " can not be coerced to float")
			}
			_ = s.metricsProvider.Count(m.Name, valInt, tags, 1)
		}
	}
}

func toMilliSecond(val interface{}) (float64, bool) {
	if f, ok := toFloat64(val); ok {
		return f, true
	}
	d, ok := val.(time.Duration)
	if !ok {
		p, ok := val.(*time.Duration)
		if !ok {
			return 0, false
		}
		d = *p
	}
	return float64(d.Milliseconds()), true
}

func extractTagsFromFields(tags []string, fields map[string]interface{}) []string {
	result := make([]string, 0, len(tags))
	for _, name := range tags {
		val, ok := getField(name, fields)
		if ok {
			result = append(result, fmtTag(normaliseName(name), val))
		}
	}
	return result
}

func getField(name string, fields map[string]interface{}) (interface{}, bool) {
	val, ok := fields[name]
	if !ok {
		// Also support the app. prefix, for interop with honeycomb's prefixed fields
		val, ok = fields["app."+name]
	}
	return val, ok
}

func fmtTag(name string, val interface{}) string {
	return fmt.Sprintf("%s:%v", name, val)
}

func toFloat64(val interface{}) (float64, bool) {
	if i, ok := val.(float64); ok {
		return i, true
	}
	if i, ok := toInt64(val); ok {
		return float64(i), true
	}
	return 0, false
}

func toInt64(val interface{}) (int64, bool) {
	switch v := val.(type) {
	case int64:
		return v, true
	case int:
		return int64(v), true
	}
	return 0, false
}

func ensureErr(rawErr interface{}) error {
	switch err := rawErr.(type) {
	case string:
		return errors.New(err)
	case error:
		return err
	default:
		return fmt.Errorf("%v", err)
	}
}
