package logging

import (
	"fmt"
	"strings"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type metricMap[T prometheus.Collector] map[string]*T

type PrometheusProvider struct {
	registry   *prometheus.Registry
	factory    promauto.Factory
	histograms metricMap[prometheus.HistogramVec]
	summaries  metricMap[prometheus.SummaryVec]
	gauges     metricMap[prometheus.GaugeVec]
	counts     metricMap[prometheus.CounterVec]
	mu         sync.Mutex
}

func NewPrometheusProvider(registry *prometheus.Registry) *PrometheusProvider {
	pp := &PrometheusProvider{
		histograms: make(metricMap[prometheus.HistogramVec]),
		summaries:  make(metricMap[prometheus.SummaryVec]),
		gauges:     make(metricMap[prometheus.GaugeVec]),
		counts:     make(metricMap[prometheus.CounterVec]),
		registry:   registry,
		factory:    promauto.With(registry),
	}

	return pp
}

func normaliseName(name string) string {
	name = strings.TrimPrefix(name, "gauge.")
	return strings.ReplaceAll(name, ".", "_")
}

func (mm metricMap[T]) getMetric(
	constructor func(string, []string) *T, name string, tags []string) (prometheus.Labels, *T, error) {

	name = normaliseName(name)
	keys, labels, err := toLabels(tags)
	if err != nil {
		return nil, nil, err
	}

	metric, ok := mm[name]
	if !ok {
		mm[name] = constructor(name, keys)
		metric = mm[name]
		if err != nil {
			return nil, nil, err
		}
	}

	return labels, metric, nil
}

//nolint:dupl // false positive
func (p *PrometheusProvider) Histogram(name string, value float64, tags []string, _ float64) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	labels, metric, err := p.histograms.getMetric(func(s string, names []string) *prometheus.HistogramVec {
		return p.factory.NewHistogramVec(prometheus.HistogramOpts{
			Name: s,
		}, names)
	}, name, tags)
	if err != nil {
		return err
	}
	obv, err := metric.GetMetricWith(labels)
	if err != nil {
		return err
	}

	obv.Observe(value)
	return nil
}

func (p *PrometheusProvider) TimeInMilliseconds(name string, value float64, tags []string, _ float64) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	labels, metric, err := p.summaries.getMetric(func(s string, names []string) *prometheus.SummaryVec {
		return p.factory.NewSummaryVec(prometheus.SummaryOpts{
			Name:       s,
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		}, names)
	}, name, tags)
	if err != nil {
		return err
	}

	obv, err := metric.GetMetricWith(labels)
	if err != nil {
		return err
	}
	obv.Observe(value)

	return nil
}

//nolint:dupl // false positive
func (p *PrometheusProvider) Gauge(name string, value float64, tags []string, _ float64) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	labels, metric, err := p.gauges.getMetric(func(s string, names []string) *prometheus.GaugeVec {
		return p.factory.NewGaugeVec(prometheus.GaugeOpts{
			Name: s,
		}, names)
	}, name, tags)
	if err != nil {
		return err
	}

	setter, err := metric.GetMetricWith(labels)
	if err != nil {
		return err
	}
	setter.Set(value)

	return nil
}

func countName(s string) string {
	if strings.HasSuffix(s, "_total") {
		return s
	}
	return s + "_total"
}

func (p *PrometheusProvider) Count(name string, value int64, tags []string, _ float64) error {
	p.mu.Lock()
	defer p.mu.Unlock()
	name = countName(name)

	labels, metric, err := p.counts.getMetric(func(s string, names []string) *prometheus.CounterVec {
		return p.factory.NewCounterVec(prometheus.CounterOpts{
			Name: s,
		}, names)
	}, name, tags)
	if err != nil {
		return err
	}

	obv, err := metric.GetMetricWith(labels)
	if err != nil {
		return err
	}
	obv.Add(float64(value))

	return nil
}

func toLabels(tags []string) ([]string, prometheus.Labels, error) {
	labels := make(prometheus.Labels, len(tags))
	keys := make([]string, len(tags))
	for i, tag := range tags {
		parts := strings.Split(tag, ":")
		if len(parts) != 2 {
			return nil, nil, fmt.Errorf("invalid tag provided: %s", tag)
		}
		keys[i] = normaliseName(parts[0])
		labels[normaliseName(parts[0])] = parts[1]
	}
	return keys, labels, nil
}
