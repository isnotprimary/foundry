package logging

import (
	"context"
	"time"

	"github.com/circleci/ex/o11y"
)

type ZeroLogProvider struct {
	metricsProvider o11y.MetricsProvider
	globalFields    map[string]interface{}
}

type spanCtxKeyType struct{}

var spanCtxKey = spanCtxKeyType{}

func (p *ZeroLogProvider) StartSpan(ctx context.Context, name string, _ ...o11y.SpanOpt) (context.Context, o11y.Span) {
	s := ZeroLogSpan{
		name:            name,
		start:           time.Now(),
		fields:          map[string]interface{}{},
		metricsProvider: p.metricsProvider,
	}
	for k, v := range p.globalFields {
		s.fields[k] = v
	}
	ctx = context.WithValue(ctx, spanCtxKey, s)
	return ctx, &s
}

func (p *ZeroLogProvider) AddGlobalField(key string, val interface{}) {
	p.globalFields[key] = val
}

func (p *ZeroLogProvider) GetSpan(ctx context.Context) o11y.Span {
	if s, ok := ctx.Value(spanCtxKey).(*ZeroLogSpan); ok {
		return s
	}
	return nil
}

func (p *ZeroLogProvider) AddField(ctx context.Context, key string, val interface{}) {
	s := p.GetSpan(ctx)
	if s == nil {
		return
	}
	s.AddField(key, val)
}

func (p *ZeroLogProvider) AddFieldToTrace(ctx context.Context, key string, val interface{}) {
	//Log "spans" are not in a hierarchy - so just add it to the in flight span
	p.AddField(ctx, key, val)
}

func (p *ZeroLogProvider) Log(_ context.Context, name string, fields ...o11y.Pair) {
	s := ZeroLogSpan{
		name:            name,
		fields:          map[string]interface{}{},
		metricsProvider: p.metricsProvider,
	}
	for k, v := range p.globalFields {
		s.fields[k] = v
	}
	for _, pair := range fields {
		s.fields[pair.Key] = pair.Value
	}
	s.sendSpan()
}

func (p *ZeroLogProvider) Close(_ context.Context) {
	if p.metricsProvider != nil {
		closer, ok := p.metricsProvider.(o11y.ClosableMetricsProvider)
		if ok {
			_ = closer.Close()
		}
	}
}

func (p *ZeroLogProvider) MetricsProvider() o11y.MetricsProvider {
	return p.metricsProvider
}

func (p *ZeroLogProvider) Helpers(_ ...bool) o11y.Helpers {
	return helpers{}
}

func (p *ZeroLogProvider) MakeSpanGolden(ctx context.Context) context.Context {
	return ctx
}
