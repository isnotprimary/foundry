#!/usr/bin/env bash
set -ex -o pipefail

if [ "$CI" != "true" ]; then
  GOBIN="${PWD}/bin" go install "golang.org/x/tools/cmd/goimports@v0.29.0"
  ./bin/goimports -local "gitlab.com/isnotprimary/foundry" -w "${@:-.}"
fi

GOBIN="${PWD}/bin" go install "github.com/golangci/golangci-lint/cmd/golangci-lint@v1.63.4"
./bin/golangci-lint run --timeout 5m "${@:-./...}"
