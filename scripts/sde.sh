#!/usr/bin/env bash
set -eu -o pipefail

mkdir -p data/sde

latest_checksum="data/sde/latest.sha"
previous_checksum="data/sde/previous.sha"

sde_latest_checksum="data/sde/sde_latest.sha"
sde_previous_checksum="data/sde/sde_previous.sha"

new_sde=false
curl --retry 5 --retry-connrefused  \
  "https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/checksum" \
  -o $latest_checksum

if ! cmp -s $latest_checksum $previous_checksum; then
  echo "SDE changed or not yet fetched - downloading"
  curl --retry 5 --retry-connrefused  \
    "https://eve-static-data-export.s3-eu-west-1.amazonaws.com/tranquility/sde.zip" \
    -o data/sde/sde.zip

  unzip -u data/sde/sde.zip \
    fsd/blueprints.yaml \
    bsd/staStations.yaml \
    fsd/stationOperations.yaml \
    fsd/types.yaml \
    -d data/sde

  cp $latest_checksum $previous_checksum
  new_sde=true
else
  echo "SDE is already uptodate"
fi

sha256sum ./sde/* | sha256sum > $sde_latest_checksum
if $new_sde || ! cmp -s $sde_latest_checksum $sde_previous_checksum; then
  echo "Regenerating code"
  go generate ./sde/
  cp $sde_latest_checksum $sde_previous_checksum
else
    echo "Generation is already uptodate"
fi
