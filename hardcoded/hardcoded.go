package hardcoded

const AccountingSkillID = 16622
const AccountTaxRateReduction = 0.11
const BaseBrokerFeePercentage = 0.03
const BaseTaxRatePercentage = 0.08
const BrokerFeeMin = 100.00
const BrokerRelationsReduction = 0.003
const BrokerRelationsSkillID = 3446
const FacilittySCCSurchage = 0.04
const FacilityTaxPercentage = 0.0025
const ItemIDMin = 1000000000000
const SDEServiceIDFactory = 14
const StationIDLow = 60000000
const StationIDHigh = 61000000
