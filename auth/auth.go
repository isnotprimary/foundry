package auth

import (
	"net/http"
	"time"

	"github.com/antihax/goesi"

	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/oauth2"
)

type EveAuther interface {
	AuthCodeURL(state string) string
	Exchange(code string) (*EveCreds, error)
	Refresh(token *oauth2.Token) (*oauth2.Token, error)
}

var scopesArr = []string{
	"publicData",
	"esi-skills.read_skills.v1",
	"esi-wallet.read_character_wallet.v1",
	"esi-assets.read_assets.v1",
	"esi-industry.read_character_jobs.v1",
	"esi-markets.read_character_orders.v1",
	"esi-characters.read_blueprints.v1",
	"esi-corporations.read_blueprints.v1",
}

type eveOauth2 struct {
	ssoAuthenticator *goesi.SSOAuthenticator
}

func NewEveAuther(clientID string, clientSecret string, redirectURL string) EveAuther {
	hc := &http.Client{Timeout: 10 * time.Second}
	SSOAuthenticator := goesi.NewSSOAuthenticatorV2(hc, clientID, clientSecret, redirectURL, scopesArr)
	return &eveOauth2{
		ssoAuthenticator: SSOAuthenticator,
	}
}

func (m *eveOauth2) AuthCodeURL(state string) string {
	return m.ssoAuthenticator.AuthorizeURL(state, true, scopesArr)
}

func (m *eveOauth2) Refresh(token *oauth2.Token) (*oauth2.Token, error) {
	src := m.ssoAuthenticator.TokenSource(token)
	return src.Token()
}

func (m *eveOauth2) Exchange(code string) (*EveCreds, error) {
	token, err := m.ssoAuthenticator.TokenExchange(code)
	if err != nil {
		return nil, err
	}

	tokSrc := m.ssoAuthenticator.TokenSource(token)
	v, err := m.ssoAuthenticator.Verify(tokSrc)
	if err != nil {
		return nil, err
	}

	creds := &EveCreds{
		Token:   token,
		Details: v,
	}
	return creds, nil
}

type EveCreds struct {
	Token   *oauth2.Token
	Details *goesi.VerifyResponse
}

type EveClaims struct {
	jwt.RegisteredClaims
	Name string `json:"name"`
}
