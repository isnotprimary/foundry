# [Eve Foundry](https://evefoundry.uk)

A tool to help Eve Online manufacturers pick what to build. It looks at all the owned blueprints of
a character and checks their profitability when buying materials from sell orders and selling
products as buy orders at the same price as the current cheapest on the market.

It is still in early development, bugs can be reported
on [gitlab](https://gitlab.com/isnotprimary/foundry)

