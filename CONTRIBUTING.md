# Contributing

## Set up

Foundry is developed in Go and Typescript. The backend requires a PostgreSQL database. A dev one can
be started using the docker-compose file at the root of the repo:

```shell
docker-compose up -d
```

A copy of the Eve SDE is required. It can be fetched by calling

```shell
./scripts/sde.sh
```

A developer application needs to be created with eve online
here: https://developers.eveonline.com/ . The required scopes can be found in `auth/auth.go`

Some environment variables need to be set, using the values from the application:

```shell
export DEV_MODE=true
export ESI_REDIRECT_URL=http://localhost:8080/api/login/complete
export ESI_CLIENT_ID=
export ESI_SECRET=
export SDE_DIR=${PWD}/foundry/data/sde/
export SESSION_KEY=secret
```

In dev mode, the UI is proxied via the back end, `npm run dev` needs to be running

## Developing

An (incomplete) fake version of the Eve API (ESI) can be found in the `internal` package and can be
used to test API calls with more control.

After database changes (via migrations), run:

```shell
./scripts/schema.sh
```

to update the schema used in tests

Linting can be run with:

```shell
./scripts/lint.sh
```