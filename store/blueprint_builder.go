package store

type Builder struct {
	bp Blueprint
}

func BPFromBase(base BaseBlueprint) Builder {
	bp := Blueprint{
		BaseBlueprint: base,
		ItemID:        200,
		LocationFlag:  "hanger",
		LocationID:    201,
		Quantity:      1,
		Runs:          1,
		Owned:         1,
		StationName:   "unknown",
		AverageVolume: 1,
	}

	return Builder{bp: bp}
}

func BPFromBP(bp Blueprint) Builder {
	return Builder{bp: bp}
}

func (bb Builder) Done() Blueprint {
	return bb.bp
}

func (bb Builder) WithME(me int32) Builder {
	bb.bp.MaterialEfficiency = me
	return bb
}

func (bb Builder) WithTE(te int32) Builder {
	bb.bp.TimeEfficiency = te
	return bb
}

func (bb Builder) WithQuantity(q int32) Builder {
	bb.bp.Quantity = q
	return bb
}

func (bb Builder) WithTypeID(id int32) Builder {
	bb.bp.TypeID = id
	return bb
}

func (bb Builder) WithCostList(cl CostList) Builder {
	bb.bp.Costs = &cl
	return bb
}

func (bb Builder) WithOwned(owned int32) Builder {
	bb.bp.Owned = owned
	return bb
}

func (bb Builder) WithRuns(runs int32) Builder {
	bb.bp.Runs = runs
	return bb
}

func (bb Builder) WithItemID(id int64) Builder {
	bb.bp.ItemID = id
	return bb
}

func (bb Builder) WithMetaGroupID(id int32) Builder {
	bb.bp.MetaGroupID = id
	return bb
}
