package store

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/isnotprimary/foundry/client"
	"gitlab.com/isnotprimary/foundry/models/system"

	"github.com/antihax/goesi/esi"
	"github.com/cenkalti/backoff/v4"
	"github.com/circleci/ex/o11y"
	"github.com/circleci/ex/worker"
	"github.com/rs/zerolog"

	"gitlab.com/isnotprimary/foundry/floats"
	"gitlab.com/isnotprimary/foundry/logging"
)

type CostIndices struct {
	Manufacturing float64 `json:"manufacturing"`
}

func (s *ESIStore) runIndustrySystemsWorker(ctx context.Context) {
	go func() {
		worker.Run(ctx, worker.Config{
			Name:               "prices",
			NoWorkBackOff:      &backoff.ConstantBackOff{Interval: 5 * time.Minute},
			MaxWorkTime:        1 * time.Minute,
			MinWorkTime:        5 * time.Minute,
			WorkFunc:           s.updateIndustrySystems,
			BackoffOnAllErrors: true,
		})
	}()
}

func (s *ESIStore) updateIndustrySystems(ctx context.Context) (err error) {
	_, span := o11y.StartSpan(ctx, "update-industry-systems")
	defer o11y.End(span, &err)
	defer span.AddField("len", len(s.costIndices))
	span.AddRawField(logging.LogLevelKey, zerolog.DebugLevel)

	costIndices, err := s.getIndustrySystems(ctx)

	if errors.Is(err, errAlreadyFresh) {
		span.AddField("cached", true)
		return nil
	}
	if err != nil {
		return err
	}

	s.pricesMu.Lock()
	defer s.pricesMu.Unlock()
	s.costIndices = costIndices
	return nil
}

func (s *ESIStore) getIndustrySystems(ctx context.Context) (map[system.ID]CostIndices, error) {
	systems, resp, err := client.Retry(ctx, func() ([]esi.GetIndustrySystems200Ok, *http.Response, error) {
		return s.client.ESI.IndustryApi.GetIndustrySystems(ctx, nil)
	})

	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error calling ESI: %s", resp.Status)
	}

	cached := resp.Header.Get("X-From-Cache") == "1"
	if cached && s.costIndices != nil {
		return nil, errAlreadyFresh
	}

	costsMap := make(map[system.ID]CostIndices, len(systems))
	for _, sys := range systems {
		for _, index := range sys.CostIndices {
			if index.Activity == "manufacturing" {
				costsMap[system.ID(sys.SolarSystemId)] = CostIndices{Manufacturing: floats.Round(float64(index.CostIndex), 4)}
			}
		}
	}

	return costsMap, nil
}

func (s *ESIStore) CostIndices(system system.ID) (CostIndices, error) {
	s.costIndicesMu.RLock()
	index, ok := s.costIndices[system]
	s.costIndicesMu.RUnlock()
	if !ok {
		return CostIndices{}, fmt.Errorf("unexpected system %d when calculating install costs", system)
	}
	return index, nil
}
