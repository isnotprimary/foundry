package store

import (
	"database/sql/driver"
	"fmt"
)

type Kind int

const (
	KindUnknown Kind = iota
	KindRegion
	KindType
	KindAssets
	KindConstellation
	KindSystem
)

func (k Kind) String() string {
	strings := [...]string{"UNKNOWN", "REGION", "TYPE", "ASSETS", "CONSTELLATION", "SYSTEM"}

	// prevent panicking in case of status is out-of-range
	if k < KindUnknown || k > KindSystem {
		return strings[0]
	}

	return strings[k]
}

// Scan implements sql.Scanner so a State can be read from databases transparently
// Currently, database types that map to string are supported.
func (k *Kind) Scan(src any) error {
	switch src := src.(type) {
	case nil:
		return nil

	case string:
		resource, ok := map[string]Kind{
			"REGION":        KindRegion,
			"TYPE":          KindType,
			"ASSETS":        KindAssets,
			"CONSTELLATION": KindConstellation,
			"SYSTEM":        KindSystem,
		}[src]
		// if an empty Kind comes from a table, we return a null Kind
		if !ok {
			return nil
		}
		*k = resource
	default:
		return fmt.Errorf("scan: unable to scan type %T into Kind", src)
	}

	return nil
}

// Value implements sql.Valuer so that a Kind can be written to databases
// transparently. Class map to strings.
func (k Kind) Value() (driver.Value, error) {
	if k < KindUnknown || k > KindSystem {
		return nil, fmt.Errorf("resource %q: can not create a value", k)
	}
	return k.String(), nil
}
