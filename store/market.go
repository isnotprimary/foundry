package store

import (
	"context"
	"errors"
	"fmt"
	"math"
	"net/http"
	"slices"
	"sync"
	"time"

	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/station"

	"github.com/antihax/goesi/esi"
	"github.com/antihax/goesi/optional"
	"github.com/cenkalti/backoff/v4"
	"github.com/circleci/ex/o11y"
	"github.com/circleci/ex/worker"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"

	"gitlab.com/isnotprimary/foundry/client"
	"gitlab.com/isnotprimary/foundry/floats"
	"gitlab.com/isnotprimary/foundry/hardcoded"
)

type Price struct {
	AdjustedPrice float64 `json:"adjusted_price"`
}

type Order struct {
	VolumeRemain int32
	Price        float64
	StationID    station.ID
}

type MarketData struct {
	// TODO - these 2 orders could likely be merged together
	materialOrders map[int32][]Order
	productOrders  map[int32][]Order
}

func NewMarketData(mo map[int32][]Order, po map[int32][]Order) MarketData {
	return MarketData{
		materialOrders: mo,
		productOrders:  po,
	}
}

func (md *MarketData) GetLowestProductOrder(id int32) float64 {
	orders := md.productOrders[id]
	if len(orders) == 0 {
		return 0
	}
	return orders[0].Price
}

func (md *MarketData) GetStationLowestProductOrder(id int32, sID station.ID) float64 {
	orders := md.productOrders[id]
	for _, order := range orders {
		if order.StationID == sID {
			return order.Price
		}
	}
	return 0
}

func (md *MarketData) MaterialOrders(id int32) []Order {
	return md.materialOrders[id]
}

func (s *ESIStore) runAdjustPricesWorker(ctx context.Context) {
	go func() {
		worker.Run(ctx, worker.Config{
			Name:               "prices",
			NoWorkBackOff:      &backoff.ConstantBackOff{Interval: 5 * time.Minute},
			MaxWorkTime:        1 * time.Minute,
			MinWorkTime:        5 * time.Minute,
			WorkFunc:           s.updateMarketsPrices,
			BackoffOnAllErrors: true,
		})
	}()
}

func (s *ESIStore) updateMarketsPrices(ctx context.Context) error {
	prices, err := s.getMarketsPrices(ctx)

	if errors.Is(err, errAlreadyFresh) {
		return nil
	}
	if err != nil {
		return err
	}

	s.pricesMu.Lock()
	defer s.pricesMu.Unlock()
	s.prices = prices
	return nil
}

var errAlreadyFresh = fmt.Errorf("already up to date")

func (s *ESIStore) getMarketsPrices(ctx context.Context) (map[int32]Price, error) {
	prices, resp, err := s.client.ESI.MarketApi.GetMarketsPrices(ctx, nil)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting calling ESI: %s", resp.Status)
	}

	cached := resp.Header.Get("X-From-Cache") == "1"
	if cached && s.prices != nil {
		return nil, errAlreadyFresh
	}

	priceMap := make(map[int32]Price, len(prices))
	for _, price := range prices {
		priceMap[price.TypeId] = Price{
			AdjustedPrice: price.AdjustedPrice,
		}
	}

	return priceMap, nil
}

func (s *ESIStore) InitMarketData(ctx context.Context, bps []Blueprint, rID region.ID) (MarketData, error) {
	var err error
	_, span := o11y.StartSpan(ctx, "store: init market data")
	defer o11y.End(span, &err)

	mats := make(map[int32]Material)
	products := make(map[int32]Material)
	for _, bp := range bps {
		for _, mat := range bp.Activities.Manufacturing.Materials {
			mats[mat.TypeID] = mat
		}
		prod := bp.Activities.Manufacturing.Products[0]
		products[prod.TypeID] = prod
	}

	eg, ctx := errgroup.WithContext(ctx)

	var matOrders map[int32][]Order
	eg.Go(func() error {
		var err error
		matOrders, err = s.getMaterialOrders(ctx, maps.Values(mats), rID)
		return err
	})

	var productOrders map[int32][]Order
	eg.Go(func() error {
		var err error
		productOrders, err = s.getMaterialOrders(ctx, maps.Values(products), rID)
		return err
	})

	err = eg.Wait()
	if err != nil {
		return MarketData{}, err
	}

	md := NewMarketData(matOrders, productOrders)
	return md, nil
}

func (s *ESIStore) getMaterialOrders(ctx context.Context, mats []Material, rID region.ID) (map[int32][]Order, error) {
	matOrders := make(map[int32][]Order)
	eg, ctx := errgroup.WithContext(ctx)
	eg.SetLimit(11)

	mu := sync.Mutex{}
	eg.Go(func() error { // Outer loop needs to be in errgroup so we wait for it to finish filling the pool
		for _, mat := range mats {
			eg.Go(func() error {
				orders, err := s.sortedOrders(ctx, mat, rID)
				if err != nil {
					return fmt.Errorf("error getting material orders: %w", err)
				}
				mu.Lock()
				defer mu.Unlock()
				matOrders[mat.TypeID] = orders
				return nil
			})
		}
		return nil
	})

	err := eg.Wait()
	if err != nil {
		return nil, err
	}

	return matOrders, nil
}

func (s *ESIStore) enrichWithHistory(ctx context.Context, bp *Blueprint, rID region.ID) error {
	types, err := s.regionTypes(ctx, rID)
	if err != nil {
		return err
	}

	if len(bp.Activities.Manufacturing.Products) == 0 || !types[bp.Activities.Manufacturing.Products[0].TypeID] {
		return nil
	}
	vol, err := s.getHistory(ctx, *bp, rID)
	if err != nil {
		return err
	}

	bp.AverageVolume = float64(vol) / 30
	return nil
}

func (s *ESIStore) getHistory(ctx context.Context, bp Blueprint, rID region.ID) (int64, error) {
	product := bp.Activities.Manufacturing.Products[0] // manufacturing always produces 1
	h, res, err := s.client.ESI.MarketApi.GetMarketsRegionIdHistory(ctx, int32(rID), product.TypeID, nil)
	if err != nil {
		return 0, err
	}
	if res.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("error getting product sales volume from ESI: %s", res.Status)
	}
	if len(h) == 0 {
		return 0, nil
	}

	thirtyDays := thirtyDaysAgo()

	var volume int64
	for i := len(h) - 1; i >= 0; i-- {
		t, err := time.Parse(time.DateOnly, h[i].Date)
		if err != nil {
			return 0, err
		}
		if t.Before(thirtyDays) {
			return volume, nil
		}
		volume += h[i].Volume
	}
	return volume, nil
}

func thirtyDaysAgo() time.Time {
	t := time.Now()
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	return t.Add(-30 * (24 * time.Hour))
}

func (s *ESIStore) regionTypes(ctx context.Context, rID region.ID) (map[int32]bool, error) {
	types := make(map[int32]bool)
	pager := client.NewPaginator(
		func(i int32) ([]int32, *http.Response, error) {
			opts := &esi.GetMarketsRegionIdTypesOpts{
				Page: optional.NewInt32(i),
			}
			return s.client.ESI.MarketApi.GetMarketsRegionIdTypes(ctx, int32(rID), opts)
		})

	for pager.Next() {
		page, err := pager.Page(ctx)
		if err != nil {
			return nil, err
		}
		for _, t := range page {
			types[t] = true
		}
	}
	return types, nil
}

func (s *ESIStore) orders(ctx context.Context, m Material, rID region.ID) ([]Order, error) {
	var orders []Order
	pager := client.NewPaginator(
		func(i int32) ([]esi.GetMarketsRegionIdOrders200Ok, *http.Response, error) {
			opts := &esi.GetMarketsRegionIdOrdersOpts{
				TypeId: optional.NewInt32(m.TypeID),
				Page:   optional.NewInt32(i),
			}
			return s.client.ESI.MarketApi.GetMarketsRegionIdOrders(ctx, "sell", int32(rID), opts)
		})

	for pager.Next() {
		page, err := pager.Page(ctx)
		if err != nil {
			return nil, err
		}
		for _, order := range page {
			// I expect LocationID to always be a station ID,
			// but use NewID for the down casting to be sure
			sID, err := station.NewID(order.LocationId)
			if err != nil {
				return nil, err
			}
			orders = append(orders, Order{
				Price:        order.Price,
				VolumeRemain: order.VolumeRemain,
				StationID:    sID,
			})
		}
	}
	return orders, nil
}

func (s *ESIStore) sortedOrders(ctx context.Context, m Material, rID region.ID) ([]Order, error) {
	orders, err := s.orders(ctx, m, rID)
	if err != nil {
		return nil, err
	}
	slices.SortFunc(orders, func(a, b Order) int {
		return int(a.Price - b.Price)
	})
	return orders, nil
}

func (s *ESIStore) AdjustedPrice(typeID int32) (float64, error) {
	s.pricesMu.RLock()
	defer s.pricesMu.RUnlock()
	adjCost, ok := s.prices[typeID]
	if !ok {
		return 0, fmt.Errorf("unknown type id whilst calculating estimated values")
	}
	return adjCost.AdjustedPrice, nil
}

func (c *Character) SalesTax(price float64) float64 {
	base := price * hardcoded.BaseTaxRatePercentage
	if c.skills.Accounting == 0 {
		return floats.Round(base, 2)
	}

	skillBonus := float64(c.skills.Accounting)
	reduction := base * (hardcoded.AccountTaxRateReduction * skillBonus)
	return floats.Round(base-reduction, 2)
}

func (c *Character) BrokerFee(price float64) float64 {
	if c.skills.BrokerRelations == 0 {
		base := price * hardcoded.BaseBrokerFeePercentage
		return math.Max(hardcoded.BrokerFeeMin, floats.Round(base, 2))
	}

	skillBonus := float64(c.skills.BrokerRelations)
	adjustedRate := hardcoded.BaseBrokerFeePercentage - (hardcoded.BrokerRelationsReduction * skillBonus)
	base := price * adjustedRate
	return math.Max(hardcoded.BrokerFeeMin, floats.Round(base, 2))
}
