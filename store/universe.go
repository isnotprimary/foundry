package store

import (
	"cmp"
	"context"
	"fmt"
	"net/http"
	"slices"

	"golang.org/x/exp/maps"

	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/station"

	"github.com/antihax/goesi/esi"

	"gitlab.com/isnotprimary/foundry/hardcoded"
)

func (s *ESIStore) GetFactories() []station.Station {
	factories := maps.Values(s.factories)
	slices.SortFunc(factories, func(a, b station.Station) int {
		return cmp.Compare(a.Name, b.Name)
	})
	return factories
}

func (s *ESIStore) GetStations() []station.Station {
	sta := maps.Values(s.stations)
	slices.SortFunc(sta, func(a, b station.Station) int {
		return cmp.Compare(a.Name, b.Name)
	})
	return sta
}

func (s *ESIStore) GetStation(id station.ID) (station.Station, bool) {
	sta, ok := s.stations[id]
	return sta, ok
}

func (s *ESIStore) GetFactory(id station.ID) (station.Station, bool) {
	sta, ok := s.factories[id]
	return sta, ok
}

func (s *ESIStore) GetRegions(ctx context.Context) ([]region.Region, error) {
	ids, resp, err := s.client.ESI.UniverseApi.GetUniverseRegions(ctx, nil)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting regions from ESI: %s", resp.Status)
	}

	names, resp, err := s.client.ESI.UniverseApi.PostUniverseNames(ctx, ids, nil)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting names from ESI: %s", resp.Status)
	}

	regions := make([]region.Region, 0, len(names))
	for _, r := range names {
		id, err := region.NewID(r.Id)
		if err != nil {
			return nil, err
		}
		regions = append(regions, region.Region{
			ID:   id,
			Name: r.Name,
		})
	}
	slices.SortFunc(regions, func(a, b region.Region) int {
		return cmp.Compare(a.Name, b.Name)
	})
	return regions, nil
}

const (
	locationStation  = "station"
	locationItem     = "item"
	locationUnlocked = "Unlocked"
	locationLocked   = "Locked"
	locationHanger   = "Hangar"
	locationCargo    = "Cargo"
	locationUnknown  = "unknown"
)

func (s *ESIStore) ResolveLocationID(bp Blueprint, char *Character) (string, int64) {
	lastID := bp.LocationID
	lastLocFlag := bp.LocationFlag
	lastLocType := ""
	if bp.LocationID >= hardcoded.ItemIDMin {
		lastLocType = locationItem
	} else if bp.LocationID >= hardcoded.StationIDLow && bp.LocationID < hardcoded.StationIDHigh {
		lastLocType = locationStation
	} else {
		return locationUnknown, 0
	}
	for i := 0; i < 5; i++ {
		switch lastLocType {
		case locationStation:
			//nolint:gosec // range checked already
			sta, ok := s.stations[station.ID(lastID)]
			if !ok {
				return locationUnknown, 0
			}
			return sta.Name, lastID
		case locationItem:
			switch lastLocFlag {
			case locationUnlocked, locationLocked, locationHanger:
				// The assets and blueprint apis differ in their usage of the location flag
				// In assets something in a hanger would not be in an item, but a station etc
				item, ok := char.assets[lastID]
				if !ok {
					return locationUnknown, 0
				}
				lastID = item.LocationID
				lastLocFlag = item.LocationFlag
				lastLocType = item.LocationType
			case locationCargo:
				return "A ship", 0 // TODO get the name or station or system
			}
		default:
			return locationUnknown, 0
		}
	}
	return locationUnknown, 0
}

// EnrichWithMaterialNames applies material names to the materials. Batches to the names endpoint to reduce api calls
func (s *ESIStore) EnrichWithMaterialNames(ctx context.Context, mCost []MaterialCost) error {
	// mapped by type id to quickly rematch to names
	mats := make(map[int32]*MaterialCost)
	for i, mat := range mCost {
		mats[mat.TypeID] = &mCost[i]
	}

	names, err := s.getNames(ctx, getKeys(mats))
	if err != nil {
		return err
	}

	for k := range mats {
		name := names[k]
		mats[k].Name = name
	}
	return nil
}

// getNames breaks ids into the batches for the post names endpoint,
// it returns names mapped by the provided id
func (s *ESIStore) getNames(ctx context.Context, ids []int32) (map[int32]string, error) {
	names := make(map[int32]string, len(ids))
	batch := 1000

	for i := 0; i < len(ids); i += batch {
		j := i + batch
		if j > len(ids) {
			j = len(ids)
		}

		toGet := ids[i:j]
		res, err := s.names(ctx, toGet)
		if err != nil {
			return nil, err
		}
		for _, name := range res {
			names[name.Id] = name.Name
		}
	}
	return names, nil
}

func (s *ESIStore) names(ctx context.Context, ids []int32) ([]esi.PostUniverseNames200Ok, error) {
	names, res, err := s.client.ESI.UniverseApi.PostUniverseNames(ctx, ids, nil)
	if err != nil {
		return nil, fmt.Errorf("error fetching names: %w", err)
	}
	if res.StatusCode >= 300 {
		return nil, fmt.Errorf("issue fetching names from esi. status code: %d", res.StatusCode)
	}
	return names, nil
}
