package store

import (
	"testing"

	"gitlab.com/isnotprimary/foundry/models/system"

	"github.com/circleci/ex/testing/testcontext"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/testing/fakeesi"
)

func TestStore_getIndustrySystems(t *testing.T) {
	_, eveAPI := fakeesi.NewClient(t)

	s := &ESIStore{
		client: eveAPI,
	}
	ctx := testcontext.Background()

	costs, err := s.getIndustrySystems(ctx)
	assert.NilError(t, err)
	assert.Check(t, cmp.DeepEqual(costs, map[system.ID]CostIndices{
		20000:    {Manufacturing: 0.1},
		20001:    {Manufacturing: 0.2},
		30000119: {Manufacturing: 0.2},
		30000600: {Manufacturing: 0.2},
	}))
}
