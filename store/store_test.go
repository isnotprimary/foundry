//go:build realtest
// +build realtest

package store

import (
	"net/http"
	"testing"
	"time"

	"github.com/antihax/goesi"
	"github.com/circleci/ex/testing/testcontext"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/db"
)

func TestNewStore(t *testing.T) {
	ctx := testcontext.Background()
	dbFix := db.SetupFixture(ctx, t)
	hc := http.Client{Timeout: 10 * time.Second}
	eveAPI := goesi.NewAPIClient(&hc, "eve@che.thisdevice.co.uk")

	s, err := NewStore(eveAPI, dbFix.DB, "../data/sde")
	assert.NilError(t, err)
	assert.Check(t, cmp.Len(s.blueprints, 4713))
	assert.Check(t, cmp.Len(s.factories, 2232))
}
