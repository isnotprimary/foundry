package store

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	"github.com/antihax/goesi/esi"
	"github.com/antihax/goesi/optional"
	"github.com/circleci/ex/o11y"
	"golang.org/x/sync/errgroup"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/client"
	"gitlab.com/isnotprimary/foundry/hardcoded"
)

type Character struct {
	assets     map[int64]Asset
	blueprints []Blueprint
	skills     Skills
}

func NewCharacter(assets map[int64]Asset, blueprints []Blueprint, skills Skills) *Character {
	return &Character{
		assets:     assets,
		blueprints: blueprints,
		skills:     skills,
	}
}

func (c *Character) Skills() Skills {
	return c.skills
}

func (c *Character) Blueprints() []Blueprint {
	return c.blueprints
}

func (c *Character) Assets() map[int64]Asset {
	return c.assets
}

func (s *ESIStore) InitCharacter(ctx context.Context, creds *auth.EveCreds, mos ...MutatorOpt) (*Character, error) {
	var err error
	_, span := o11y.StartSpan(ctx, "store: init character data")
	defer o11y.End(span, &err)

	span.AddField("character", creds.Details.CharacterName)

	char := &Character{}
	eg, egctx := errgroup.WithContext(ctx)

	eg.Go(func() error {
		assets, err := s.assets(egctx, creds)
		if err != nil {
			return err
		}
		char.assets = assets
		return nil
	})

	eg.Go(func() error {
		bps, err := s.characterBPs(egctx, creds, mos...)
		if err != nil {
			return err
		}

		char.blueprints = bps
		return nil
	})

	eg.Go(func() error {
		skills, err := s.skills(egctx, creds)
		if err != nil {
			return err
		}
		char.skills = skills
		return nil
	})

	return char, eg.Wait()
}

func (s *ESIStore) characterBPs(ctx context.Context, creds *auth.EveCreds, mos ...MutatorOpt) ([]Blueprint, error) {
	var bps []Blueprint

	pager := client.NewPaginator(
		func(i int32) ([]esi.GetCharactersCharacterIdBlueprints200Ok, *http.Response, error) {
			opts := &esi.GetCharactersCharacterIdBlueprintsOpts{
				Token: optional.NewString(creds.Token.AccessToken),
				Page:  optional.NewInt32(i),
			}
			return s.client.ESI.CharacterApi.GetCharactersCharacterIdBlueprints(ctx, creds.Details.CharacterID, opts)
		})

	for pager.Next() {
		page, err := pager.Page(ctx)
		if err != nil {
			return nil, err
		}
		storeBPs, err := s.toStoreBPs(page)
		if err != nil {
			return nil, err
		}
		bps = append(bps, storeBPs...)
	}

	bps, err := applyToAll(bps, mos...)
	if err != nil {
		return nil, err
	}

	return bps, nil
}

func applyToAll(bps []Blueprint, mos ...MutatorOpt) ([]Blueprint, error) {
	mu := sync.Mutex{}
	mods := make([]*Blueprint, 0, len(bps))
	eg := errgroup.Group{}
	eg.SetLimit(11)

	for _, bp := range bps {
		eg.Go(func() error {
			if err := applyToOne(&bp, mos...); err != nil {
				if o11y.IsWarning(err) {
					// By returning early we skip appending the bp and so it is filtered
					return nil
				}
				return err
			}
			mu.Lock()
			defer mu.Unlock()
			mods = append(mods, &bp)
			return nil
		})
	}

	err := eg.Wait()
	if err != nil {
		return nil, err
	}

	res := make([]Blueprint, 0, len(mods))
	for _, bp := range mods {
		res = append(res, *bp)
	}
	return res, nil
}

func applyToOne(bp *Blueprint, mos ...MutatorOpt) error {
	for _, mo := range mos {
		if err := mo(bp); err != nil {
			return err
		}
	}
	return nil
}

type CostList struct {
	Total             float64 `json:"total"`
	InstallCost       float64 `json:"install_cost"`
	Possible          bool    `json:"possible"`
	Costs             []Cost  `json:"costs"`
	CurrentSellData   bool    `json:"current_sell_data"`
	SellPrice         float64 `json:"sell_price"`
	Runs              float64 `json:"runs"`
	RunsSell          float64 `json:"runs_sell"`
	Profit            float64 `json:"profit"`
	ProfitPerc        float64 `json:"profit_perc"`
	SalesTax          float64 `json:"sales_tax"`
	BrokerFee         float64 `json:"broker_fee"`
	CostPerUnit       float64 `json:"cost_per_unit"`
	ProfitPerUnit     float64 `json:"profit_per_unit"`
	SalesVolumeProfit float64 `json:"sales_volume_profit"`
}

func (cl *CostList) AddCost(c Cost) {
	cl.Costs = append(cl.Costs, c)
	cl.Total += c.OrderCost
	if !c.Enough {
		cl.Possible = false
	}
}

type Cost struct {
	MEMaterial    Material `json:"me_material"`
	OrderCost     float64  `json:"order_cost"`
	EstimatedCost float64  `json:"estimated_cost"`
	Enough        bool     `json:"enough"`
}

type MaterialCost struct {
	TypeID   int32   `json:"type_id"`
	Name     string  `json:"name"`
	Total    float64 `json:"total"`
	Quantity int32   `json:"quantity"`
	Enough   bool    `json:"enough"`
}

type Blueprint struct {
	BaseBlueprint
	ItemID             int64     `json:"item_id"`
	LocationFlag       string    `json:"-"`
	LocationID         int64     `json:"-"`
	StationName        string    `json:"station_name"`
	StationID          int64     `json:"-"`
	MaterialEfficiency int32     `json:"material_efficiency"`
	Quantity           int32     `json:"quantity"`
	Runs               int32     `json:"runs"`
	TimeEfficiency     int32     `json:"time_efficiency"`
	Costs              *CostList `json:"cost"`
	Owned              int32     `json:"duplicates"`
	AverageVolume      float64   `json:"average_volume"`
}

// BaseBlueprint is the blueprint from the SDE, combining data from multiple files.
type BaseBlueprint struct {
	Name               string     `json:"name"`
	TypeID             int32      `json:"-"`
	Activities         Activities `json:"-"`
	MaxProductionLimit int32      `json:"max_production_limit"`
	MetaGroupID        int32      `json:"meta_group_id"`
}

type Skill struct {
	Level  int32 `json:"level"`
	TypeID int32 `json:"typeID"`
}
type Material struct {
	Quantity int32 `json:"quantity"`
	TypeID   int32 `json:"type_id"`
}
type Manufacturing struct {
	Skills    []Skill    `json:"skills"`
	Materials []Material `json:"materials"`
	Products  []Material `json:"products"`
	Time      int32      `json:"time"`
}
type Activities struct {
	Manufacturing Manufacturing `json:"manufacturing"`
}

func (s *ESIStore) toStoreBPs(esiBPs []esi.GetCharactersCharacterIdBlueprints200Ok) ([]Blueprint, error) {
	bps := make([]Blueprint, len(esiBPs))
	for i, esiBP := range esiBPs {
		base, ok := s.blueprints[esiBP.TypeId]
		if !ok {
			return nil, fmt.Errorf("unable to map blueprint %d", esiBP.TypeId)
		}

		bps[i] = Blueprint{
			BaseBlueprint:      base,
			ItemID:             esiBP.ItemId,
			LocationFlag:       esiBP.LocationFlag,
			LocationID:         esiBP.LocationId,
			MaterialEfficiency: esiBP.MaterialEfficiency,
			Quantity:           esiBP.Quantity,
			Runs:               esiBP.Runs,
			TimeEfficiency:     esiBP.TimeEfficiency,
		}
	}
	return bps, nil
}

func (s *ESIStore) Portrait(ctx context.Context, creds *auth.EveCreds) (
	*esi.GetCharactersCharacterIdPortraitOk, error) {

	portrait, resp, err := s.client.ESI.CharacterApi.GetCharactersCharacterIdPortrait(
		ctx,
		creds.Details.CharacterID,
		nil,
	)

	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error getting portraits from ESI: %s", resp.Status)
	}
	return &portrait, nil
}

type Skills struct {
	BrokerRelations int32
	Accounting      int32
}

func (s *ESIStore) skills(ctx context.Context, creds *auth.EveCreds) (Skills, error) {
	apiSkills, resp, err := s.client.ESI.SkillsApi.GetCharactersCharacterIdSkills(
		ctx,
		creds.Details.CharacterID,
		&esi.GetCharactersCharacterIdSkillsOpts{
			Token: optional.NewString(creds.Token.AccessToken),
		},
	)

	if err != nil {
		return Skills{}, err
	}
	if resp.StatusCode != http.StatusOK {
		return Skills{}, fmt.Errorf("error getting skills from ESI: %s", resp.Status)
	}

	skills := Skills{}
	for _, skill := range apiSkills.Skills {
		// active skill accounts for alpha/omega status
		switch skill.SkillId {
		case hardcoded.AccountingSkillID:
			skills.Accounting = skill.ActiveSkillLevel
		case hardcoded.BrokerRelationsSkillID:
			skills.BrokerRelations = skill.ActiveSkillLevel
		}
	}
	return skills, nil
}

func (s *ESIStore) Blueprints(opts ...MutatorOpt) ([]Blueprint, error) {
	bps := make([]Blueprint, 0, len(s.blueprints))
	for _, bp := range s.blueprints {
		bps = append(bps, Blueprint{
			BaseBlueprint:      bp,
			Quantity:           1,
			MaterialEfficiency: 10,
			Owned:              1,
			Runs:               bp.MaxProductionLimit,
		})
	}
	return applyToAll(bps, opts...)
}
