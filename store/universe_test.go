package store

import (
	"testing"

	"gitlab.com/isnotprimary/foundry/models/station"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestStore_ResolveLocationID(t *testing.T) {
	tests := []struct {
		name     string
		assets   map[int64]Asset
		print    Blueprint
		wantName string
		wantID   int64
	}{
		// This also covers the case where a BP is in use (and so doesn't appear in assets)
		{
			name: "Directly in hanger station 1",
			print: Blueprint{
				LocationID: 60001483,
			},
			wantName: "Station1",
			wantID:   60001483,
		},
		// This also covers the case where a BP is in use (and so doesn't appear in assets)
		{
			name: "Directly in hanger station 2",
			print: Blueprint{
				LocationID: 60001484,
			},
			wantName: "Station2",
			wantID:   60001484,
		},
		{
			name: "In a box",
			print: Blueprint{
				LocationID:   1000000000001,
				LocationFlag: "Unlocked",
			},
			assets: map[int64]Asset{
				1000000000001: {
					LocationID:   60001483,
					LocationFlag: "Hanger",
					LocationType: "station",
				},
			},
			wantName: "Station1",
			wantID:   60001483,
		},
		{
			name: "In a box",
			print: Blueprint{
				LocationID:   1000000000001,
				LocationFlag: "Hangar",
			},
			assets: map[int64]Asset{
				1000000000001: {
					LocationID:   60001483,
					LocationFlag: "Hangar",
					LocationType: "station",
				},
			},
			wantName: "Station1",
			wantID:   60001483,
		},
		{
			name: "In cargo",
			print: Blueprint{
				LocationID:   1000000000001,
				LocationFlag: "Cargo",
			},
			assets: map[int64]Asset{
				1000000000001: {
					LocationID: 60001483,
				},
			},
			wantName: "A ship",
			wantID:   0,
		},
	}

	store := &ESIStore{
		stations: map[station.ID]station.Station{
			60001483: {
				Name:     "Station1",
				ID:       60001483,
				SystemID: 30000119,
			},
			60001484: {
				Name:     "Station2",
				ID:       60001484,
				SystemID: 30000119,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			char := Character{
				assets: tt.assets,
			}
			name, id := store.ResolveLocationID(tt.print, &char)
			assert.Check(t, cmp.Equal(name, tt.wantName))
			assert.Check(t, cmp.Equal(id, tt.wantID))
		})
	}
}
