package store

import (
	"context"
	"slices"
	"sync"

	"github.com/circleci/ex/o11y"

	"gitlab.com/isnotprimary/foundry/models/region"
)

var filterErr = o11y.NewWarning("filtered")

// MutatorOpt takes a blueprint and makes modifications. It returns store.filterErr if it decides the blueprint should
// be filtered. MutatorOpt are applied serially over a blueprint so can make modifications to it without locks. They are
// applied over the entire set of blueprints asynchronously, so internal state should be thread safe
type MutatorOpt func(bp *Blueprint) error

// DedupeOpt deduplicates a blueprint if all fields of store.key matches an already seen blueprint.
// It updates the Owned field accordingly
func DedupeOpt() func(bp *Blueprint) error {
	deduper := blueprintDeduper{seen: make(map[key]*Blueprint)}
	return deduper.dedupe
}

type blueprintDeduper struct {
	mu   sync.Mutex
	seen map[key]*Blueprint
}

type key struct {
	typeID int32
	runs   int32
	ME     int32
	TE     int32
	locID  int64
}

func (b *blueprintDeduper) dedupe(bp *Blueprint) error {
	k := key{
		typeID: bp.TypeID,
		runs:   bp.Runs,
		ME:     bp.MaterialEfficiency,
		TE:     bp.TimeEfficiency,
		locID:  bp.LocationID,
	}
	b.mu.Lock()
	defer b.mu.Unlock()

	uniBp, ok := b.seen[k]
	if !ok {
		uniBp = bp
	}
	uniBp.Owned++
	b.seen[k] = uniBp
	if ok {
		return filterErr
	}
	return nil
}

type metaGroupFilter struct {
	metaGroupLevels []int32
	all             bool
}

const (
	FilterTech1  = "TECH1"
	FilterTech2  = "TECH2"
	FilterAdv    = "ADV"
	FilterStruct = "STRUCT"
	FilterAll    = "ALL"
)

// MetaGroupFilterOpt filters out blueprints that do not match the provided meta group levels e.g tech 1 or 2
func MetaGroupFilterOpt(grp string) func(bp *Blueprint) error {
	var levels []int32
	switch grp {
	case FilterTech1:
		levels = []int32{0, 1}
	case FilterTech2:
		levels = []int32{2}
	case FilterAdv:
		// faction, officer, Tech 3, abysall
		levels = []int32{4, 5, 14, 15}
	case FilterStruct:
		levels = []int32{52, 53, 54}
	case FilterAll:
		fallthrough
	default:
		return metaGroupFilter{all: true}.filter
	}
	return metaGroupFilter{metaGroupLevels: levels}.filter
}

func (m metaGroupFilter) filter(bp *Blueprint) error {
	if m.all {
		return nil
	}
	if !slices.Contains(m.metaGroupLevels, bp.MetaGroupID) {
		return filterErr
	}
	return nil
}

func (s *ESIStore) HistoryOpt(ctx context.Context, rID region.ID) func(bp *Blueprint) error {
	return func(bp *Blueprint) error {
		return s.enrichWithHistory(ctx, bp, rID)
	}
}

func AverageVolumeFilter(vol float64) func(bp *Blueprint) error {
	return func(bp *Blueprint) error {
		if bp.AverageVolume < vol {
			return filterErr
		}
		return nil
	}
}
