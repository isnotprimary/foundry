package store

import (
	"testing"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

func TestMetaGroupFilter(t *testing.T) {
	tests := []struct {
		name   string
		filter string
		keep   map[int32]bool
		all    bool
	}{
		{
			name:   "tech1",
			filter: FilterTech1,
			keep:   map[int32]bool{0: true, 1: true},
		},
		{
			name:   "tech2",
			filter: FilterTech2,
			keep:   map[int32]bool{2: true},
		},
		{
			name:   "adv",
			filter: FilterAdv,
			keep: map[int32]bool{
				4:  true,
				5:  true,
				14: true,
				15: true,
			},
		},
		{
			name:   "structures",
			filter: FilterStruct,
			keep: map[int32]bool{
				52: true,
				53: true,
				54: true,
			},
		},
		{
			name:   "all",
			filter: FilterAll,
			all:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i := 0; i <= 100; i++ {
				//nolint:gosec // i is never more than 100
				bp := BPFromBase(blueprint2).WithMetaGroupID(int32(i)).Done()
				op := MetaGroupFilterOpt(tt.filter)
				err := op(&bp)
				//nolint:gosec // i is never more than 100
				wantFilter := !(tt.all || tt.keep[int32(i)])
				if wantFilter {
					assert.Check(t, cmp.ErrorIs(err, filterErr))
				} else {
					assert.Check(t, err == nil)
				}
			}
		})
	}
}
