package store

import (
	"testing"

	"github.com/antihax/goesi"
	"github.com/circleci/ex/testing/testcontext"
	"github.com/google/go-cmp/cmp/cmpopts"
	"golang.org/x/oauth2"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/testing/fakeesi"
)

var testCreds = &auth.EveCreds{
	Token: &oauth2.Token{
		AccessToken: "token",
	},
	Details: &goesi.VerifyResponse{
		CharacterID: 1,
	},
}

func TestStore_Blueprints(t *testing.T) {
	tests := []struct {
		name    string
		sdeBPs  map[int32]BaseBlueprint
		esiBPs  []fakeesi.Blueprint
		want    []Blueprint
		wantErr bool
	}{
		{
			name: "gets users single blueprint",
			sdeBPs: map[int32]BaseBlueprint{
				2: blueprint2,
			},
			esiBPs: []fakeesi.Blueprint{
				{
					ItemID:             200,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               6,
					TimeEfficiency:     7,
					TypeID:             2,
				},
			},
			want: []Blueprint{
				{
					ItemID:             200,
					BaseBlueprint:      blueprint2,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               6,
					TimeEfficiency:     7,
					Owned:              1,
					AverageVolume:      1,
				},
			},
			wantErr: false,
		},
		{
			name: "gets users multiple blueprints",
			sdeBPs: map[int32]BaseBlueprint{
				2: blueprint2,
				3: blueprint3,
			},
			esiBPs: []fakeesi.Blueprint{
				{
					ItemID:             200,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               6,
					TimeEfficiency:     7,
					TypeID:             2,
				},
				{
					ItemID:             201,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 9,
					Quantity:           8,
					Runs:               7,
					TimeEfficiency:     6,
					TypeID:             2,
				},
				{
					ItemID:             400,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               600,
					TimeEfficiency:     7,
					TypeID:             3,
				},
			},
			want: []Blueprint{
				{
					BaseBlueprint:      blueprint2,
					ItemID:             200,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               6,
					TimeEfficiency:     7,
					Owned:              1,
					AverageVolume:      1,
				},
				{
					BaseBlueprint:      blueprint2,
					ItemID:             201,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 9,
					Quantity:           8,
					Runs:               7,
					TimeEfficiency:     6,
					Owned:              1,
					AverageVolume:      1,
				},
				{
					BaseBlueprint:      blueprint3,
					ItemID:             400,
					LocationFlag:       "hanger",
					LocationID:         201,
					MaterialEfficiency: 4,
					Quantity:           5,
					Runs:               600,
					TimeEfficiency:     7,
					Owned:              1,
					AverageVolume:      1,
				},
			},
			wantErr: false,
		},
	}
	fakeESI, eveAPI := fakeesi.NewClient(t)

	for _, tt := range tests {
		ctx := testcontext.Background()
		t.Run(tt.name, func(t *testing.T) {
			fakeESI.SetBlueprints(1, tt.esiBPs)
			s := &ESIStore{
				client:     eveAPI,
				blueprints: tt.sdeBPs,
			}

			got, err := s.characterBPs(ctx, testCreds, DedupeOpt(), s.HistoryOpt(ctx, 10000002))

			if tt.wantErr {
				assert.Assert(t, err != nil)
			} else {
				assert.NilError(t, err)
				assert.Check(t, cmp.DeepEqual(got, tt.want, cmpopts.SortSlices(sortBlueprints)))
			}
		})
	}
}

func TestStore_applyOps(t *testing.T) {
	tests := []struct {
		name string
		in   []Blueprint
		want []Blueprint
	}{
		{
			name: "does not filter non-dupes",
			in: []Blueprint{
				dupPrint1,
				dupPrint2,
				dupPrint3,
				dupPrint4,
				dupPrint5,
			},
			want: []Blueprint{
				BPFromBP(dupPrint1).WithOwned(1).Done(),
				BPFromBP(dupPrint2).WithOwned(1).Done(),
				BPFromBP(dupPrint3).WithOwned(1).Done(),
				BPFromBP(dupPrint4).WithOwned(1).Done(),
				BPFromBP(dupPrint5).WithOwned(1).Done(),
			},
		},
		{
			name: "does filter dupes",
			in: []Blueprint{
				dupPrint1, dupPrint1,
				dupPrint2, dupPrint2,
				dupPrint3, dupPrint3,
				dupPrint4, dupPrint4,
				dupPrint5, dupPrint5, dupPrint5, dupPrint5, dupPrint5,
				dupPrint5, dupPrint5, dupPrint5, dupPrint5, dupPrint5,
				dupPrint5, dupPrint5, dupPrint5, dupPrint5, dupPrint5,
			},
			want: []Blueprint{
				BPFromBP(dupPrint1).WithOwned(2).Done(),
				BPFromBP(dupPrint2).WithOwned(2).Done(),
				BPFromBP(dupPrint3).WithOwned(2).Done(),
				BPFromBP(dupPrint4).WithOwned(2).Done(),
				BPFromBP(dupPrint5).WithOwned(15).Done(),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			deduper := DedupeOpt()
			got, err := applyToAll(tt.in, deduper)
			assert.Assert(t, err == nil)
			assert.Check(t, cmp.DeepEqual(got, tt.want, cmpopts.SortSlices(sortBlueprints)))
		})
	}
}

func sortBlueprints(a, b Blueprint) bool {
	if a.TypeID != b.TypeID {
		return a.TypeID > b.TypeID
	}
	if a.Runs != b.Runs {
		return a.Runs > b.Runs
	}
	if a.MaterialEfficiency != b.MaterialEfficiency {
		return a.MaterialEfficiency > b.MaterialEfficiency
	}
	if a.TimeEfficiency != b.TimeEfficiency {
		return a.TimeEfficiency > b.TimeEfficiency
	}
	return a.LocationID > b.LocationID
}

var dupPrint1 = Blueprint{
	BaseBlueprint: BaseBlueprint{
		TypeID: 1,
	},
	Runs:               1,
	MaterialEfficiency: 0,
	TimeEfficiency:     0,
	LocationID:         1,
}

var dupPrint2 = Blueprint{
	BaseBlueprint: BaseBlueprint{
		TypeID: 2,
	},
	Runs:               1,
	MaterialEfficiency: 0,
	TimeEfficiency:     0,
	LocationID:         1,
}

var dupPrint3 = Blueprint{
	BaseBlueprint: BaseBlueprint{
		TypeID: 1,
	},
	Runs:               1,
	MaterialEfficiency: 1,
	TimeEfficiency:     0,
	LocationID:         1,
}

var dupPrint4 = Blueprint{
	BaseBlueprint: BaseBlueprint{
		TypeID: 1,
	},
	Runs:               1,
	MaterialEfficiency: 0,
	TimeEfficiency:     1,
	LocationID:         1,
}

var dupPrint5 = Blueprint{
	BaseBlueprint: BaseBlueprint{
		TypeID: 1,
	},
	Runs:               1,
	MaterialEfficiency: 0,
	TimeEfficiency:     0,
	LocationID:         2,
}

var blueprint2 = BaseBlueprint{
	Name:   "Blueprint2",
	TypeID: 2,
	Activities: Activities{
		Manufacturing: Manufacturing{
			Skills: []Skill{
				{
					Level:  5,
					TypeID: 100,
				},
			},
			Materials: []Material{
				{
					Quantity: 6,
					TypeID:   300,
				},
			},
			Products: []Material{
				{
					Quantity: 40,
					TypeID:   400,
				},
			},
			Time: 2,
		},
	},
	MaxProductionLimit: 2,
}

var blueprint3 = BaseBlueprint{
	Name:   "Blueprint3",
	TypeID: 3,
	Activities: Activities{
		Manufacturing: Manufacturing{
			Skills: []Skill{
				{
					Level:  5,
					TypeID: 10,
				},
			},
			Materials: []Material{
				{
					Quantity: 700,
					TypeID:   3000,
				},
			},
			Products: []Material{
				{
					Quantity: 400,
					TypeID:   4000,
				},
			},
			Time: 200,
		},
	},
	MaxProductionLimit: 100,
}
