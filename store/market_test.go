package store

import (
	"testing"
	"time"

	"github.com/circleci/ex/testing/testcontext"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/testing/fakeesi"
)

func TestStore_getMarketPrices(t *testing.T) {
	_, eveAPI := fakeesi.NewClient(t)

	ctx := testcontext.Background()
	s := &ESIStore{
		client: eveAPI,
	}

	prices, err := s.getMarketsPrices(ctx)
	assert.NilError(t, err)
	assert.Check(t, cmp.DeepEqual(prices, map[int32]Price{
		34:   {AdjustedPrice: 210},
		300:  {AdjustedPrice: 110},
		400:  {AdjustedPrice: 100},
		3000: {AdjustedPrice: 110},
	}))
}

func TestStore_getMaterialOrders(t *testing.T) {
	fakeEsi, eveAPI := fakeesi.NewClient(t)

	ctx := testcontext.Background()
	s := &ESIStore{
		client: eveAPI,
	}

	fakeEsi.SetOrders(4000, [][]fakeesi.Order{
		{
			{
				Price:        1000,
				VolumeRemain: 100,
				LocationID:   60000001,
			},
			{
				Price:        1001,
				VolumeRemain: 101,
				LocationID:   60000001,
			},
		},
		{
			{
				Price:        2000,
				VolumeRemain: 200,
				LocationID:   60000001,
			},
		},
	})
	fakeEsi.SetOrders(4001, [][]fakeesi.Order{
		{
			{
				Price:        3000,
				VolumeRemain: 300,
				LocationID:   60000001,
			},
		},
	})
	fakeEsi.SetOrders(4002, [][]fakeesi.Order{
		{},
	})

	mats := []Material{
		{
			TypeID: 4000,
		},
		{
			TypeID: 4001,
		},
		{
			TypeID: 4002,
		},
	}
	prices, err := s.getMaterialOrders(ctx, mats, 10000002)
	assert.NilError(t, err)
	assert.Check(t, cmp.DeepEqual(prices, map[int32][]Order{
		4000: {
			{
				Price:        1000,
				VolumeRemain: 100,
				StationID:    60000001,
			},
			{
				Price:        1001,
				VolumeRemain: 101,
				StationID:    60000001,
			},
			{
				Price:        2000,
				VolumeRemain: 200,
				StationID:    60000001,
			},
		},
		4001: {
			{
				Price:        3000,
				VolumeRemain: 300,
				StationID:    60000001,
			},
		},
		4002: nil,
	}))
}

func TestStore_getMaterialOrders_largeVol(t *testing.T) {
	fakeEsi, eveAPI := fakeesi.NewClient(t)

	ctx := testcontext.Background()
	s := &ESIStore{
		client: eveAPI,
	}
	var mats []Material
	want := make(map[int32][]Order)
	for i := int32(4000); i < 16000; i++ {
		mats = append(mats, Material{
			TypeID: i,
		})
		fakeEsi.SetOrders(int64(i), [][]fakeesi.Order{
			{
				{
					Price:        1000,
					VolumeRemain: 100,
					LocationID:   60000001,
				},
				{
					Price:        1001,
					VolumeRemain: 101,
					LocationID:   60000001,
				},
			},
			{
				{
					Price:        2000,
					VolumeRemain: 200,
					LocationID:   60000001,
				},
			},
		})
		want[i] = []Order{
			{
				Price:        1000,
				VolumeRemain: 100,
				StationID:    60000001,
			},
			{
				Price:        1001,
				VolumeRemain: 101,
				StationID:    60000001,
			},
			{
				Price:        2000,
				VolumeRemain: 200,
				StationID:    60000001,
			},
		}
	}

	prices, err := s.getMaterialOrders(ctx, mats, 10000002)
	assert.NilError(t, err)
	assert.Check(t, cmp.DeepEqual(prices, want))
}

func TestStore_getHistory(t *testing.T) {
	tests := []struct {
		name    string
		history []fakeesi.History
		want    int64
	}{
		{
			name: "all to old",
			history: []fakeesi.History{
				history(33, 1),
				history(32, 1),
				history(31, 1),
			},
			want: 0,
		},
		{
			name: "1 new enough",
			history: []fakeesi.History{
				history(33, 1),
				history(32, 1),
				history(31, 1),
				history(30, 1),
			},
			want: 1,
		},
		{
			name: "2 new enough",
			history: []fakeesi.History{
				history(33, 1),
				history(32, 1),
				history(31, 1),
				history(7, 1),
				history(6, 1),
			},
			want: 2,
		},
		{
			name: "full",
			history: []fakeesi.History{
				history(30, 1),
				history(29, 1),
				history(28, 1),
				history(27, 1),
				history(26, 1),
				history(25, 1),
				history(24, 1),
				history(23, 1),
				history(22, 1),
				history(21, 1),
				history(20, 1),
				history(19, 1),
				history(18, 1),
				history(17, 1),
				history(16, 1),
				history(15, 1),
				history(14, 1),
				history(13, 1),
				history(12, 1),
				history(11, 1),
				history(10, 1),
				history(9, 1),
				history(8, 1),
				history(7, 1),
				history(6, 1),
				history(5, 1),
				history(4, 1),
				history(3, 1),
				history(2, 1),
				history(1, 1),
			},
			want: 30,
		},
		{
			name: "complex",
			history: []fakeesi.History{
				history(50, 1000),
				history(20, 100),
				history(8, 8),
				history(7, 7),
				history(6, 6),
				history(5, 5),
				history(3, 3),
				history(2, 2),
				history(1, 1),
			},
			want: 132,
		},
		{
			name:    "no history",
			history: []fakeesi.History{},
			want:    0,
		},
	}

	bp := Blueprint{}
	bp.Activities.Manufacturing.Products = []Material{
		{
			TypeID: 400,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fake, eveAPI := fakeesi.NewClient(t)

			ctx := testcontext.Background()
			s := &ESIStore{
				client: eveAPI,
			}

			fake.SetHistory(10000002, 400, tt.history)

			got, err := s.getHistory(ctx, bp, 10000002)

			assert.NilError(t, err)
			assert.Check(t, cmp.Equal(got, tt.want))
		})
	}
}

func TestStore_salesTax(t *testing.T) {
	tests := []struct {
		name       string
		price      float64
		accounting int32
		want       float64
	}{
		{
			name:       "base",
			price:      1000,
			accounting: 0,
			want:       80,
		},
		{
			name:       "level 1",
			price:      1000,
			accounting: 1,
			want:       71.2,
		},
		{
			name:       "level 2",
			price:      1000,
			accounting: 2,
			want:       62.4,
		},
		{
			name:       "level 3",
			price:      1000,
			accounting: 3,
			want:       53.6,
		},
		{
			name:       "level 4",
			price:      1000,
			accounting: 4,
			want:       44.8,
		},
		{
			name:       "level 5",
			price:      1000,
			accounting: 5,
			want:       36,
		},
		{
			name:       "base decimal",
			price:      1234.56,
			accounting: 0,
			want:       98.76,
		},
		{
			name:       "level 1 decimal",
			price:      1234.56,
			accounting: 1,
			want:       87.9,
		},
		{
			name:       "level 2 decimal",
			price:      1234.56,
			accounting: 2,
			want:       77.04,
		},
		{
			name:       "level 3 decimal",
			price:      1234.56,
			accounting: 3,
			want:       66.17,
		},
		{
			name:       "level 4 decimal",
			price:      1234.56,
			accounting: 4,
			want:       55.31,
		},
		{
			name:       "level 5 decimal",
			price:      1234.56,
			accounting: 5,
			want:       44.44,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Character{
				skills: Skills{
					Accounting: tt.accounting,
				},
			}

			got := c.SalesTax(tt.price)
			assert.Check(t, cmp.Equal(got, tt.want))
		})
	}
}

func TestStore_BrokerFee(t *testing.T) {
	tests := []struct {
		name            string
		price           float64
		brokerRelations int32
		want            float64
	}{
		{
			name:            "min fee",
			price:           1000,
			brokerRelations: 0,
			want:            100,
		},
		{
			name:            "base",
			price:           10000,
			brokerRelations: 0,
			want:            300,
		},
		{
			name:            "level 1",
			price:           10000,
			brokerRelations: 1,
			want:            270,
		},
		{
			name:            "level 2",
			price:           10000,
			brokerRelations: 2,
			want:            240,
		},
		{
			name:            "level 3",
			price:           10000,
			brokerRelations: 3,
			want:            210,
		},
		{
			name:            "level 4",
			price:           10000,
			brokerRelations: 4,
			want:            180,
		},
		{
			name:            "level 5",
			price:           10000,
			brokerRelations: 5,
			want:            150,
		},
		{
			name:            "base decimal",
			price:           11234.56,
			brokerRelations: 0,
			want:            337.04,
		},
		{
			name:            "level 1 decimal",
			price:           11234.56,
			brokerRelations: 1,
			want:            303.33,
		},
		{
			name:            "level 2 decimal",
			price:           11234.56,
			brokerRelations: 2,
			want:            269.63,
		},
		{
			name:            "level 3 decimal",
			price:           11234.56,
			brokerRelations: 3,
			want:            235.93,
		},
		{
			name:            "level 4 decimal",
			price:           11234.56,
			brokerRelations: 4,
			want:            202.22,
		},
		{
			name:            "level 5 decimal",
			price:           11234.56,
			brokerRelations: 5,
			want:            168.52,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Character{
				skills: Skills{
					BrokerRelations: tt.brokerRelations,
				},
			}

			got := c.BrokerFee(tt.price)
			assert.Check(t, cmp.Equal(got, tt.want))
		})
	}
}

func history(daysOld, volume int64) fakeesi.History {
	return fakeesi.History{
		Date: time.Now().
			Add(-time.Duration(daysOld) * 24 * time.Hour).
			Format(time.DateOnly),
		Volume: volume,
	}
}
