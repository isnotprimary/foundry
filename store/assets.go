package store

import (
	"context"
	"net/http"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/client"

	"github.com/antihax/goesi/esi"
	"github.com/antihax/goesi/optional"
)

type Asset struct {
	ItemID       int64  `json:"item_id"`
	LocationID   int64  `json:"location_id"`
	LocationFlag string `json:"location_flag"`
	LocationType string `json:"location_type"`
	TypeID       int32  `json:"type_id"`
}

func (s *ESIStore) assets(ctx context.Context, creds *auth.EveCreds) (map[int64]Asset, error) {
	assets := make(map[int64]Asset)
	pager := client.NewPaginator[esi.GetCharactersCharacterIdAssets200Ok](
		func(i int32) ([]esi.GetCharactersCharacterIdAssets200Ok, *http.Response, error) {
			opts := &esi.GetCharactersCharacterIdAssetsOpts{
				Token: optional.NewString(creds.Token.AccessToken),
				Page:  optional.NewInt32(i),
			}
			return s.client.ESI.AssetsApi.GetCharactersCharacterIdAssets(ctx, creds.Details.CharacterID, opts)
		},
	)

	for pager.Next() {
		page, err := pager.Page(ctx)
		if err != nil {
			return nil, err
		}
		for _, a := range page {
			assets[a.ItemId] = Asset{
				ItemID:       a.ItemId,
				LocationID:   a.LocationId,
				LocationFlag: a.LocationFlag,
				LocationType: a.LocationType,
				TypeID:       a.TypeId,
			}
		}
	}

	return assets, nil
}
