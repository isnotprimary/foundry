package store

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/station"
	"gitlab.com/isnotprimary/foundry/models/system"

	"github.com/antihax/goesi"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/sde"
)

type Store interface {
	CostIndices(system system.ID) (CostIndices, error)
	InitCharacter(ctx context.Context, creds *auth.EveCreds, mos ...MutatorOpt) (*Character, error)
	InitMarketData(ctx context.Context, bps []Blueprint, rID region.ID) (MarketData, error)
	AdjustedPrice(typeID int32) (float64, error)
	GetFactories() []station.Station
	GetFactory(id station.ID) (station.Station, bool)
	GetStation(id station.ID) (station.Station, bool)
	GetStations() []station.Station
	GetRegions(ctx context.Context) ([]region.Region, error)
	ResolveLocationID(bp Blueprint, char *Character) (string, int64)
	EnrichWithMaterialNames(ctx context.Context, mCost []MaterialCost) error
}

type blueprintMap map[int32]BaseBlueprint

type ESIStore struct {
	client *goesi.APIClient

	blueprints blueprintMap

	stations  map[station.ID]station.Station
	factories map[station.ID]station.Station

	prices   map[int32]Price
	pricesMu sync.RWMutex

	costIndices   map[system.ID]CostIndices
	costIndicesMu sync.RWMutex
}

func NewStore(ctx context.Context, client *goesi.APIClient, sdeDir string) (*ESIStore, error) {
	s := &ESIStore{
		client: client,
	}

	// First runs to ensure we have prices ready to go
	err := s.updateMarketsPrices(ctx)
	if err != nil {
		return nil, err
	}
	err = s.updateIndustrySystems(ctx)
	if err != nil {
		return nil, err
	}
	s.runAdjustPricesWorker(ctx)
	s.runIndustrySystemsWorker(ctx)

	s.blueprints, err = s.asBaseBlueprints(ctx, blueprints)
	if err != nil {
		return nil, err
	}

	bpMeta := make(map[int32]blueprintMap)
	for k, v := range s.blueprints {
		if bpMeta[v.MetaGroupID] == nil {
			bpMeta[v.MetaGroupID] = make(blueprintMap)
		}
		bpMeta[v.MetaGroupID][k] = v
	}

	stations, err := sde.LoadStations(ctx, sdeDir)
	if err != nil {
		return nil, err
	}

	s.stations = make(map[station.ID]station.Station, len(stations))
	s.factories = make(map[station.ID]station.Station)
	for _, sdeStation := range stations {
		sta := station.FromSDEStation(sdeStation)
		s.stations[sta.ID] = sta
		if sta.IsFactory() {
			s.factories[sta.ID] = sta
		}
	}

	return s, nil
}

func getKeys[T any](m map[int32]T) []int32 {
	keys := make([]int32, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

func (s *ESIStore) asBaseBlueprints(ctx context.Context,
	sdeBPs map[int32]sde.Blueprint) (map[int32]BaseBlueprint, error) {

	keys := getKeys(sdeBPs)
	bps := make(map[int32]BaseBlueprint, len(sdeBPs))

	for i := 0; i < len(sdeBPs); i += 1000 {
		batch := 1000
		if len(keys) < 1000 {
			batch = len(keys)
		}
		toFetch := keys[:batch]
		keys = keys[batch:]

		names, resp, err := s.client.ESI.UniverseApi.PostUniverseNames(ctx, toFetch, nil)
		if err != nil {
			return nil, err
		}
		if resp.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("error getting calling ESI: %s", resp.Status)
		}
		for _, name := range names {
			bps[name.Id] = BaseBlueprint{
				Name:               name.Name,
				TypeID:             name.Id,
				Activities:         asStoreActivities(sdeBPs[name.Id].Activities),
				MaxProductionLimit: sdeBPs[name.Id].MaxProductionLimit,
				MetaGroupID:        sdeBPs[name.Id].MetaGroupID,
			}
		}
	}

	return bps, nil
}

func asStoreActivities(activities sde.Activities) (act Activities) {
	skills := make([]Skill, len(activities.Manufacturing.Skills))
	for i, s := range activities.Manufacturing.Skills {
		skills[i] = Skill{
			Level:  s.Level,
			TypeID: s.TypeID,
		}
	}
	materials := make([]Material, len(activities.Manufacturing.Materials))
	for i, m := range activities.Manufacturing.Materials {
		materials[i] = Material{
			Quantity: m.Quantity,
			TypeID:   m.TypeID,
		}
	}
	products := make([]Material, len(activities.Manufacturing.Products))
	for i, p := range activities.Manufacturing.Products {
		products[i] = Material{
			Quantity: p.Quantity,
			TypeID:   p.TypeID,
		}
	}
	return Activities{
		Manufacturing: Manufacturing{
			Skills:    skills,
			Materials: materials,
			Products:  products,
			Time:      activities.Manufacturing.Time,
		},
	}
}
