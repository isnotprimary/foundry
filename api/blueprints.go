package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/blueprints"
	"gitlab.com/isnotprimary/foundry/store"
)

type AllBlueprintsRequest struct {
	locatedRequest
	MetaGroup string `form:"meta_group_id" validate:"required,oneof=TECH1 TECH2 ADV STRUCT"`
}

func (a *API) getAllBlueprints(c *gin.Context) {
	creds := c.MustGet(contextCredsKey).(*auth.EveCreds)

	var req AllBlueprintsRequest
	err := c.BindQuery(&req)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	rID, mID, fID, err := a.locations(req.locatedRequest)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	char, err := a.config.EveStore.InitCharacter(c.Request.Context(), creds)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	opts := []store.MutatorOpt{
		store.MetaGroupFilterOpt(req.MetaGroup),
		a.config.EveStore.HistoryOpt(c.Request.Context(), rID),
		store.AverageVolumeFilter(0.1),
	}

	allBps, err := a.config.EveStore.Blueprints(opts...)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	md, err := a.config.EveStore.InitMarketData(c.Request.Context(), allBps, rID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	coster := blueprints.NewCoster(a.config.EveStore, char, mID)
	bps, err := coster.CostedBlueprints(c.Request.Context(), allBps, md, fID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, bps)
}
