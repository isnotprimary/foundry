package api

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/antihax/goesi"
	"github.com/circleci/ex/testing/testcontext"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/auth"
)

// TODO Replace the mocked author with a full oauth server
func TestAPI_getLogin(t *testing.T) {
	store := cookie.NewStore([]byte("secret"))
	a := New(testcontext.Background(), Config{
		AuthManager: &mockAuther{
			authURL:     "http://localhost:5050",
			redirectURL: "http://localhost:8080",
			code:        "thecode",
		},
		SessionStore: store,
	})

	var state string
	var cookies []*http.Cookie
	t.Run("begin auth session", func(t *testing.T) {
		w, c := makeRequest("/api/login", a, cookies)

		sess := sessions.Default(c)
		var ok bool
		state, ok = sess.Get(sessionStateKey).(string)

		assert.Check(t, cmp.Equal(302, w.Code))
		assert.Assert(t, ok, "session state should be a string")
		assert.Check(t, cmp.Equal(
			w.Header().Get("Location"),
			fmt.Sprintf("http://localhost:5050/?state=%s&redirect_uri=http://localhost:8080", state)),
		)
		cookies = w.Result().Cookies()
	})

	t.Run("auth requires matching state", func(t *testing.T) {
		url := "/api/login/complete?state=notTheState&code=thecode"
		w, c := makeRequest(url, a, cookies)

		a.router.HandleContext(c)
		assert.Check(t, cmp.Equal(400, w.Code))

		sess := sessions.Default(c)
		_, ok := sess.Get(sessionTokenKey).(*oauth2.Token)
		assert.Check(t, !ok)
	})

	t.Run("auth requires a code", func(t *testing.T) {
		url := "/api/login/complete?state=notTheState"
		w, c := makeRequest(url, a, cookies)

		assert.Check(t, cmp.Equal(400, w.Code))

		sess := sessions.Default(c)
		_, ok := sess.Get(sessionTokenKey).(*oauth2.Token)
		assert.Check(t, !ok)
	})

	t.Run("returns error if exchange fails", func(t *testing.T) {
		url := fmt.Sprintf("/api/login/complete?state=%s&code=notThecode", state)
		w, _ := makeRequest(url, a, cookies)

		assert.Check(t, cmp.Equal(401, w.Code))
	})

	t.Run("can finish login", func(t *testing.T) {
		url := fmt.Sprintf("/api/login/complete?state=%s&code=thecode", state)
		w, c := makeRequest(url, a, cookies)

		assert.Check(t, cmp.Equal(307, w.Code))
		assert.Check(t, cmp.Equal(w.Header().Get("Location"), "/"))

		sess := sessions.Default(c)
		tok, ok := sess.Get(sessionTokenKey).(*oauth2.Token)
		assert.Check(t, ok)
		assert.Check(t, cmp.Equal(tok.AccessToken, "90d64460d14870c08c81352a05dedd3465940a7c"))
		assert.Check(t, cmp.Equal(tok.TokenType, "bearer"))

		claim, ok := sess.Get(sessionDetailsKey).(*goesi.VerifyResponse)
		assert.Check(t, ok)
		assert.Check(t, cmp.Equal(claim.CharacterName, "the char"))
	})

}

func makeRequest(url string, a *API, cookies []*http.Cookie) (*httptest.ResponseRecorder, *gin.Context) {
	w := httptest.NewRecorder()
	c := gin.CreateTestContextOnly(w, a.router)
	c.Request, _ = http.NewRequest("GET", url, nil)
	for _, r := range cookies {
		c.Request.AddCookie(r)
	}

	a.router.HandleContext(c)
	return w, c
}

type mockAuther struct {
	authURL     string
	redirectURL string
	code        string
}

func (ma *mockAuther) Refresh(_ *oauth2.Token) (*oauth2.Token, error) {
	//TODO implement me
	panic("implement me")
}

func (ma *mockAuther) AuthCodeURL(state string) string {
	return fmt.Sprintf("%s/?state=%s&redirect_uri=%s", ma.authURL, state, ma.redirectURL)
}
func (ma *mockAuther) Exchange(code string) (*auth.EveCreds, error) {
	if code == ma.code {
		return &auth.EveCreds{
			Token: &oauth2.Token{
				AccessToken: "90d64460d14870c08c81352a05dedd3465940a7c",
				TokenType:   "bearer",
			},
			Details: &goesi.VerifyResponse{
				CharacterID:   4,
				CharacterName: "the char",
			},
		}, nil
	}
	return nil, fmt.Errorf("invalid code")
}
