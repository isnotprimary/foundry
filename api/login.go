package api

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"net/http"

	"github.com/circleci/ex/o11y"

	"github.com/antihax/goesi"

	"github.com/gin-contrib/sessions"
	"golang.org/x/oauth2"

	"gitlab.com/isnotprimary/foundry/auth"

	"github.com/gin-gonic/gin"
)

const (
	contextCredsKey   = "creds"
	sessionTokenKey   = "token"
	sessionDetailsKey = "details"
	sessionStateKey   = "state"
)

func (a *API) getLogin(c *gin.Context) {
	session := sessions.Default(c)

	state := generateState(10)
	session.Set(sessionStateKey, state)
	err := session.Save()
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	url := a.config.AuthManager.AuthCodeURL(state)
	c.Redirect(http.StatusFound, url)
}

func (a *API) getLoginResult(c *gin.Context) {
	session := sessions.Default(c)
	if c.Query("state") != session.Get(sessionStateKey) {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	code := c.Request.URL.Query().Get("code")
	if code == "" {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	creds, err := a.config.AuthManager.Exchange(code)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	session.Delete(sessionStateKey)
	session.Set(sessionTokenKey, creds.Token)
	session.Set(sessionDetailsKey, creds.Details)
	err = session.Save()

	mp := o11y.FromContext(c.Request.Context()).MetricsProvider()
	_ = mp.Count("login_request", 1, []string{
		"login_name:" + creds.Details.CharacterName,
		fmt.Sprintf("login_id:%d", creds.Details.CharacterID)},
		1)

	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.Redirect(http.StatusTemporaryRedirect, "/")
}

func (a *API) getPortrait(c *gin.Context) {
	creds := c.MustGet(contextCredsKey).(*auth.EveCreds)
	portrait, err := a.config.EveStore.Portrait(c.Request.Context(), creds)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, portrait)
}

func (a *API) mustBeAuthenticated(c *gin.Context) {
	session := sessions.Default(c)
	token := session.Get(sessionTokenKey)
	details := session.Get(sessionDetailsKey)
	if token == nil || details == nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	t := token.(oauth2.Token)
	cl := details.(goesi.VerifyResponse)

	fresh, err := a.config.AuthManager.Refresh(&t)
	if err != nil {
		c.Redirect(http.StatusFound, "/api/login")
		return
	}

	//if token was refreshed, update it
	if fresh.AccessToken != t.AccessToken {
		session.Set(sessionTokenKey, fresh)
		err := session.Save()
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
	}

	c.Set(contextCredsKey, &auth.EveCreds{
		Token:   fresh,
		Details: &cl,
	})
	c.Next()
}

func generateState(length int) string {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return ""
	}
	return hex.EncodeToString(b)
}
