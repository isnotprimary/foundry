package api

import (
	"net/http"
	"net/http/httputil"
)

func (a *API) rootHandler() http.Handler {
	if a.dev {
		return &httputil.ReverseProxy{Director: func(req *http.Request) {
			req.URL.Scheme = "http"
			req.URL.Host = "localhost:5173"
		}}
	}
	return http.FileServer(http.Dir("ui/build"))
}
