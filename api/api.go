package api

import (
	"context"
	"encoding/gob"
	"net/http"
	"strings"

	"github.com/circleci/ex/httpserver/ginrouter"

	"github.com/antihax/goesi"
	"github.com/circleci/ex/o11y"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"golang.org/x/oauth2"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/logging"
	"gitlab.com/isnotprimary/foundry/store"
)

type API struct {
	router *gin.Engine
	config Config
	dev    bool
}

type Config struct {
	EsiSecretKey   string
	EsiClientID    string
	EsiRedirectURL string
	SessionStore   sessions.Store
	EveStore       *store.ESIStore
	AuthManager    auth.EveAuther
	DevMode        bool
}

func New(ctx context.Context, cfg Config) *API {
	gin.SetMode(gin.ReleaseMode)
	r := ginrouter.Default(ctx, "eve-foundry")
	r.Use(LogMiddleware(ctx))

	_ = r.SetTrustedProxies(nil)

	api := &API{
		router: r,
		dev:    cfg.DevMode,
		config: cfg,
	}

	//Allow storing of tokens and claims
	gob.Register(oauth2.Token{})
	gob.Register(goesi.VerifyResponse{})

	r.Use(sessions.Sessions("auth-session", api.config.SessionStore))

	if api.dev {
		r.NoRoute(gin.WrapH(api.rootHandler()))
	} else {
		r.NoRoute(gin.WrapH(api.rootHandler()))
		r.GET("/", func(c *gin.Context) {
			c.File("ui/build/index.html")
		})
	}

	unauthed := r.Group("/api")
	unauthed.GET("/login", api.getLogin)
	unauthed.GET("/login/complete", api.getLoginResult)
	unauthed.GET("/regions", api.getRegions)
	unauthed.GET("/factories", api.getFactories)

	authed := r.Group("/api")
	authed.Use(api.mustBeAuthenticated)
	authed.GET("/blueprints", api.getCostedBlueprints)
	authed.GET("/allblueprints", api.getAllBlueprints)
	authed.GET("/portrait", api.getPortrait)

	authed.POST("/buildqueue", api.getCostedBuildQueue)
	return api
}

func (a *API) Handler() http.Handler {
	return a.router.Handler()
}

func LogMiddleware(ctx context.Context) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		_, span := o11y.StartSpan(ctx, "api-request")
		defer o11y.End(span, &err)

		// Process the request
		c.Next()

		if len(c.Errors) > 0 {
			err = c.Errors[0]
		}

		if strings.HasPrefix(c.FullPath(), "/api") {
			span.AddRawField(logging.LogLevelKey, zerolog.InfoLevel)
		}
		span.AddField("status", c.Writer.Status())
		span.AddField("method", c.Request.Method)
		span.AddField("client_ip", c.ClientIP())
		span.AddField("path", c.Request.URL.Path)

		authed := false
		creds, ok := c.Get(contextCredsKey)
		if ok {
			authed = true
			span.AddField("character", creds.(*auth.EveCreds).Details.CharacterName)
		}
		span.AddField("authed", authed)

	}
}
