package api

import (
	"cmp"
	"net/http"
	"slices"

	"github.com/gin-gonic/gin"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/blueprints"
	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/station"
	"gitlab.com/isnotprimary/foundry/store"
)

type locatedRequest struct {
	RegionID  int32 `form:"region" validate:"required_without=market"`
	FactoryID int64 `form:"factory" validate:"required"`
	MarketID  int64 `form:"market" validate:"required_without=region"`
}

type CostRequest struct {
	locatedRequest
	MetaGroup string `form:"meta_group_id" validate:"oneof=TECH1 TECH2 ADV STRUCT ALL"`
}

func (a *API) getCostedBlueprints(c *gin.Context) {
	creds := c.MustGet(contextCredsKey).(*auth.EveCreds)

	var req CostRequest
	err := c.BindQuery(&req)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	rID, mID, fID, err := a.locations(req.locatedRequest)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var opts []store.MutatorOpt
	if req.MetaGroup != "" {
		opts = append(opts, store.MetaGroupFilterOpt(req.MetaGroup))
	}
	opts = append(opts,
		store.DedupeOpt(),
		a.config.EveStore.HistoryOpt(c.Request.Context(), rID),
		store.AverageVolumeFilter(0.1),
	)

	char, err := a.config.EveStore.InitCharacter(c.Request.Context(), creds, opts...)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	md, err := a.config.EveStore.InitMarketData(c.Request.Context(), char.Blueprints(), rID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	coster := blueprints.NewCoster(a.config.EveStore, char, mID)
	bps, err := coster.CostedBlueprints(c.Request.Context(), char.Blueprints(), md, fID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, bps)
}

type BlueprintRuns struct {
	ItemID int64 `json:"item_id"`
	Runs   int32 `json:"runs"`
}

type BuildQueueRequest struct {
	Blueprints []BlueprintRuns `json:"blueprints"`
}

func (a *API) getCostedBuildQueue(c *gin.Context) {
	ctx := c.Request.Context()
	creds := c.MustGet(contextCredsKey).(*auth.EveCreds)

	var req CostRequest
	err := c.BindQuery(&req)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	rID, mID, fID, err := a.locations(req.locatedRequest)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	char, err := a.config.EveStore.InitCharacter(ctx, creds, a.config.EveStore.HistoryOpt(c.Request.Context(), rID))
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	md, err := a.config.EveStore.InitMarketData(ctx, char.Blueprints(), rID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	var body BuildQueueRequest
	err = c.BindJSON(&body)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	coster := blueprints.NewCoster(a.config.EveStore, char, mID)
	bps, mats, err := coster.CostedBuildQueue(md, fID, toIDRunsMap(body.Blueprints))
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	err = a.config.EveStore.EnrichWithMaterialNames(ctx, mats)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	slices.SortFunc(bps, func(a, b store.Blueprint) int {
		if a.Name == b.Name {
			return cmp.Compare(a.ItemID, b.ItemID)
		}
		return cmp.Compare(a.Name, b.Name)
	})
	slices.SortFunc(mats, func(a, b store.MaterialCost) int {
		return cmp.Compare(a.Name, b.Name)
	})

	result := struct {
		Blueprints []store.Blueprint    `json:"blueprints"`
		Materials  []store.MaterialCost `json:"materials"`
	}{
		Blueprints: bps,
		Materials:  mats,
	}

	c.JSON(http.StatusOK, result)
}

func toIDRunsMap(br []BlueprintRuns) map[int64]int32 {
	idRunsMap := make(map[int64]int32, len(br))
	for _, bp := range br {
		idRunsMap[bp.ItemID] = bp.Runs
	}
	return idRunsMap
}

func (a *API) locations(req locatedRequest) (region.ID, station.ID, station.ID, error) {
	var err error
	var regID region.ID
	var markID station.ID

	if req.MarketID != 0 {
		markID, err = station.NewID(req.MarketID)
		if err != nil {
			return 0, 0, 0, err
		}

		sta, _ := a.config.EveStore.GetStation(markID)
		regID = sta.RegionID
	} else {
		regID, err = region.NewID(req.RegionID)
		if err != nil {
			return 0, 0, 0, err
		}
	}

	factID, err := station.NewID(req.FactoryID)
	if err != nil {
		return 0, 0, 0, err
	}

	return regID, markID, factID, err
}
