package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (a *API) getRegions(c *gin.Context) {
	regions, err := a.config.EveStore.GetRegions(c.Request.Context())
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, regions)
}

func (a *API) getFactories(c *gin.Context) {
	fs := a.config.EveStore.GetFactories()

	c.JSON(http.StatusOK, fs)
}
