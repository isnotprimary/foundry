package system

import (
	"fmt"
	"strconv"
)

type ID int32

type System struct {
	ID   ID     `json:"id"`
	Name string `json:"name"`
}

func NewID(id int32) (ID, error) {
	if id < 30000000 || id >= 31000000 {
		return 0, fmt.Errorf("invalid system id %d", id)
	}
	return ID(id), nil
}

func ParseID(raw string) (ID, error) {
	id, err := strconv.ParseInt(raw, 10, 32)
	if err != nil {
		return 0, fmt.Errorf("error system id %s: %w", raw, err)
	}
	return NewID(int32(id))
}
