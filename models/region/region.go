package region

import (
	"fmt"
	"strconv"
)

type ID int32

type Region struct {
	ID   ID     `json:"id" validate:"lte=20000000 gte=10000000"`
	Name string `json:"name"`
}

func NewID(id int32) (ID, error) {
	if id < 10000000 || id >= 20000000 {
		return 0, fmt.Errorf("invalid region id %d", id)
	}
	return ID(id), nil
}

func ParseID(raw string) (ID, error) {
	id, err := strconv.ParseInt(raw, 10, 32)
	if err != nil {
		return 0, fmt.Errorf("error parsing region id %s: %w", raw, err)
	}
	return NewID(int32(id))
}
