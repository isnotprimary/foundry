package station

import (
	"fmt"
	"slices"
	"strconv"

	"gitlab.com/isnotprimary/foundry/hardcoded"
	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/system"
	"gitlab.com/isnotprimary/foundry/sde"
)

type ID int32

type Station struct {
	ID        ID        `json:"id"`
	Name      string    `json:"name"`
	SystemID  system.ID `json:"-"`
	RegionID  region.ID `json:"-"`
	Operation Operation `json:"-"`
}

type Operation struct {
	Services []int32 `json:"-"`
}

func NewID(id int64) (ID, error) {
	if id < 60000000 || id >= 64000000 {
		return 0, fmt.Errorf("invalid station id %d", id)
	}
	return ID(id), nil
}

func (id ID) IsSet() bool {
	return id != 0
}

func FromSDEStation(sdeStation sde.Station) Station {
	return Station{
		ID:        ID(sdeStation.ID),
		Name:      sdeStation.Name,
		SystemID:  system.ID(sdeStation.SystemID),
		RegionID:  region.ID(sdeStation.RegionID),
		Operation: Operation{Services: sdeStation.Operation.Services},
	}
}

func ParseID(raw string) (ID, error) {
	id, err := strconv.ParseInt(raw, 10, 32)
	if err != nil {
		return 0, fmt.Errorf("error parsing station id %s: %w", raw, err)
	}
	return NewID(id)
}

func (s Station) IsFactory() bool {
	return slices.Contains(s.Operation.Services, hardcoded.SDEServiceIDFactory)
}
