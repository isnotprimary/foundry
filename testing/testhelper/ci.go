package testhelper

import (
	"os"
	"strconv"
)

func IsCI() bool {
	ci, ok := os.LookupEnv("CI")
	if ok {
		isCI, err := strconv.ParseBool(ci)
		if err != nil {
			return false
		}
		return isCI
	}
	return false
}
