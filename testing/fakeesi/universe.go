package fakeesi

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Region struct {
	Constellations []int32 `json:"constellations"`
	Description    string  `json:"description"`
	Name           string  `json:"name"`
	RegionID       int32   `json:"region_id"`
}

type Constellation struct {
	ConstellationID int32   `json:"constellation_id"`
	Name            string  `json:"name"`
	RegionID        int32   `json:"region_id"`
	Systems         []int32 `json:"systems"`
}

type System struct {
	ConstellationID int32   `json:"constellation_id"`
	Name            string  `json:"name"`
	SecurityClass   string  `json:"security_class"`
	SecurityStatus  float64 `json:"security_status"`
	Stations        []int32 `json:"stations"`
	SystemID        int32   `json:"system_id"`
}

type Station struct {
	Name      string `json:"name"`
	StationID int    `json:"station_id"`
	SystemID  int    `json:"system_id"`
	TypeID    int    `json:"type_id"`
}

func (esi *FakeESI) regions(ctx *gin.Context) {
	regionIDs := make([]int32, 0, len(regions))
	for i := range regions {
		regionIDs = append(regionIDs, i)
	}
	ctx.JSON(http.StatusOK, regionIDs)
}

func (esi *FakeESI) region(ctx *gin.Context) {
	rIDStr := ctx.Param("region_id")
	rID, err := strconv.ParseInt(rIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	reg, ok := regions[int32(rID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, reg)

}

func (esi *FakeESI) constellations(ctx *gin.Context) {
	cIDs := make([]int32, 0, len(constellations))
	for i := range constellations {
		cIDs = append(cIDs, i)
	}
	ctx.JSON(http.StatusOK, cIDs)
}

func (esi *FakeESI) constellation(ctx *gin.Context) {
	cIDStr := ctx.Param("constellation_id")
	cID, err := strconv.ParseInt(cIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	cons, ok := constellations[int32(cID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}
	ctx.JSON(http.StatusOK, cons)
}

func (esi *FakeESI) systems(ctx *gin.Context) {
	sIDs := make([]int32, 0, len(systems))
	for i := range systems {
		sIDs = append(sIDs, i)
	}
	ctx.JSON(http.StatusOK, sIDs)
}

func (esi *FakeESI) system(ctx *gin.Context) {
	sIDStr := ctx.Param("system_id")
	sID, err := strconv.ParseInt(sIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	system, ok := systems[int32(sID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}
	ctx.JSON(http.StatusOK, system)
}

func (esi *FakeESI) station(ctx *gin.Context) {
	sIDStr := ctx.Param("station_id")
	sID, err := strconv.ParseInt(sIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	station, ok := stations[int32(sID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}
	ctx.JSON(http.StatusOK, station)
}

type Name struct {
	ID       int32  `json:"id"`
	Name     string `json:"name"`
	Category string `json:"category"`
}

func (esi *FakeESI) names(ctx *gin.Context) {
	var ids []int32
	err := ctx.BindJSON(&ids)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	names := make([]Name, len(ids))
	for i, id := range ids {
		reg, ok := regions[id]
		if ok {
			names[i] = Name{
				ID:       reg.RegionID,
				Name:     reg.Name,
				Category: "region",
			}
			continue
		}
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, names)
}

var regions = map[int32]Region{
	10000002: {
		Name:           "The Forge",
		RegionID:       10000002,
		Constellations: []int32{20000017},
	},
	10000033: {
		Name:           "The Citadel",
		RegionID:       10000033,
		Constellations: []int32{20000401},
	},
}

var constellations = map[int32]Constellation{
	20000017: {
		ConstellationID: 20000017,
		Name:            "Ruomo",
		RegionID:        10000002,
		Systems:         []int32{30000119},
	},
	20000401: {
		ConstellationID: 20000401,
		Name:            "Imurukka",
		RegionID:        10000033,
		Systems:         []int32{30002738},
	},
}

var systems = map[int32]System{
	30000119: {
		ConstellationID: 20000017,
		Name:            "Itamo",
		SecurityClass:   "C",
		SecurityStatus:  0.67173832654953,
		Stations:        []int32{60001483},
		SystemID:        30000119,
	},
	30002738: {
		ConstellationID: 20000401,
		Name:            "Inoue",
		SecurityClass:   "C",
		SecurityStatus:  0.5708567500114441,
		Stations:        []int32{60002296},
		SystemID:        30002738,
	},
}

var stations = map[int32]Station{
	60001483: {
		Name:      "Itamo VII - Moon 3 - Rapid Assembly Factory",
		StationID: 60001483,
		SystemID:  30000119,
		TypeID:    4024,
	},
	60002296: {
		Name:      "Inoue IV - Moon 4 - Lai Dai Corporation Warehouse",
		StationID: 60002296,
		SystemID:  30002738,
		TypeID:    1531,
	},
}
