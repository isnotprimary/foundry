package fakeesi

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Asset struct {
	IsBlueprintCopy bool   `json:"is_blueprint_copy,omitempty"`
	IsSingleton     bool   `json:"is_singleton,omitempty"`
	ItemID          int64  `json:"item_id,omitempty"`
	LocationFlag    string `json:"location_flag,omitempty"`
	LocationID      int64  `json:"location_id,omitempty"`
	LocationType    string `json:"location_type,omitempty"`
	Quantity        int32  `json:"quantity,omitempty"`
	TypeID          int32  `json:"type_id,omitempty"`
}

func (esi *FakeESI) characterAssets(ctx *gin.Context) {
	cID := ctx.Value("character_id").(int32)
	page := ctx.Value("page").(int32)

	user, ok := esi.characters[cID]
	if !ok {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}
	ctx.Header("X-Pages", strconv.Itoa(len(user.assets)))
	assets, ok := user.assets[page]
	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}
	ctx.JSON(http.StatusOK, assets)
}

func (esi *FakeESI) SetAssets(cID int32, assets map[int32][]Asset) {
	user, ok := esi.characters[cID]
	if !ok {
		user = User{}
	}
	esi.mu.Lock()
	defer esi.mu.Unlock()

	user.assets = assets
	esi.characters[cID] = user
}
