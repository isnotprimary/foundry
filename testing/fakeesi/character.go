package fakeesi

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Blueprint struct {
	ItemID             int64  `json:"item_id,omitempty"`
	LocationFlag       string `json:"location_flag,omitempty"`
	LocationID         int64  `json:"location_id,omitempty"`
	MaterialEfficiency int32  `json:"material_efficiency,omitempty"`
	Quantity           int32  `json:"quantity,omitempty"`
	Runs               int32  `json:"runs,omitempty"`
	TimeEfficiency     int32  `json:"time_efficiency,omitempty"`
	TypeID             int32  `json:"type_id,omitempty"`
}

func (esi *FakeESI) blueprints(ctx *gin.Context) {
	cIDStr := ctx.Param("character_id")
	cID, err := strconv.ParseInt(cIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	user, ok := esi.characters[int32(cID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	ctx.JSON(http.StatusOK, user.blueprints)
}

func (esi *FakeESI) SetBlueprints(cID int32, bps []Blueprint) {
	user, ok := esi.characters[cID]
	if !ok {
		user = User{}
	}
	esi.mu.Lock()
	defer esi.mu.Unlock()

	user.blueprints = bps
	esi.characters[cID] = user
}
