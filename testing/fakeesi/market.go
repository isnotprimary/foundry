package fakeesi

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Price struct {
	TypeID        int32   `json:"type_id"`
	AdjustedPrice float64 `json:"adjusted_price"`
}

type History struct {
	Volume int64  `json:"volume"`
	Date   string `json:"date"`
}

func (esi *FakeESI) prices(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, prices)
}

type Order struct {
	IsBuyOrder   bool    `json:"is_buy_order"`
	Price        float64 `json:"price"`
	TypeID       int32   `json:"type_id"`
	VolumeRemain int32   `json:"volume_remain"`
	LocationID   int64   `json:"location_id"`
}

func (esi *FakeESI) orders(ctx *gin.Context) {
	rIDStr := ctx.Param("region_id")
	rID, err := strconv.ParseInt(rIDStr, 10, 0)
	if err != nil || rID != 10000002 {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	page := int64(1)
	pageStr := ctx.Query("page")
	if pageStr != "" {
		page, err = strconv.ParseInt(pageStr, 10, 0)
		if err != nil {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
	}
	page--

	typeIDStr := ctx.Query("type_id")
	var ord []Order
	if typeIDStr == "" {
		for _, v := range ordersByType {
			for _, p := range v {
				ord = append(ord, p...)
			}
		}
	} else {
		tID, err := strconv.ParseInt(typeIDStr, 10, 0)
		if err != nil {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}

		ordPages, ok := ordersByType[tID]
		if !ok {
			ctx.AbortWithStatus(http.StatusNotFound)
			return
		}
		if len(ordPages) == 0 {
			ord := make([]Order, 0)
			ctx.JSON(http.StatusOK, ord)
			return
		}
		ord = ordPages[page]

		xPage := fmt.Sprintf("%d", len(ordPages))
		ctx.Header("X-Pages", xPage)
	}

	ctx.JSON(http.StatusOK, ord)
}

func (esi *FakeESI) history(ctx *gin.Context) {
	rIDStr := ctx.Param("region_id")
	rID, err := strconv.ParseInt(rIDStr, 10, 0)
	if err != nil || rID != 10000002 {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	typeIDStr := ctx.Query("type_id")
	var hist []History

	tID, err := strconv.ParseInt(typeIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	var ok bool
	hist, ok = esi.histByRIDAndType[rID][tID]

	if !ok {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	ctx.JSON(http.StatusOK, hist)
}

func regionTypes(c *gin.Context) {
	c.JSON(http.StatusOK, []int32{
		400,
		4000,
	})
}

func (esi *FakeESI) SetHistory(rID, tID int64, hist []History) {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.histByRIDAndType[rID][tID] = hist
}

func (esi *FakeESI) SetOrders(typeID int64, pagedOrders [][]Order) {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	ordersByType[typeID] = pagedOrders
}

var ordersByType = map[int64][][]Order{
	300: {
		{
			{
				IsBuyOrder:   false,
				Price:        66,
				TypeID:       300,
				VolumeRemain: 5,
			},
			{
				IsBuyOrder:   false,
				Price:        10,
				TypeID:       300,
				VolumeRemain: 15,
			},
			{
				IsBuyOrder:   false,
				Price:        70,
				TypeID:       300,
				VolumeRemain: 1,
			},
		},
	},

	400: {
		{
			{
				IsBuyOrder:   false,
				Price:        50,
				TypeID:       400,
				VolumeRemain: 5,
			},
			{
				IsBuyOrder:   false,
				Price:        2.1,
				TypeID:       34,
				VolumeRemain: 15,
			},
			{
				IsBuyOrder:   false,
				Price:        70,
				TypeID:       400,
				VolumeRemain: 1,
			},
		},
	},
	3000: {},
	4000: {},
}

var prices = []Price{
	{
		AdjustedPrice: 100,
		TypeID:        400,
	},
	{
		AdjustedPrice: 210,
		TypeID:        34,
	},
	{
		AdjustedPrice: 110,
		TypeID:        300,
	},
	{
		AdjustedPrice: 110,
		TypeID:        3000,
	},
}
