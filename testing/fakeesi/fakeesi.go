package fakeesi

import (
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type FakeESI struct {
	Router *gin.Engine
	Addr   string

	mu sync.Mutex

	requests []Requests
	//Keyed on character ID
	characters map[int32]User

	histByRIDAndType map[int64]map[int64][]History

	etag    string
	expires *time.Time
}

type User struct {
	blueprints []Blueprint
	skills     Skills
	//Pages of assets
	assets map[int32][]Asset
}
type Requests struct {
	Path string
}

var once sync.Once

func New() *FakeESI {
	once.Do(func() {
		gin.SetMode(gin.ReleaseMode)
	})
	fake := &FakeESI{
		characters: make(map[int32]User, 1),
		etag:       uuid.NewString(),
		histByRIDAndType: map[int64]map[int64][]History{
			10000002: {
				400:  thirtyDayHistory(),
				4000: thirtyDayHistory(),
			},
		},
	}

	r := gin.Default()
	r.Use(lock(fake))
	r.Use(requestStore(fake))
	r.Use(pager())
	r.Use(cache(fake))

	r.GET("/v1/universe/regions", fake.regions)
	r.GET("/v1/universe/regions/:region_id", fake.region)
	r.GET("/v1/universe/constellations", fake.constellations)
	r.GET("/v1/universe/constellations/:constellation_id", fake.constellation)
	r.GET("/v1/universe/systems", fake.systems)
	r.GET("/v4/universe/systems/:system_id", fake.system)
	r.GET("/v2/universe/stations/:station_id", fake.station)
	r.POST("/v3/universe/names/", fake.names)

	r.GET("/v1/markets/prices/", fake.prices)
	r.GET("/v1/markets/:region_id/orders/", fake.orders)
	r.GET("/v1/markets/:region_id/history/", fake.history)
	r.GET("/v1/markets/:region_id/types/", regionTypes)

	r.GET("/v1/industry/systems/", fake.industrySystems)

	authed := r.Group("", auth())
	authed.GET("/v3/characters/:character_id/blueprints/", fake.blueprints)
	authed.GET("/v5/characters/:character_id/assets/", fake.characterAssets)
	authed.GET("/v4/characters/:character_id/skills/", fake.skills)

	fake.Router = r
	return fake
}

func auth() func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		cIDStr := ctx.Param("character_id")
		if cIDStr == "" {
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		cID, err := strconv.ParseInt(cIDStr, 10, 0)
		if err != nil {
			ctx.AbortWithStatus(http.StatusBadRequest)
			return
		}
		ctx.Set("character_id", int32(cID))
	}
}

func lock(esi *FakeESI) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		esi.mu.Lock()
		defer esi.mu.Unlock()
		ctx.Next()
	}
}

func cache(esi *FakeESI) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		ctx.Header("etag", esi.etag)
		t := time.Now().Add(10 * time.Minute)
		if esi.expires != nil {
			t = *esi.expires
		}
		ctx.Header("expires", t.Format(time.RFC1123))

		etag := ctx.Request.Header.Get("If-None-Match")
		if etag != "" && etag == esi.etag {
			ctx.AbortWithStatus(http.StatusNotModified)
		}
	}
}

func pager() func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		pageStr := ctx.Query("page")
		if pageStr != "" {
			page, err := strconv.ParseInt(pageStr, 10, 0)
			if err != nil {
				ctx.AbortWithStatus(http.StatusBadRequest)
				return
			}
			ctx.Set("page", int32(page))
		} else {
			ctx.Set("page", int32(1))
		}
	}
}

func requestStore(esi *FakeESI) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		esi.requests = append(esi.requests, Requests{Path: ctx.Request.URL.Path})
	}
}

func (esi *FakeESI) Reset() {
	esi.ResetRequests()
	esi.ResetExpires()

	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.characters = make(map[int32]User, 1)
}

func (esi *FakeESI) GetRequests() []Requests {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	return esi.requests
}

func (esi *FakeESI) ResetRequests() {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.requests = make([]Requests, 0, 10)
}

func (esi *FakeESI) NewETag() {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.etag = uuid.NewString()
}

func (esi *FakeESI) SetExpires(expires *time.Time) {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.expires = expires
}

func (esi *FakeESI) ResetExpires() {
	esi.mu.Lock()
	defer esi.mu.Unlock()
	esi.expires = nil
}

func thirtyDayHistory() []History {
	hist := make([]History, 0, 30)
	day := -24 * time.Hour
	for i := 0; i < 30; i++ {
		hist = append(hist, History{
			Volume: 1,
			Date:   time.Now().Add(day * time.Duration((i + 1))).Format(time.DateOnly),
		})
	}
	return hist
}
