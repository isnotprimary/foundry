package fakeesi

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type SystemIndices struct {
	SolarSystemID int32         `json:"solar_system_id"`
	CostIndices   []CostIndices `json:"cost_indices"`
}

type CostIndices struct {
	Activity  string  `json:"activity"`
	CostIndex float32 `json:"cost_index"`
}

func (esi *FakeESI) industrySystems(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, systemIndices)
}

var systemIndices = []SystemIndices{
	{
		SolarSystemID: 20000,
		CostIndices: []CostIndices{
			{
				Activity:  "manufacturing",
				CostIndex: 0.1,
			},
		},
	},
	{
		SolarSystemID: 20001,
		CostIndices: []CostIndices{
			{
				Activity:  "manufacturing",
				CostIndex: 0.2,
			},
			{
				Activity:  "copying",
				CostIndex: 0.4,
			},
		},
	},
	{
		SolarSystemID: 30000600,
		CostIndices: []CostIndices{
			{
				Activity:  "manufacturing",
				CostIndex: 0.2,
			},
			{
				Activity:  "copying",
				CostIndex: 0.4,
			},
		},
	},
	{
		SolarSystemID: 30000119,
		CostIndices: []CostIndices{
			{
				Activity:  "manufacturing",
				CostIndex: 0.2,
			},
			{
				Activity:  "copying",
				CostIndex: 0.4,
			},
		},
	},
	{
		SolarSystemID: 20002,
		CostIndices: []CostIndices{
			{
				Activity:  "none",
				CostIndex: 0,
			},
		},
	},
}
