package fakeesi

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Skills struct {
	Skills []Skill `json:"skills"`
}

type Skill struct {
	ActiveSkillLevel int `json:"active_skill_level"`
	SkillID          int `json:"skill_id"`
}

func (esi *FakeESI) skills(ctx *gin.Context) {
	cIDStr := ctx.Param("character_id")
	cID, err := strconv.ParseInt(cIDStr, 10, 0)
	if err != nil {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}
	user, ok := esi.characters[int32(cID)]
	if !ok {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}
	ctx.JSON(http.StatusOK, user.skills)

}

func (esi *FakeESI) SetSkills(cID int32, skills Skills) {
	user, ok := esi.characters[cID]
	if !ok {
		user = User{}
	}

	esi.mu.Lock()
	defer esi.mu.Unlock()
	user.skills = skills

	esi.characters[cID] = user
}
