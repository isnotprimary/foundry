package fakeesi

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/antihax/goesi"
)

func NewClient(t *testing.T) (*FakeESI, *goesi.APIClient) {
	fakeESI := New()
	fakeESIServer := httptest.NewServer(fakeESI.Router.Handler())
	t.Cleanup(func() {
		fakeESIServer.Close()
	})
	hc := http.Client{Timeout: 10 * time.Second}
	eveAPI := goesi.NewAPIClient(&hc, "test client - API access not expected (eve@che.thisdevice.co.uk)")
	eveAPI.ChangeBasePath(fakeESIServer.URL)
	return fakeESI, eveAPI
}
