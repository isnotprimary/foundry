package fakestore

import (
	"context"
	"fmt"

	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/models/region"
	"gitlab.com/isnotprimary/foundry/models/station"
	"gitlab.com/isnotprimary/foundry/models/system"
	"gitlab.com/isnotprimary/foundry/store"
)

type FakeStore struct {
	Stations      map[station.ID]station.Station
	Factories     map[station.ID]station.Station
	SystemIndices map[system.ID]store.CostIndices
	Prices        map[int32]store.Price
}

func (fs *FakeStore) CostIndices(system system.ID) (store.CostIndices, error) {
	c, ok := fs.SystemIndices[system]
	if !ok {
		return store.CostIndices{}, fmt.Errorf("unexpected system %d when calculating install costs", system)
	}
	return c, nil

}

func (fs *FakeStore) InitCharacter(_ context.Context,
	_ *auth.EveCreds, _ ...store.MutatorOpt) (*store.Character, error) {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) InitMarketData(_ context.Context, _ []store.Blueprint, _ region.ID) (store.MarketData, error) {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) AdjustedPrice(typeID int32) (float64, error) {
	p, ok := fs.Prices[typeID]
	if !ok {
		return 0, fmt.Errorf("unknown type id whilst calculating estimated values")
	}

	return p.AdjustedPrice, nil
}

func (fs *FakeStore) SalesTax(_ float64) float64 {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) BrokerFee(_ float64) float64 {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) GetFactories() []station.Station {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) GetStations() []station.Station {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) GetStation(_ station.ID) (station.Station, bool) {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) GetFactory(id station.ID) (station.Station, bool) {
	f, ok := fs.Factories[id]
	return f, ok
}

func (fs *FakeStore) GetRegions(_ context.Context) ([]region.Region, error) {
	//TODO implement me
	panic("implement me")
}

func (fs *FakeStore) ResolveLocationID(_ store.Blueprint, _ *store.Character) (string, int64) {
	return "unknown", 0
}

func (fs *FakeStore) EnrichWithMaterialNames(_ context.Context, _ []store.MaterialCost) error {
	//TODO implement me
	panic("implement me")
}
