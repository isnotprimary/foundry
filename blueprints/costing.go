package blueprints

import (
	"fmt"
	"math"

	"gitlab.com/isnotprimary/foundry/models/system"

	"gitlab.com/isnotprimary/foundry/floats"
	"gitlab.com/isnotprimary/foundry/hardcoded"
	"gitlab.com/isnotprimary/foundry/store"
)

func runs(bp store.Blueprint, preferedRuns int32) float64 {
	//TODO account for 30+1run limit

	var runs float64
	if bp.Runs == -1 {
		runs = float64(bp.MaxProductionLimit)
	} else {
		runs = math.Min(float64(bp.Runs), float64(bp.MaxProductionLimit))
	}

	// check to make sure we have't requested more runs than possible
	if preferedRuns > 0 && float64(preferedRuns) < runs {
		runs = float64(preferedRuns)
	}
	return runs
}

func requiredMaterialsForJob(runs float64, base float64, modifier float64) int32 {
	modPerc := modifier / 100
	unmod := runs * base
	calced := unmod * (1 - modPerc)
	req := math.Max(runs, math.Ceil(calced))
	return int32(req)
}

func (c *Coster) consumeOrders(needed int32, typeID int32, orders []store.Order) store.Cost {
	remaingToFill := needed
	cost := store.Cost{
		MEMaterial: store.Material{
			Quantity: needed,
			TypeID:   typeID,
		},
	}

	var orderCost float64
	for _, order := range orders {
		if c.market.IsSet() && order.StationID != c.market {
			continue
		}
		if order.VolumeRemain >= remaingToFill {
			orderCost += order.Price * float64(remaingToFill)
			cost.Enough = true
			cost.OrderCost = floats.Round(orderCost, 2)
			return cost
		}
		orderCost += order.Price * float64(order.VolumeRemain)
		remaingToFill -= order.VolumeRemain
	}
	return cost
}

func (c *Coster) calculateProfit(bp *store.Blueprint, productPrice float64, sID system.ID) error {

	costList := bp.Costs

	installCost, err := c.installCost(costList.Costs, sID)
	if err != nil {
		return err
	}

	product := bp.Activities.Manufacturing.Products[0] // manufacturing always produces 1
	costList.InstallCost = installCost
	costList.Total += installCost
	costList.SellPrice = productPrice
	costList.RunsSell = (productPrice * float64(product.Quantity)) * costList.Runs

	tax := c.char.SalesTax(costList.RunsSell)
	fee := c.char.BrokerFee(costList.RunsSell)
	costList.SalesTax = tax
	costList.BrokerFee = fee
	costList.Total += tax
	costList.Total += fee
	units := float64(product.Quantity) * costList.Runs
	costList.CostPerUnit = floats.Round(costList.Total/units, 2)

	costList.Profit = floats.Round(costList.RunsSell-costList.Total, 2)
	costList.ProfitPerc = floats.Round((costList.Profit/costList.Total)*100, 2)
	costList.CurrentSellData = true
	costList.ProfitPerUnit = floats.Round(costList.Profit/units, 2)
	costList.SalesVolumeProfit = floats.Round(costList.ProfitPerUnit*bp.AverageVolume, 2)
	bp.Costs = costList
	return nil
}

func (c *Coster) installCost(costs []store.Cost, system system.ID) (float64, error) {
	index, err := c.store.CostIndices(system)
	if err != nil {
		return 0, fmt.Errorf("unexpected system %d when calculating install costs", system)
	}
	var totalEstimatedValue float64
	for _, cost := range costs {
		totalEstimatedValue += cost.EstimatedCost
	}

	jobCost := totalEstimatedValue * index.Manufacturing
	jobCost += totalEstimatedValue * hardcoded.FacilityTaxPercentage // station tax
	jobCost += totalEstimatedValue * hardcoded.FacilittySCCSurchage  // scc surcharge
	return floats.Round(jobCost, 2), nil
}
