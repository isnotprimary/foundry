package blueprints

import (
	"testing"

	"gitlab.com/isnotprimary/foundry/models/station"

	"gitlab.com/isnotprimary/foundry/models/system"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/store"
	"gitlab.com/isnotprimary/foundry/testing/fakestore"
)

func TestRequiredMaterialsForJob(t *testing.T) {
	tests := []struct {
		name     string
		runs     float64
		base     float64
		me       float64
		expected int32
	}{
		{
			name:     "min 1",
			runs:     1,
			base:     1,
			me:       10,
			expected: 1,
		},
		{
			name:     "min 1 per run",
			runs:     10,
			base:     1,
			me:       10,
			expected: 10,
		},
		{
			name:     "me 10 base 100 1 run",
			runs:     1,
			base:     100,
			me:       10,
			expected: 90,
		},
		{
			name:     "me 10 base 100 10 run",
			runs:     10,
			base:     100,
			me:       10,
			expected: 900,
		},
		{
			name:     "results rounded up",
			runs:     10,
			base:     99,
			me:       4,
			expected: 951,
		},
		{
			name:     "me 10 base 100 10 run",
			runs:     10,
			base:     100,
			me:       10,
			expected: 900,
		},
		{
			name:     "Shuttle example",
			runs:     3,
			base:     2778,
			me:       3,
			expected: 8084,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := requiredMaterialsForJob(tt.runs, tt.base, tt.me)
			assert.Check(t, cmp.Equal(result, tt.expected))
		})
	}
}

func TestInstallCost(t *testing.T) {
	var defCostIndices = map[system.ID]store.CostIndices{
		100001: {
			Manufacturing: 0.001,
		},
	}
	type args struct {
		costs  []store.Cost
		system system.ID
	}
	tests := []struct {
		name        string
		costIndices map[system.ID]store.CostIndices
		args        args
		want        float64
	}{
		{
			name:        "One item",
			costIndices: defCostIndices,
			args: args{
				costs: []store.Cost{
					{
						EstimatedCost: 1000,
					},
				},
				system: 100001,
			},
			want: 43.5,
		},
		{
			name:        "two items",
			costIndices: defCostIndices,
			args: args{
				costs: []store.Cost{
					{
						EstimatedCost: 1000,
					},
					{
						EstimatedCost: 1000,
					},
				},
				system: 100001,
			},
			want: 87,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &fakestore.FakeStore{
				SystemIndices: tt.costIndices,
			}
			c := Coster{
				store: s,
			}
			got, err := c.installCost(tt.args.costs, tt.args.system)
			assert.NilError(t, err)
			assert.Check(t, cmp.Equal(got, tt.want))
		})
	}
}

func TestConsumeOrders(t *testing.T) {
	tests := []struct {
		name   string
		orders []store.Order
		needed int32
		typeID int32
		market station.ID
		want   store.Cost
	}{
		{
			name: "simple",
			orders: []store.Order{
				{
					VolumeRemain: 100,
					Price:        10,
				},
			},
			needed: 100,
			typeID: 1,

			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 100,
					TypeID:   1,
				},
				Enough:    true,
				OrderCost: 1000,
			},
		},
		{
			name: "station",
			orders: []store.Order{
				{
					VolumeRemain: 100,
					Price:        9,
					StationID:    station.ID(60000001),
				},
				{
					VolumeRemain: 100,
					Price:        10,
					StationID:    station.ID(60000002),
				},
			},
			needed: 100,
			typeID: 1,
			market: station.ID(60000002),
			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 100,
					TypeID:   1,
				},
				Enough:    true,
				OrderCost: 1000,
			},
		},
		{
			name: "multiple required orders",
			orders: []store.Order{
				{
					VolumeRemain: 99,
					Price:        10,
				},
				{
					VolumeRemain: 1,
					Price:        11,
				},
			},
			needed: 100,
			typeID: 2,

			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 100,
					TypeID:   2,
				},
				Enough:    true,
				OrderCost: 1001,
			},
		},
		{
			name: "float",
			orders: []store.Order{
				{
					VolumeRemain: 25,
					Price:        1.1,
				},
			},
			needed: 25,
			typeID: 1,

			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 25,
					TypeID:   1,
				},
				Enough:    true,
				OrderCost: 27.50,
			},
		},

		{
			name: "floats",
			orders: []store.Order{
				{
					VolumeRemain: 25,
					Price:        1.1,
				},
				{
					VolumeRemain: 25,
					Price:        1.2,
				},
				{
					VolumeRemain: 25,
					Price:        1.01,
				},
				{
					VolumeRemain: 25,
					Price:        1.02,
				},
			},
			needed: 100,
			typeID: 1,

			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 100,
					TypeID:   1,
				},
				Enough:    true,
				OrderCost: 108.25,
			},
		},
		{
			name: "not enough orders",
			orders: []store.Order{
				{
					VolumeRemain: 25,
					Price:        1.1,
				},
			},
			needed: 100,
			typeID: 1,

			want: store.Cost{
				MEMaterial: store.Material{
					Quantity: 100,
					TypeID:   1,
				},
				Enough:    false,
				OrderCost: 0,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := Coster{
				market: tt.market,
			}
			got := c.consumeOrders(tt.needed, tt.typeID, tt.orders)
			assert.Check(t, cmp.DeepEqual(tt.want, got))
		})
	}
}
