package blueprints

import (
	"context"
	"fmt"
	"slices"

	"github.com/circleci/ex/o11y"
	"github.com/rs/zerolog"

	"gitlab.com/isnotprimary/foundry/logging"
	"gitlab.com/isnotprimary/foundry/models/station"

	"gitlab.com/isnotprimary/foundry/store"
)

func (c *Coster) CostedBlueprints(ctx context.Context,
	bps []store.Blueprint, md store.MarketData, sID station.ID) ([]store.Blueprint, error) {

	sys, ok := c.store.GetFactory(sID)
	if !ok {
		return nil, fmt.Errorf("%d not a factory", sID)
	}

	possible := bps[:0]
	for _, bp := range bps {
		bp := bp
		costs := c.costMaterials(ctx, bp, md)

		if costs.Possible {
			// manufacturing always produces 1
			product := bp.Activities.Manufacturing.Products[0].TypeID
			var price float64
			if c.market.IsSet() {
				price = md.GetStationLowestProductOrder(product, c.market)
			} else {
				price = md.GetLowestProductOrder(product)
			}

			if price == 0 { // skip items we have no price for
				continue
			}

			bp.Costs = costs
			err := c.calculateProfit(&bp, price, sys.SystemID)
			if err != nil {
				return nil, err
			}

			name, id := c.store.ResolveLocationID(bp, c.char)
			bp.StationName = name
			bp.StationID = id
			possible = append(possible, bp)
		}
	}

	slices.SortFunc(possible, func(a, b store.Blueprint) int {
		// Reversed for descending sort
		return int(b.Costs.SalesVolumeProfit - a.Costs.SalesVolumeProfit)
	})
	return possible, nil
}

func (c *Coster) costMaterials(ctx context.Context, bp store.Blueprint, md store.MarketData) *store.CostList {
	var costList store.CostList
	costList.Possible = true

	mats := bp.Activities.Manufacturing.Materials
	runs := runs(bp, 0)
	costList.Runs = runs

	for _, mat := range mats {
		orders := md.MaterialOrders(mat.TypeID)
		needed := requiredMaterialsForJob(runs, float64(mat.Quantity), float64(bp.MaterialEfficiency))
		cost := c.consumeOrders(needed, mat.TypeID, orders)

		adj, err := c.store.AdjustedPrice(mat.TypeID)
		if err != nil {
			costList.Possible = false
			o11y.LogError(ctx, "adjusted-price", err,
				o11y.Field(logging.LogLevelKey, zerolog.WarnLevel),
				o11y.Field("typeID", mat.TypeID),
			)
			return &costList
		}
		cost.EstimatedCost = adj * float64(needed)

		costList.AddCost(cost)
	}

	return &costList
}
