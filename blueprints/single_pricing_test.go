package blueprints

import (
	"testing"

	"github.com/circleci/ex/testing/testcontext"

	"gitlab.com/isnotprimary/foundry/models/station"

	"github.com/google/go-cmp/cmp/cmpopts"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/store"
	"gitlab.com/isnotprimary/foundry/testing/fakeesi"
	"gitlab.com/isnotprimary/foundry/testing/fakestore"
)

func TestStore_CostedBlueprints(t *testing.T) {
	bp := store.BPFromBase(blueprint2).
		WithME(4).
		WithQuantity(5).
		WithTE(7).
		WithTypeID(2).
		WithRuns(6).
		Done()

	tests := []struct {
		name    string
		sdeBPs  map[int32]store.BaseBlueprint
		esiBPs  []fakeesi.Blueprint
		inBPs   []store.Blueprint
		want    []store.Blueprint
		wantErr bool
	}{
		{
			name: "gets users single blueprint",
			inBPs: []store.Blueprint{
				store.BPFromBP(bp).Done(),
			},
			want: []store.Blueprint{
				store.BPFromBP(bp).
					WithCostList(store.CostList{
						Total:       553.54,
						InstallCost: 320.1,
						Possible:    true,
						Runs:        2,
						Costs: []store.Cost{{
							MEMaterial: store.Material{
								Quantity: 12,
								TypeID:   300,
							},
							OrderCost:     120,
							EstimatedCost: 1320,
							Enough:        true,
						}},
						SellPrice:         2.1,
						RunsSell:          168,
						Profit:            -385.54,
						ProfitPerc:        -69.65,
						SalesTax:          13.44,
						BrokerFee:         100,
						CostPerUnit:       6.92,
						ProfitPerUnit:     -4.82,
						SalesVolumeProfit: -4.82,
						CurrentSellData:   true,
					}).
					Done(),
			},
			wantErr: false,
		},
		{
			name: "supports BPOs",
			inBPs: []store.Blueprint{
				store.BPFromBP(bp).WithRuns(-1).Done(),
			},
			want: []store.Blueprint{
				store.BPFromBP(bp).
					WithRuns(-1).
					WithCostList(store.CostList{
						Total:       553.54,
						InstallCost: 320.1,
						Possible:    true,
						Runs:        2,
						Costs: []store.Cost{{
							MEMaterial: store.Material{
								Quantity: 12,
								TypeID:   300,
							},
							OrderCost:     120,
							EstimatedCost: 1320,
							Enough:        true,
						}},
						SellPrice:         2.1,
						RunsSell:          168,
						Profit:            -385.54,
						ProfitPerc:        -69.65,
						SalesTax:          13.44,
						BrokerFee:         100,
						CostPerUnit:       6.92,
						ProfitPerUnit:     -4.82,
						SalesVolumeProfit: -4.82,
						CurrentSellData:   true,
					}).
					Done(),
			},
			wantErr: false,
		},
		{
			name: "no orders available",
			inBPs: []store.Blueprint{
				store.BPFromBase(blueprint3).Done(),
			},
			want:    []store.Blueprint{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := testcontext.Background()
			s := &fakestore.FakeStore{
				Factories: map[station.ID]station.Station{60001483: {
					ID:       60001483,
					SystemID: 30000119,
				}},
				SystemIndices: systemIndices,
				Prices: map[int32]store.Price{
					300: {
						AdjustedPrice: 110,
					},
					3000: {
						AdjustedPrice: 0,
					},
				},
			}
			c := Coster{
				store: s,
				char:  store.NewCharacter(nil, tt.inBPs, store.Skills{}),
			}
			md := store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						Price:        10,
						VolumeRemain: 15,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        2.1,
					},
				}},
			)

			got, err := c.CostedBlueprints(ctx, c.char.Blueprints(), md, 60001483)

			if tt.wantErr {
				assert.Assert(t, err != nil)
			} else {
				assert.NilError(t, err)
				assert.Check(t, cmp.DeepEqual(got, tt.want, cmpopts.SortSlices(sortBlueprints)))
			}
		})
	}
}
