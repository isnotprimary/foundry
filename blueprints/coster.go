package blueprints

import (
	"gitlab.com/isnotprimary/foundry/models/station"
	"gitlab.com/isnotprimary/foundry/store"
)

type Coster struct {
	store  store.Store
	char   *store.Character
	market station.ID
}

func NewCoster(s store.Store, c *store.Character, market station.ID) *Coster {
	return &Coster{
		store:  s,
		char:   c,
		market: market,
	}
}
