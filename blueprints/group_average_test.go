package blueprints

import (
	"testing"

	"gitlab.com/isnotprimary/foundry/models/station"
	"gitlab.com/isnotprimary/foundry/models/system"

	"github.com/google/go-cmp/cmp/cmpopts"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"

	"gitlab.com/isnotprimary/foundry/store"
	"gitlab.com/isnotprimary/foundry/testing/fakestore"
)

func TestStore_CostedBuildQueue(t *testing.T) {
	defCostList := store.CostList{
		Total:             133.35,
		InstallCost:       14.55,
		Possible:          true,
		Runs:              1,
		SellPrice:         4,
		RunsSell:          160,
		Profit:            26.65,
		ProfitPerc:        19.99,
		SalesTax:          12.8,
		BrokerFee:         100,
		CostPerUnit:       3.33,
		ProfitPerUnit:     0.67,
		SalesVolumeProfit: 0.67,
		CurrentSellData:   true,
		Costs: []store.Cost{{
			MEMaterial: store.Material{
				Quantity: 6,
				TypeID:   300,
			},
			OrderCost:     6,
			EstimatedCost: 60,
			Enough:        true,
		}},
	}

	tests := []struct {
		name    string
		inBps   []store.Blueprint
		inQueue map[int64]int32
		inMd    store.MarketData
		station station.ID
		want    []store.Blueprint
		wantErr bool
	}{
		{
			name: "gets users single blueprint",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).Done(),
			},
			inQueue: map[int64]int32{200: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 6,
						Price:        1,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).WithCostList(defCostList).Done(),
			},
			wantErr: false,
		},
		{
			name: "gets users single blueprint for market",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).Done(),
			},
			station: station.ID(60000001),
			inQueue: map[int64]int32{200: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 6,
						Price:        2,
						StationID:    station.ID(60000002),
					},
					{
						VolumeRemain: 6,
						Price:        1,
						StationID:    station.ID(60000001),
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        3,
						StationID:    station.ID(60000002),
					},
					{
						VolumeRemain: 6,
						Price:        4,
						StationID:    station.ID(60000001),
					},
				}},
			),
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).WithCostList(defCostList).Done(),
			},
			wantErr: false,
		},
		{
			name: "includes multiple selected blueprints",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).Done(),
				store.BPFromBase(blueprint2).WithItemID(201).Done(),
			},
			inQueue: map[int64]int32{200: 0, 201: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 12,
						Price:        1,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).WithCostList(defCostList).Done(),
				store.BPFromBase(blueprint2).WithItemID(201).WithCostList(defCostList).Done(),
			},
			wantErr: false,
		},
		{
			name: "only selected blueprints are used",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).Done(),
				store.BPFromBase(blueprint2).WithItemID(201).Done(),
			},
			inQueue: map[int64]int32{200: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 12,
						Price:        1,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).WithCostList(defCostList).Done(),
			},
			wantErr: false,
		},
		{
			name: "supports BPOs",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).WithRuns(-1).Done(),
			},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 12,
						Price:        1,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			inQueue: map[int64]int32{200: 0},
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).
					WithRuns(-1).
					WithCostList(
						store.CostList{
							Total:             166.7,
							InstallCost:       29.1,
							Possible:          true,
							Runs:              2,
							SellPrice:         4,
							RunsSell:          320,
							Profit:            153.3,
							ProfitPerc:        91.96,
							SalesTax:          25.6,
							BrokerFee:         100,
							CostPerUnit:       2.08,
							ProfitPerUnit:     1.92,
							SalesVolumeProfit: 1.92,
							CurrentSellData:   true,
							Costs: []store.Cost{{
								MEMaterial: store.Material{
									Quantity: 12,
									TypeID:   300,
								},
								OrderCost:     12,
								EstimatedCost: 120,
								Enough:        true,
							}},
						}).
					Done(),
			},
			wantErr: false,
		},
		{
			name: "handles ME",
			inBps: []store.Blueprint{
				store.
					BPFromBase(blueprint3).
					WithME(5).
					Done(),
			},
			inQueue: map[int64]int32{200: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{3000: {
					{
						VolumeRemain: 700,
						Price:        1,
					},
				}},
				map[int32][]store.Order{4000: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			want: []store.Blueprint{
				store.
					BPFromBase(blueprint3).
					WithME(5).
					WithCostList(
						store.CostList{
							Total:             2505.63,
							InstallCost:       1612.63,
							Possible:          true,
							Runs:              1,
							SellPrice:         4,
							RunsSell:          1600,
							Profit:            -905.63,
							ProfitPerc:        -36.14,
							SalesTax:          128,
							BrokerFee:         100,
							CostPerUnit:       6.26,
							ProfitPerUnit:     -2.26,
							SalesVolumeProfit: -2.26,
							CurrentSellData:   true,
							Costs: []store.Cost{{
								MEMaterial: store.Material{
									Quantity: 665,
									TypeID:   3000,
								},
								OrderCost:     665,
								EstimatedCost: 6650,
								Enough:        true,
							}},
						}).
					Done(),
			},
			wantErr: false,
		},
		{
			name: "handles not enough materials",
			inBps: []store.Blueprint{
				store.BPFromBase(blueprint2).Done(),
				store.BPFromBase(blueprint3).WithItemID(202).Done(),
			},
			inQueue: map[int64]int32{200: 0, 202: 0},
			inMd: store.NewMarketData(
				map[int32][]store.Order{300: {
					{
						VolumeRemain: 6,
						Price:        1,
					},
				}},
				map[int32][]store.Order{400: {
					{
						VolumeRemain: 6,
						Price:        4,
					},
				}},
			),
			want: []store.Blueprint{
				store.BPFromBase(blueprint2).WithCostList(defCostList).Done(),
				store.BPFromBase(blueprint3).
					WithItemID(202).
					WithCostList(store.CostList{
						Possible: false,
						Runs:     1,
						Costs: []store.Cost{
							{
								MEMaterial: store.Material{
									TypeID:   3000,
									Quantity: 700,
								},
								Enough:        false,
								EstimatedCost: 7000,
							},
						},
					}).
					Done(),
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &fakestore.FakeStore{
				Factories: map[station.ID]station.Station{60001483: {
					ID:       60001483,
					SystemID: 30000119,
				}},
				SystemIndices: systemIndices,
				Prices: map[int32]store.Price{
					300: {
						AdjustedPrice: 10,
					},
					3000: {
						AdjustedPrice: 10,
					},
				},
			}
			c := Coster{
				store:  s,
				market: tt.station,
				char:   store.NewCharacter(nil, tt.inBps, store.Skills{}),
			}
			got, _, err := c.CostedBuildQueue(tt.inMd, station.ID(60001483), tt.inQueue)

			if tt.wantErr {
				assert.Assert(t, err != nil)
			} else {
				assert.NilError(t, err)
				assert.Check(t, cmp.DeepEqual(got, tt.want, cmpopts.SortSlices(sortBlueprints)))
			}
		})
	}
}

func sortBlueprints(a, b store.Blueprint) bool {
	if a.TypeID != b.TypeID {
		return a.TypeID > b.TypeID
	}
	if a.Runs != b.Runs {
		return a.Runs > b.Runs
	}
	if a.MaterialEfficiency != b.MaterialEfficiency {
		return a.MaterialEfficiency > b.MaterialEfficiency
	}
	if a.TimeEfficiency != b.TimeEfficiency {
		return a.TimeEfficiency > b.TimeEfficiency
	}
	return a.LocationID > b.LocationID
}

var blueprint2 = store.BaseBlueprint{
	Name:   "Blueprint2",
	TypeID: 2,
	Activities: store.Activities{
		Manufacturing: store.Manufacturing{
			Skills: []store.Skill{
				{
					Level:  5,
					TypeID: 100,
				},
			},
			Materials: []store.Material{
				{
					Quantity: 6,
					TypeID:   300,
				},
			},
			Products: []store.Material{
				{
					Quantity: 40,
					TypeID:   400,
				},
			},
			Time: 2,
		},
	},
	MaxProductionLimit: 2,
}

var blueprint3 = store.BaseBlueprint{
	Name:   "Blueprint3",
	TypeID: 3,
	Activities: store.Activities{
		Manufacturing: store.Manufacturing{
			Skills: []store.Skill{
				{
					Level:  5,
					TypeID: 10,
				},
			},
			Materials: []store.Material{
				{
					Quantity: 700,
					TypeID:   3000,
				},
			},
			Products: []store.Material{
				{
					Quantity: 400,
					TypeID:   4000,
				},
			},
			Time: 200,
		},
	},
	MaxProductionLimit: 100,
}

var systemIndices = map[system.ID]store.CostIndices{
	20000: {
		Manufacturing: 0.1,
	},
	20001: {
		Manufacturing: 0.2,
	},
	30000600: {
		Manufacturing: 0.2,
	},
	30000119: {
		Manufacturing: 0.2,
	},
	20002: {},
}
