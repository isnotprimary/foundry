package blueprints

import (
	"fmt"

	"golang.org/x/exp/maps"

	"gitlab.com/isnotprimary/foundry/models/station"

	"gitlab.com/isnotprimary/foundry/store"
)

type idRunMap map[int64]int32

func (c *Coster) CostedBuildQueue(md store.MarketData, sID station.ID, queue idRunMap) (
	[]store.Blueprint, []store.MaterialCost, error) {

	sys, ok := c.store.GetFactory(sID)
	if !ok { // TODO validate this earlier
		return nil, nil, fmt.Errorf("%d not a factory", sID)
	}

	bps := c.selectedBPs(c.char.Blueprints(), queue)

	needs := materialsNeeded(bps)
	matCosts := c.groupMaterialCost(needs, md)
	err := c.avgBlueprintCostings(bps, matCosts, md, sys)

	if err != nil {
		return nil, nil, err
	}

	return bps, maps.Values(matCosts), nil
}

func (c *Coster) selectedBPs(bps []store.Blueprint, queue idRunMap) []store.Blueprint {
	queuedBPs := make([]store.Blueprint, 0, len(queue))
	for _, bp := range bps {
		if reqRuns, ok := queue[bp.ItemID]; ok {
			name, id := c.store.ResolveLocationID(bp, c.char)
			bp.StationName = name
			bp.StationID = id

			var costList store.CostList
			costList.Possible = true
			runs := runs(bp, reqRuns)
			costList.Runs = runs
			bp.Costs = &costList
			queuedBPs = append(queuedBPs, bp)
		}
	}
	return queuedBPs
}

func materialsNeeded(bps []store.Blueprint) map[int32]int32 {
	res := make(map[int32]int32)
	for _, bp := range bps {
		for _, mat := range bp.Activities.Manufacturing.Materials {
			needs := requiredMaterialsForJob(bp.Costs.Runs, float64(mat.Quantity), float64(bp.MaterialEfficiency))
			res[mat.TypeID] += needs
		}
	}
	return res
}

func (c *Coster) groupMaterialCost(needs map[int32]int32, md store.MarketData) map[int32]store.MaterialCost {
	res := make(map[int32]store.MaterialCost)
	for k, need := range needs {
		cost := c.consumeOrders(need, k, md.MaterialOrders(k))

		res[k] = store.MaterialCost{
			TypeID:   k,
			Total:    cost.OrderCost,
			Quantity: need,
			Enough:   cost.Enough,
		}
	}
	return res
}

func (c *Coster) avgBlueprintCostings(
	bps []store.Blueprint,
	costs map[int32]store.MaterialCost,
	md store.MarketData,
	sys station.Station) error {

	for i, bp := range bps {
		for _, mat := range bp.Activities.Manufacturing.Materials {
			adj, err := c.store.AdjustedPrice(mat.TypeID)
			if err != nil {
				return err
			}

			avgPrice := costs[mat.TypeID].Total / float64(costs[mat.TypeID].Quantity)

			needs := requiredMaterialsForJob(bp.Costs.Runs, float64(mat.Quantity), float64(bp.MaterialEfficiency))
			ordCost := avgPrice * float64(needs)
			estVal := adj * float64(needs)
			enough := costs[mat.TypeID].Enough

			bps[i].Costs.AddCost(store.Cost{
				MEMaterial: store.Material{
					Quantity: needs,
					TypeID:   mat.TypeID,
				},
				OrderCost:     ordCost,
				EstimatedCost: estVal,
				Enough:        enough,
			})
		}

		if bp.Costs.Possible {
			product := bp.Activities.Manufacturing.Products[0].TypeID // manufacturing always produces 1
			var price float64
			if c.market.IsSet() {
				price = md.GetStationLowestProductOrder(product, c.market)
			} else {
				price = md.GetLowestProductOrder(product)
			}
			err := c.calculateProfit(&bps[i], price, sys.SystemID)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
