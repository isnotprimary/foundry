FROM node:alpine as node
WORKDIR /src/ui/
COPY ui/package.json ui/package-lock.json /src/ui/
RUN npm install
COPY ui /src/ui
RUN npm run build

FROM golang:1.23 as golang
WORKDIR /src
RUN apt-get update && apt-get install -y unzip
COPY . /src/
RUN scripts/sde.sh
RUN cd /src && go install && CGO_ENABLED=0 go build -o foundry

FROM scratch
COPY --from=node /src/ui/dist /ui/build
COPY --from=golang /src/foundry /foundry
COPY --from=golang /src/data/sde/fsd/blueprints.yaml /sde/fsd/blueprints.yaml
COPY --from=golang /src/data/sde/bsd/staStations.yaml /sde/bsd/staStations.yaml
COPY --from=golang /src/data/sde/fsd/stationOperations.yaml /sde/fsd/stationOperations.yaml
EXPOSE 8080
EXPOSE 2112
ENV ESI_SECRET=${ESI_SECRET}
ENV ESI_CLIENT_ID=${ESI_CLIENT_ID}
ENV ESI_REDIRECT_URL=${ESI_REDIRECT_URL}
ENV USER_AGENT_HEADER=${USER_AGENT_HEADER}
ENV HTTPS_CERT=${HTTPS_CERT}
ENV HTTPS_KEY=${HTTPS_KEY}
ENV DB_USER=${DB_USER}
ENV DB_PASSWORD=${DB_PASSWORD}
ENV DB_ADDR=${DB_ADDR}
ENV SESSION_KEY=${SESSION_KEY}

ENTRYPOINT ["/foundry", "--sde-dir", "/sde"]