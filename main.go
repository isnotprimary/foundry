package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/alecthomas/kong"
	"github.com/antihax/goesi"
	"github.com/cenkalti/backoff/v4"
	"github.com/circleci/ex/httpserver"
	"github.com/circleci/ex/o11y"
	"github.com/circleci/ex/rootcerts"
	"github.com/circleci/ex/rundef"
	"github.com/circleci/ex/system"
	"github.com/circleci/ex/termination"
	"github.com/circleci/ex/worker"
	"github.com/die-net/lrucache"
	"github.com/die-net/lrucache/twotier"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/postgres"
	"github.com/gregjones/httpcache"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/isnotprimary/foundry/api"
	"gitlab.com/isnotprimary/foundry/auth"
	"gitlab.com/isnotprimary/foundry/client"
	"gitlab.com/isnotprimary/foundry/db"
	"gitlab.com/isnotprimary/foundry/logging"
	"gitlab.com/isnotprimary/foundry/store"
)

type cli struct {
	Dev bool `env:"DEV_MODE" help:"Set up for development"`

	EsiSecret      string `env:"ESI_SECRET" help:"The secret token for esi" required:""`
	EsiClientID    string `env:"ESI_CLIENT_ID" help:"The client id for esi" required:""`
	EsiRedirectURL string `env:"ESI_REDIRECT_URL" required:""`

	SDEDir string `env:"SDE_DIR" required:""`

	UserAgentHeader string `env:"USER_AGENT_HEADER"`

	DBUser     string `env:"DB_USER" default:"foundry"`
	DBPassword string `env:"DB_PASSWORD" default:"insecureBox"`
	DBAddr     string `env:"DB_ADDR" default:"localhost:5432"`
	DBUrl      string `env:"DATABASE_URL"`
	SessionKey string `env:"SESSION_KEY" required:""`
}

const MB = 1000000

func main() {
	cli := cli{}
	kong.Parse(&cli)

	logLevel := zerolog.InfoLevel
	if cli.Dev {
		logLevel = zerolog.DebugLevel
	}

	ctx := context.Background()

	sys := system.New()
	defer sys.Cleanup(ctx)

	ctx, endLogging, err := logging.Setup(ctx, sys, logLevel, "0.0.0.0:2112")
	if err != nil {
		log.Fatal().Err(fmt.Errorf("failed to setup logging and metrics: %w", err)).Send()
	}

	_ = rundef.Defaults(ctx)

	sys.AddCleanup(func(ctx context.Context) error {
		endLogging(ctx)
		return nil
	})

	err = rootcerts.UpdateDefaultTransport()
	if err != nil {
		log.Fatal().Err(fmt.Errorf("failed to inject rootcerts: %w", err)).Send()
	}
	DB, err := setupDB(cli, sys)
	if err != nil {
		log.Fatal().Err(fmt.Errorf("failed to setup database: %w", err)).Send()
	}

	a := setupAPI(ctx, cli, DB, sys)

	_, err = httpserver.Load(ctx, httpserver.Config{
		Name:    "eve-foundry-api",
		Addr:    "0.0.0.0:8080",
		Handler: a.Handler(),
	}, sys)

	if err != nil {
		log.Fatal().Err(fmt.Errorf("failed to start server: %w", err)).Send()
	}

	err = sys.Run(ctx, 0)
	if err != nil && !errors.Is(err, termination.ErrTerminated) {
		log.Fatal().Err(err).Send()
	}
}

func setupAPI(ctx context.Context, cli cli, DB *sqlx.DB, sys *system.System) *api.API {
	eveAPI := makeESIAPIClient(ctx, cli, DB, sys)
	eveStore, err := store.NewStore(ctx, eveAPI, cli.SDEDir)
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	am := auth.NewEveAuther(cli.EsiClientID, cli.EsiSecret, cli.EsiRedirectURL)

	sessionStore, err := postgres.NewStore(DB.DB, []byte(cli.SessionKey))
	if err != nil {
		log.Fatal().Err(err).Send()
	}

	sessionStore.Options(sessions.Options{
		MaxAge:   86400,                // 1day
		Secure:   !cli.Dev,             // No https in dev mode
		HttpOnly: true,                 // Doesn't need to be made available to JS
		SameSite: http.SameSiteLaxMode, // Needs lax, due to redirect during sso
	})

	a := api.New(ctx, api.Config{
		EsiSecretKey:   cli.EsiSecret,
		EsiClientID:    cli.EsiClientID,
		EsiRedirectURL: cli.EsiRedirectURL,
		SessionStore:   sessionStore,
		AuthManager:    am,
		EveStore:       eveStore,
		DevMode:        cli.Dev,
	})
	return a
}

func setupDB(cli cli, sys *system.System) (*sqlx.DB, error) {
	var DB *sqlx.DB
	var err error
	if cli.DBUrl != "" {
		DB, err = db.SetupURL(cli.DBUrl)
	} else {
		DB, err = db.Setup(cli.DBUser, cli.DBPassword, cli.DBAddr, "foundry")
	}
	if err != nil {
		return nil, err
	}
	sys.AddCleanup(func(_ context.Context) error {
		return DB.Close()
	})
	return DB, nil
}

func makeESIAPIClient(ctx context.Context, cli cli, DB *sqlx.DB, sys *system.System) *goesi.APIClient {
	lru := lrucache.New(100*MB, 10*60)
	cache := twotier.New(lru, client.NewCache(DB))
	transport := httpcache.NewTransport(cache)
	wrappedTransport := client.NewMetricClient(
		&client.ErrorLimitRoundTrip{Proxied: transport},
		o11y.FromContext(ctx).MetricsProvider(),
	)
	hc := http.Client{
		Timeout:   5 * time.Second,
		Transport: wrappedTransport,
	}
	sys.AddService(func(ctx context.Context) error {
		worker.Run(ctx, worker.Config{
			Name:          "lru_metrics",
			NoWorkBackOff: &backoff.ConstantBackOff{Interval: 5 * time.Second},
			MaxWorkTime:   5 * time.Second,
			MinWorkTime:   5 * time.Second,
			WorkFunc: func(ctx context.Context) (err error) {
				m := o11y.FromContext(ctx).MetricsProvider()
				_ = m.Gauge("lru_cache_size", float64(lru.Size()), nil, 1)
				return nil
			},
			BackoffOnAllErrors: false,
		})
		return nil
	})

	return goesi.NewAPIClient(&hc, cli.UserAgentHeader)
}
